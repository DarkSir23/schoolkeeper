#!/usr/bin/env python2
# -*- coding: UTF-8 -*-
import ConfigParser
import locale
import os
import platform
import stat
import sys
import threading
import time
import re

import schoolkeeper
from schoolkeeper import webStart, logger, dbupgrade, versioncheck

def main():
    # rename this thread
    threading.currentThread().name = "MAIN"

    # Set paths
    if hasattr(sys, 'frozen'):
        schoolkeeper.FULL_PATH = os.path.abspath(sys.executable)
    else:
        schoolkeeper.FULL_PATH = os.path.abspath(__file__)

    schoolkeeper.PROG_DIR = os.path.dirname(schoolkeeper.FULL_PATH)
    schoolkeeper.ARGS = sys.argv[1:]

    schoolkeeper.SYS_ENCODING = None

    try:
        locale.setlocale(locale.LC_ALL, "")
        schoolkeeper.SYS_ENCODING = 'utf-8'
    except (locale.Error, IOError):
        pass

    # set arguments

    from optparse import OptionParser

    p = OptionParser()
    p.add_option('-d', '--daemon', action="store_true", dest='daemon', help="Run the server as a daemon")
    p.add_option('-q', '--quiet', action="store_true", dest='quiet', help="Don't log to the console")
    p.add_option('--debug', action="store_true", dest='debug', help="Show debuglog messages")
    p.add_option('--update', action="store_true", dest='update', help="Update to latest version (only git or source installs)")
    p.add_option('--port', dest='port', default=None, help="Force webinterface to listen on this port")
    p.add_option('--datadir', dest='datadir', default=None, help="Path to the data directory")
    p.add_option('--config', dest='config', default=None, help="Path to config.ini file")
    p.add_option('-p', '--pidfile', dest='pidfile', default=None, help="Store the process id in the given file")

    options, args = p.parse_args()

    schoolkeeper.LOGLEVEL = 1
    if options.debug:
        schoolkeeper.LOGLEVEL = 3
    if options.quiet:
        schoolkeeper.LOGLEVEL = 0
    if options.daemon:
        if 'windows' not in platform.system().lower():
            schoolkeeper.DAEMON = True
            schoolkeeper.daemonize()
        else:
            print("Daemonize not supported under Windows, starting normally")

    if options.update:
        schoolkeeper.SIGNAL = 'update'
        # This is the "emergency recovery" update in case schoolkeeper won't start.
        # Set up some dummy values for the update as we have not read the config file yet
        schoolkeeper.CONFIG['GIT_PROGRAM'] = ''
        schoolkeeper.CONFIG['GIT_USER'] = 'DarkSir23'
        schoolkeeper.CONFIG['GIT_REPO'] = 'schoolkeeper'
        schoolkeeper.CONFIG['LOGLIMIT'] = 2000
        versioncheck.getInstallType()
        if schoolkeeper.CONFIG['INSTALL_TYPE'] not in ['git', 'source']:
            schoolkeeper.SIGNAL = None
            print('Cannot update, not a git or source installation')
        else:
            schoolkeeper.shutdown(restart=True, update=True)

    if options.datadir:
        schoolkeeper.DATADIR = str(options.datadir)
    else:
        schoolkeeper.DATADIR = schoolkeeper.PROG_DIR

    if options.config:
        schoolkeeper.CONFIGFILE = str(options.config)
    else:
        schoolkeeper.CONFIGFILE = os.path.join(schoolkeeper.DATADIR, "config.ini")

    if options.pidfile:
        if schoolkeeper.DAEMON:
            schoolkeeper.PIDFILE = str(options.pidfile)

    # create and check (optional) paths
    if not os.path.exists(schoolkeeper.DATADIR):
        try:
            os.makedirs(schoolkeeper.DATADIR)
        except OSError:
            raise SystemExit('Could not create data directory: ' + schoolkeeper.DATADIR + '. Exit ...')

    if not os.access(schoolkeeper.DATADIR, os.W_OK):
        raise SystemExit('Could not write to the data directory: ' + schoolkeeper.DATADOR + '. Exit ...')

    print "schoolkeeper is starting up..."
    time.sleep(4) # allow a bit of time for old task to exit if restarting.  Needs to free logfile and server port.

    #create database and config
    # schoolkeeper.DBFILE = os.path.join(schoolkeeper.DATADIR, 'schoolkeeper.db')
    schoolkeeper.CFG = ConfigParser.RawConfigParser()
    schoolkeeper.CFG.read(schoolkeeper.CONFIGFILE)

    # No Logging Before This Point
    schoolkeeper.initialize()

    # Add Versioncheck here
    versioncheck.checkForUpdates()

    if options.port:
        schoolkeeper.CONFIG['HTTP_PORT'] = int(options.port)
        logger.info('Starting schoolkeeper on forced port: %s, webroot "%s"' % (schoolkeeper.CONFIG['HTTP_PORT'], schoolkeeper.CONFIG['HTTP_ROOT']))
    else:
        schoolkeeper.CONFIG['HTTP_PORT'] = int(schoolkeeper.CONFIG['HTTP_PORT'])
        logger.info('Starting schoolkeeper on port: %s, webroot "%s"' % (schoolkeeper.CONFIG['HTTP_PORT'], schoolkeeper.CONFIG['HTTP_ROOT']))

    if schoolkeeper.DAEMON:
        schoolkeeper.daemonize()

    curr_ver = dbupgrade.upgrade_needed()
    if curr_ver:
        schoolkeeper.UPDATE_MSG = 'Updating database to version %s' % curr_ver


    # Try to start the server

    webStart.initialize({
        'http_port': schoolkeeper.CONFIG['HTTP_PORT'],
        'http_host': schoolkeeper.CONFIG['HTTP_HOST'],
        'http_root': schoolkeeper.CONFIG['HTTP_ROOT'],
        'http_user': schoolkeeper.CONFIG['HTTP_USER'],
        'http_pass': schoolkeeper.CONFIG['HTTP_PASS'],
        'http_proxy': schoolkeeper.CONFIG['HTTP_PROXY'],
        'https_enabled': schoolkeeper.CONFIG['HTTPS_ENABLED'],
        'https_cert': schoolkeeper.CONFIG['HTTPS_CERT'],
        'https_key': schoolkeeper.CONFIG['HTTPS_KEY']
    })

    if curr_ver:
        threading.Thread(target=dbupgrade.dbupgrade, name="DB_UPGRADE", args=[curr_ver]).start()

    schoolkeeper.start()

    while True:
        if not schoolkeeper.SIGNAL:
            try:
                time.sleep(1)
            except KeyboardInterrupt:
                schoolkeeper.shutdown()
        else:
            if schoolkeeper.SIGNAL == 'shutdown':
                schoolkeeper.shutdown()
            elif schoolkeeper.SIGNAL == 'restart':
                schoolkeeper.shutdown(restart=True)
            elif schoolkeeper.SIGNAL == 'update':
                schoolkeeper.shutdown(restart=True, update=True)
            schoolkeeper.SIGNAL = None

if __name__ == "__main__":
    main()
