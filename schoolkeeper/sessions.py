import cherrypy

import common
import logger

def get_session_info():
    """
    Returns the session info for the user session
    """
    from schoolkeeper.webauth import SESSION_KEY

    _session = {'user_id': None,
                'user': None,
                'access_level': [],
                'expiry': None,
                'theme': None,
                'themelook': None
                }
    try:
        _newsession = cherrypy.session.get(SESSION_KEY, _session)
        if _newsession:
            return _newsession
        else:
            return _session

    except AttributeError as e:
        return _session

