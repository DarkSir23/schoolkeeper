import database
import csv
import schoolkeeper
import os
import lib.simplejson as simplejson

from schoolkeeper import logger
from schoolkeeper.common import dbDateFormat, currentTime

def directCert(importfile=None):
    myDB = database.DBConnection()
    success = False
    if importfile:
        csvlist = []
        from io import BytesIO
        try:
            csvreader = csv.DictReader(BytesIO(importfile.file.read().decode('utf-16').encode('utf-8')))
        except UnicodeDecodeError as e:
            msg = 'Unable to import DirectCert File:  %s' % e
            return success, None, None, msg
        except Exception as e:
            msg = 'Unable to read DirectCert File:  %s' % e
            return success, None, None, msg
        if 'STN' in csvreader.fieldnames:
            for row in csvreader:
                student = myDB.match('SELECT * from ps_students WHERE stn = ?', (row['STN'],))
                if student:
                    row['Student Number'] = student['studentid']
                    csvlist.append(row)
            cacheLocation = schoolkeeper.CACHEDIR
            if not os.path.exists(cacheLocation):
                os.mkdir(cacheLocation)
            filename = "direct_cert_export.csv"
            fileloc = os.path.join(cacheLocation ,filename)
            with open(fileloc, 'w') as csvfile:
                fieldnames = []
                for key in csvlist[0].keys():
                    fieldnames.append(key)
                writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
                writer.writeheader()
                for line in csvlist:
                    writer.writerow(line)
            csvfile.close()
            success = True
            return success, filename, fileloc, None
        else:
            msg = "Invalid DirectCert File (no STN field)"
            return success, None, None, msg
    else:
        msg = "No import file provided."
        return success, None, None, msg


def getKonicaAddressBookCSV():
    myDB = database.DBConnection()
    people = myDB.select('SELECT people.firstname, people.lastname, ignore, people.email, ps_staff.status from people, ps_staff WHERE people.identifier = ps_staff.psid and id LIKE ? and ignore != 1 and ps_staff.status = 1', ('STA%',))
    csvlist = []
    addresslist = []
    cacheLocation = schoolkeeper.CACHEDIR
    if not os.path.exists(cacheLocation):
        os.mkdir(cacheLocation)
    filename = 'konica_address.csv'
    fileloc = os.path.join(cacheLocation, filename)
    success = False
    for person in people:
        if person['email'] not in addresslist:
            entry = {}
            name = '%s, %s' % (person['lastname'], person['firstname'])
            entry['Name'] = name
            character = person['lastname'][0].upper()
            if character in 'ABC':
                entry['Search'] = 'ABC'
            elif character in 'DEF':
                entry['Search'] = 'DEF'
            elif character in 'GHI':
                entry['Search'] = 'GHI'
            elif character in 'JKL':
                entry['Search'] = 'JKL'
            elif character in 'MNO':
                entry['Search'] = 'MNO'
            elif character in 'PQRS':
                entry['Search'] = 'PQRS'
            elif character in 'TUV':
                entry['Search'] = 'TUV'
            elif character in 'WXYZ':
                entry['Search'] = 'WXYZ'
            else:
                entry['Search'] = 'etc'
            entry['Email'] = person['email']
            csvlist.append(entry)
    try:
        with open(fileloc, 'w') as csvfile:
            fieldnames = []
            for key in csvlist[0].keys():
                fieldnames.append(key)
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writeheader()
            for line in csvlist:
                writer.writerow(line)
        csvfile.close()
        success = True
        return success, filename, fileloc, None
    except Exception as e:
        msg = "Error: %s" % e
        return success, filename, fileloc, msg

def getKnowBe4StaffCSV():
    myDB = database.DBConnection()
    people = myDB.select('SELECT people.firstname, people.lastname, ignore, people.email, ps_staff.status from people, ps_staff WHERE people.identifier = ps_staff.psid and id LIKE ? and ignore != 1 and ps_staff.status = 1', ('STA%',))
    csvlist = []
    addresslist = []
    cacheLocation = schoolkeeper.CACHEDIR
    if not os.path.exists(cacheLocation):
        os.mkdir(cacheLocation)
    filename = 'knowbe4.csv'
    fileloc = os.path.join(cacheLocation, filename)
    success = False
    for person in people:
        if person['email'] not in addresslist:
            entry = {}
            entry['Email'] = person['email']
            name = '%s %s' % (person['firstname'], person['lastname'])
            entry['Name'] = name
            entry['Email'] = person['email']
            entry['First Name'] = person['firstname']
            entry['Last Name'] = person['lastname']
            csvlist.append(entry)
    try:
        with open(fileloc, 'w') as csvfile:
            fieldnames = []
            for key in csvlist[0].keys():
                fieldnames.append(key)
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writeheader()
            for line in csvlist:
                writer.writerow(line)
        csvfile.close()
        success = True
        return success, filename, fileloc, None
    except Exception as e:
        msg = "Error: %s" % e
        return success, filename, fileloc, msg


def getSchoolPhotosCSV(schoolid=None, classname=None):
    myDB = database.DBConnection()
    if schoolid:
        people = myDB.select('SELECT * from people WHERE schoolid=? ORDER BY lastname, firstname', (schoolid,))
    else:
        people = myDB.select('SELECT * from people ORDER BY lastname, firstname')
    csvlist = []
    addresslist = []
    if classname:
        logger.debug('Classname detected')
        tempclasses = myDB.select('SELECT schedules.psid, classname, staffid, firstday, lastday, firstname, lastname FROM schedules, \
                  classes, ps_staff WHERE schedules.classid=classes.classid AND classes.staffid=ps_staff.psid')
        classes = []
        today = currentTime()
        for entry in tempclasses:
            if dbDateFormat(entry['firstday']) <= today <= dbDateFormat(entry['lastday']):
                classes.append(entry)
    cacheLocation = schoolkeeper.CACHEDIR
    if not os.path.exists(cacheLocation):
        os.mkdir(cacheLocation)
    if schoolid:
        school = myDB.match('select * from schools WHERE schoolid=?', (schoolid,))
        logger.debug('School: %s' % school)
        filename = 'school_photos_export_%s.csv' % school['schoolabbrev'].lower()
    else:
        filename = 'school_photos_export.csv'
    fileloc = os.path.join(cacheLocation, filename)
    success = False
    for person in people:
        entry = {}
        if person['id'][:3] == 'STU':
            ps_info = myDB.match('SELECT * from ps_students WHERE studentid = ?', (person['identifier'],))
            if ps_info:
                if classname:
                    teachername = ''
                    for classentry in classes:
                        if classentry['psid'] == ps_info['psid']:
                            if classentry['classname'] == classname:
                                teachername = classentry['lastname']
                                break
                details = simplejson.loads(ps_info['details'])
                if details['guardian_email']:
                    guardian_email = details['guardian_email']
                else:
                    guardian_email = ''
                entry['Last Name'] = person['lastname']
                entry['First Name'] = person['firstname']
                entry['Grade'] = ps_info['gradelevel']
                entry['Teacher'] = teachername
                entry['Student ID'] = ps_info['studentid']
                entry['Parent Email Address'] = guardian_email
                csvlist.append(entry)
        elif person['id'][:3] == 'STA' and int(person['ignore']) != 1:
            ps_info = myDB.match('SELECT * from ps_staff WHERE psid = ?', (person['identifier'],))
            if ps_info and int(ps_info['status']) == 1:
                entry['Last Name'] = person['lastname']
                entry['First Name'] = person['firstname']
                entry['Teacher'] = 'staff'
                entry['Grade'] = 'staff'
                entry['Student ID'] = person['identifier']
                entry['Salutation'] = ''
                entry['Job Title'] = ps_info['title']
                csvlist.append(entry)
    fieldnames = ['Last Name', 'First Name', 'Grade', 'Teacher', 'Student ID', 'Parent Email Address', 'Salutation', 'Job Title']
    try:
        with open(fileloc, 'w') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writeheader()
            sortedcsv = sorted(csvlist, key=lambda k: (k['Grade'], k['Last Name'], k['First Name']))
            for line in sortedcsv:
                writer.writerow(line)
        csvfile.close()
        success = True
        return success, filename, fileloc, None
    except Exception as e:
        msg = "Error: %s" % e
        return success, filename, fileloc, msg


def getAdobeUserCSV():
    myDB = database.DBConnection()
    people = myDB.select('SELECT * from people')
    addresslist = []
    csvlist = []
    cacheLocation = schoolkeeper.CACHEDIR
    if not os.path.exists(cacheLocation):
        os.mkdir(cacheLocation)
    filename = 'adobe_user_export.csv'
    fileloc = os.path.join(cacheLocation, filename)
    success = False
    for row in people:
        if row['email'] not in addresslist:
            addresslist.append(row['email'])
            entry = {'Identity Type': 'Federated ID',
                     'Username': row['email'],
                     'domain': schoolkeeper.CONFIG['CORP_DOMAIN'],
                     'Email': row['email'],
                     'First Name': row['firstname'],
                     'Last Name': row['lastname'],
                     'Country Code': 'US',
                     'User Groups': 'Spark'}
            csvlist.append(entry)
    try:
        with open(fileloc, 'w') as csvfile:
            fieldnames = []
            for key in csvlist[0].keys():
                fieldnames.append(key)
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writeheader()
            for line in csvlist:
                writer.writerow(line)
        csvfile.close()
        success = True
        return success, filename, fileloc, None
    except Exception as e:
        msg = "Error: %s" % e
        return success, filename, fileloc, msg


def getPearsonUserCSV():
    myDB = database.DBConnection()
    people = myDB.select('SELECT pearsonid, pearsonpassword, firstname, lastname, studentid, gradelevel, studentemail from ps_students, schools WHERE ps_students.schoolid = schools.schoolid')
    addresslist = []
    csvlist = []
    cacheLocation = schoolkeeper.CACHEDIR
    if not os.path.exists(cacheLocation):
        os.mkdir(cacheLocation)
    filename = 'pearson_successnet_export.txt'
    fileloc = os.path.join(cacheLocation, filename)
    tempfileloc = fileloc + '.tmp'
    success = False
    for row in people:
        if row['studentemail'] not in addresslist:
            if int(row['gradelevel']) >= 0:
                if int(row['gradelevel']) == 0:
                    grade = 'K'
                else:
                    grade = row['gradelevel']
                addresslist.append(row['studentemail'])
                entry = {'School ID': row['pearsonid'],
                         'FirstName': row['firstname'],
                         'MiddleInitial': '',
                         'LastName': row['lastname'],
                         'StudentID': row['studentid'],
                         'Grade': grade,
                         'SuccessNetLanguage': 'ENG',
                         'UserName': row['studentemail'],
                         'Password': row['pearsonpassword'],
                         'PasswordConfirmation': row['pearsonpassword'],
                         'Gender': 'UN',
                         'EnglishLanguageProficiency': 'UN',
                         'Ethnicity': 'UN',
                         'MealProgram': 'UN',
                         'SpecialCondition': 'UN',
                         'MigrantStatus': 'UN',
                         'SpecialServices': 'UN',
                         }
                csvlist.append(entry)
    try:
        with open(tempfileloc, mode='w') as csvfile:
            import unicodecsv
            fieldnames = ['School ID', 'FirstName', 'MiddleInitial', 'LastName', 'StudentID', 'Grade', 'SuccessNetLanguage', 'UserName', 'Password', 'PasswordConfirmation', 'Gender', 'EnglishLanguageProficiency', 'Ethnicity', 'MealProgram', 'SpecialCondition', 'MigrantStatus', 'SpecialServices']
            fieldnames = list([s.encode('utf-8') for s in fieldnames])
            writer = unicodecsv.DictWriter(csvfile, delimiter='\t', dialect=csv.excel, fieldnames=fieldnames, encoding='utf-8')
            writer.writeheader()
            for line in csvlist:
                newline = {}
                for key in line.keys():
                    newline[key.encode('utf-8')] = str(line[key]).encode('utf-8')
                writer.writerow(newline)
        import codecs
        with codecs.open(tempfileloc, "r", "utf-8") as infile:
            with codecs.open(fileloc, "w", "utf-16") as outfile:
                while True:
                    contents = infile.read(1048576)
                    if not contents:
                        break
                    outfile.write(contents)
        success = True
        return success, filename, fileloc, None
    except Exception as e:
        msg = "Error: %s" % e
        return success, filename, fileloc, msg

def getStudentsDevicesByTeacher(schoolid):
    myDB = database.DBConnection()
    args =  []
    school = myDB.match("SELECT * from schools WHERE schoolid = ?", (schoolid,))
    if school:
        school['details'] = simplejson.loads(school['details']) if school['details'] else {}
        classname = school['details']['homeclassname'] if 'homeclassname' in school['details'].keys() else None
        cmd = "SELECT people.lastname as studentlast, people.firstname as studentfirst, devices.tag, schedules.classid FROM ps_students \
               LEFT JOIN schedules ON ps_students.psid = schedules.psid \
               LEFT JOIN people ON ps_students.studentid = people.identifier \
               INNER JOIN devices on people.id = devices.ownerid"
        cmd += " ORDER BY studentlast, studentfirst"
        students = myDB.select(myDB.dbformat(cmd), tuple(args))
        cmd = "SELECT people.lastname as stafflast, people.firstname as stafffirst, classes.classid, classes.firstday, classes.lastday, classes.classname FROM people \
               INNER JOIN classes ON people.identifier = classes.staffid"
        if classname:
            cmd += " WHERE classname=?"
            args.append(classname)
        if schoolid:
            cmd += " AND classes.schoolid=?"
            args.append(schoolid)
        cmd += " ORDER BY stafflast, stafffirst"
        classes = myDB.select(myDB.dbformat(cmd), tuple(args))
        today = currentTime()
        cacheLocation = schoolkeeper.CACHEDIR
        if not os.path.exists(cacheLocation):
            os.mkdir(cacheLocation)
        school = myDB.match('select * from schools WHERE schoolid=?', (schoolid,))
        logger.debug('School: %s' % school)
        filename = 'student_devices_by_teacher_%s.csv' % school['schoolabbrev'].lower()
        fileloc = os.path.join(cacheLocation, filename)
        csvlist = []
        for classentry in classes:
            if dbDateFormat(classentry['firstday']) <= today <= dbDateFormat(classentry['lastday']):
                for student in students:
                    if classentry['classid'] == student['classid']:
                        entry = {"Teacher Last": classentry['stafflast'],
                                 "Teacher First": classentry['stafffirst'],
                                 "Student Last": student['studentlast'],
                                 "Student First": student['studentfirst'],
                                 "Tag": student['tag']
                        }
                        csvlist.append(entry)
        success, msg = writecsvfile(csvlist, fileloc,
                                    fieldnames=["Teacher Last", "Teacher First", "Student Last",
                                                "Student First", "Tag"])
        return success, filename, fileloc, msg


def getMobyMaxStudents(schoolid=None, classname=None):
    myDB = database.DBConnection()
    args = []
    cmd = "SELECT ps_students.lastname as studentlast, cafepin, studentid, ps_students.gradelevel, ps_students.firstname as studentfirst, \
            psusername, classname, period, term, room, firstday, lastday, people.email \
            FROM schedules, classes, people, ps_students WHERE schedules.classid=classes.Classid \
            AND schedules.psid = ps_students.psid AND people.identifier=classes.staffid AND ps_students.gradelevel > -1"
    if classname:
        cmd += " AND classname=?"
        args.append(classname)
    if schoolid:
        cmd += " AND ps_students.schoolid=?"
        args.append(schoolid)
    cmd += " ORDER BY studentlast, studentfirst"
    maxlist = myDB.select(myDB.dbformat(cmd), tuple(args))
    csvlist = []
    userlist = []
    cacheLocation = schoolkeeper.CACHEDIR
    if not os.path.exists(cacheLocation):
        os.mkdir(cacheLocation)
    if schoolid:
        school = myDB.match('select * from schools WHERE schoolid=?', (schoolid,))
        logger.debug('School: %s' % school)
        filename = 'mobymax_student_export_%s.csv' % school['schoolabbrev'].lower()
    else:
        filename = 'mobymax_student_export.csv'
    fileloc = os.path.join(cacheLocation, filename)
    today = currentTime()
    for item in maxlist:
        if dbDateFormat(item['firstday']) <= today <= dbDateFormat(item['lastday']) and '%s|%s' % (item['psusername'], item['email'])  not in userlist:
            userlist.append('%s|%s' % (item['psusername'], item['email']))
            if item['cafepin']:
                defaultpw = item['cafepin']
            else:
                defaultpw = item['studentid']
            entry = {"Student's First Name": item['studentfirst'],
                     "Student's Last Name": item['studentlast'],
                     "Unique Student ID": item['studentid'],
                     "Student Grade Level": item['gradelevel'],
                     "Student Username": item['psusername'],
                     "Student Password": defaultpw,
                     "Teacher's School Email Address": item['email']}
            csvlist.append(entry)
    success, msg = writecsvfile(csvlist, fileloc, fieldnames=["Student's First Name","Student's Last Name", "Unique Student ID", "Student Grade Level", "Student Username", "Student Password", "Teacher's School Email Address"])
    return success, filename, fileloc, msg

def getMobyMaxTeachers(schoolid=None, classname=None):
    myDB = database.DBConnection()
    args = []
    cmd = "SELECT ps_students.lastname as studentlast, cafepin, studentid, ps_students.gradelevel, ps_students.firstname as studentfirst, \
            psusername, classname, period, term, room, firstday, lastday, people.firstname, people.lastname, people.email \
            FROM schedules, classes, people, ps_students WHERE schedules.classid=classes.Classid \
            AND schedules.psid = ps_students.psid AND people.identifier=classes.staffid AND ps_students.gradelevel > -1"
    if classname:
        cmd += " AND classname=?"
        args.append(classname)
    if schoolid:
        cmd += " AND ps_students.schoolid=?"
        args.append(schoolid)
    cmd += " ORDER BY lastname, firstname"
    maxlist = myDB.select(myDB.dbformat(cmd), tuple(args))
    csvlist = []
    userlist = []
    cacheLocation = schoolkeeper.CACHEDIR
    if not os.path.exists(cacheLocation):
        os.mkdir(cacheLocation)
    if schoolid:
        school = myDB.match('select * from schools WHERE schoolid=?', (schoolid,))
        logger.debug('School: %s' % school)
        filename = 'mobymax_teacher_export_%s.csv' % school['schoolabbrev'].lower()
    else:
        filename = 'mobymax_teacher_export.csv'
    fileloc = os.path.join(cacheLocation, filename)
    today = currentTime()
    for item in maxlist:
        if dbDateFormat(item['firstday']) <= today <= dbDateFormat(item['lastday']) and item['email'] not in userlist:
            userlist.append(item['email'])
            defaultpw = 'whitko%s%s' % (item['firstname'][0].lower(), item['lastname'][0].lower())
            entry = {"Teacher's First Name": item['firstname'],
                     "Teacher's Last Name": item['lastname'],
                     "Teacher's School Email": item['email'],
                     "Password": defaultpw,
                     "Grade Level": item['gradelevel']}
            csvlist.append(entry)
    success, msg = writecsvfile(csvlist, fileloc, fieldnames=["Teacher's First Name","Teacher's Last Name", "Teacher's School Email", "Password", "Grade Level"])
    return success, filename, fileloc, msg

def getStudentGuardianEmails():
    myDB = database.DBConnection()
    studentlist = myDB.select("SELECT email, identifier, details FROM people, ps_students WHERE id LIKE 'STU%' AND people.identifier = ps_students.studentid")
    csvlist = []
    cacheLocation = schoolkeeper.CACHEDIR
    if not os.path.exists(cacheLocation):
        os.mkdir(cacheLocation)
    filename = 'students_guardians.csv'
    fileloc = os.path.join(cacheLocation, filename)
    for item in studentlist:
        details = simplejson.loads(item['details'])
        if details['guardian_email']:
            guardian_emails = details['guardian_email']
        else:
            guardian_emails = ''
        if len(guardian_emails) > 2:
            guardianlist = guardian_emails.split(';')
            for guardian_email in guardianlist:
                entry = {'student-email': item['email'],
                         'guardian-email': guardian_email}
                csvlist.append(entry)
    success, msg = writecsvfile(csvlist, fileloc)
    return success, filename, fileloc, msg


def getStudentsWithoutDevice():
    myDB = database.DBConnection()
    activestudents = myDB.select(
        "SELECT id, people.firstname, people.lastname, gradelevel, studentid FROM people, ps_students WHERE people.identifier=ps_students.studentid ORDER by gradelevel, lastname, firstname")
    csvlist = []
    cacheLocation = schoolkeeper.CACHEDIR
    if not os.path.exists(cacheLocation):
        os.mkdir(cacheLocation)
    filename = 'students_without_devices.csv'
    fileloc = os.path.join(cacheLocation, filename)
    for student in activestudents:
        devices = myDB.select("SELECT deviceid, ownerid FROM devices WHERE ownerid=? AND (retired IS NULL OR retired = 0)",
                              (student['id'],))
        if len(devices) == 0:
            entry = {"Last Name": student['lastname'], "First Name": student['firstname'], "Grade Level": student['gradelevel']}
            csvlist.append(entry)
        else:
            logger.debug(len(devices))
    fieldnames = ["Grade Level","Last Name", "First Name"]
    success, msg = writecsvfile(csvlist, fileloc, fieldnames=fieldnames)
    return success, filename, fileloc, msg

def getStudentLists(classname=None, staffid=None):
    myDB = database.DBConnection()
    args = []
    cmd = "SELECT ps_students.lastname as studentlast, cafepin, studentid, ps_students.firstname as studentfirst, \
            ps_students.studentemail, classname, period, term, room, firstday, lastday, people.lastname, \
            people.firstname FROM schedules, classes, people, ps_students WHERE schedules.classid=classes.Classid \
            AND schedules.psid = ps_students.psid AND people.identifier=classes.staffid"
    if classname:
        cmd += " AND classname=?"
        args.append(classname)
    if staffid:
        cmd += " AND classes.staffid=?"
        args.append(staffid)
    cmd += " ORDER BY people.lastname, people.firstname, period, studentlast, studentfirst"
    schedulelist = myDB.select(myDB.dbformat(cmd), tuple(args))
    csvlist = []
    cacheLocation = schoolkeeper.CACHEDIR
    if not os.path.exists(cacheLocation):
        os.mkdir(cacheLocation)
    filename = 'students_by_class.csv'
    fileloc = os.path.join(cacheLocation, filename)
    success = False
    today = currentTime()
    for item in schedulelist:
        if dbDateFormat(item['firstday']) <= today <= dbDateFormat(item['lastday']):
            studentname = '%s, %s' % (item['studentlast'], item['studentfirst'])
            teachername = '%s, %s' % (item['lastname'], item['firstname'])
            if item['cafepin']:
                defaultpw = item['cafepin']
            else:
                defaultpw = item['studentid']
            entry = {
                'teachername': teachername,
                'term': item['term'],
                'period': item['period'],
                'classname': item['classname'],
                'studentname': studentname,
                'email': item['studentemail'],
                'defaultpw': defaultpw
            }
            csvlist.append(entry)
    try:
        with open(fileloc, 'w') as csvfile:
            fieldnames = ['teachername', 'term', 'period', 'classname', 'studentname', 'email', 'defaultpw']
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writeheader()
            for line in csvlist:
                writer.writerow(line)
        csvfile.close()
        success = True
        return success, filename, fileloc, None
    except Exception as e:
        msg = "Error: %s" % e
        return success, filename, fileloc, msg

def writecsvfile(csvlist, fileloc, usefieldnames=True, fieldnames=None):
    try:
        with open(fileloc, 'w') as csvfile:
            if usefieldnames:
                if not fieldnames:
                    fieldnames = []
                    for key in csvlist[0].keys():
                        fieldnames.append(key)
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            if usefieldnames:
                writer.writeheader()
            for line in csvlist:
                writer.writerow(line)
        csvfile.close()
        return True, None
    except Exception as e:
        msg = "CSV Error: %s" % e
        return False, msg

def getOLSATStudents(schoolid=None):
    myDB = database.DBConnection()
    args = []
    cmd = "SELECT people.lastname as studentlast, studentid, dob, ps_students.gradelevel, people.firstname as studentfirst, \
            firstday, lastday, gender, ethnicitycode \
            FROM classes, ps_students, schedules, people WHERE people.identifier = ps_students.studentid AND schedules.classid=classes.Classid \
            AND schedules.psid = ps_students.psid AND ps_students.gradelevel > 2 and ps_students.gradelevel < 9"
    if schoolid:
        cmd += " AND ps_students.schoolid=?"
        args.append(schoolid)
    cmd += " ORDER BY studentlast, studentfirst"
    olsatlist = myDB.select(myDB.dbformat(cmd), tuple(args))
    logger.debug("Count: %s" % len(olsatlist))
    csvlist = []
    userlist = []
    cacheLocation = schoolkeeper.CACHEDIR
    if not os.path.exists(cacheLocation):
        os.mkdir(cacheLocation)
    if schoolid:
        school = myDB.match('select * from schools WHERE schoolid=?', (schoolid,))
        logger.debug('School: %s' % school)
        filename = 'olsat_student_export_%s.csv' % school['schoolabbrev'].lower()
    else:
        filename = 'olsat_student_export.csv'
    fileloc = os.path.join(cacheLocation, filename)
    today = currentTime()
    for item in olsatlist:
        if dbDateFormat(item['firstday']) <= today <= dbDateFormat(item['lastday']) and '%s' % (item['studentid'])  not in userlist:
            userlist.append('%s' % (item['studentid']))
            entry = {
                'StudentID': item['studentid'],
                'LastName': item['studentlast'],
                'FirstName': item['studentfirst'],
                'MiddleName': '',
                'DateOfBirth': item['dob'],
                'Grade': item['gradelevel'],
                'TestingGrade': item['gradelevel'],
                'Gender': item['gender'],
                'Ethnicity': schoolkeeper.ETHNICITY_CODES[item['ethnicitycode']],
                'SpecialServices': '',
                'ECStatus': '',
                'Bilingual': '',
                'EnglishProficient': ''
            }
            csvlist.append(entry)
    success, msg = writecsvfile(csvlist, fileloc,
                                fieldnames=["StudentID", "LastName", "FirstName", "MiddleName",
                                            "DateOfBirth", "Grade", "TestingGrade", "Gender", "Ethnicity",
                                            "SpecialServices", "ECStatus", "Bilingual", "EnglishProficient"])
    return success, filename, fileloc, msg

def getPeople():
    myDB = database.DBConnection()
    peoplelist = myDB.select("SELECT * from people")
    csvlist = []
    cacheLocation = schoolkeeper.CACHEDIR
    if not os.path.exists(cacheLocation):
        os.mkdir(cacheLocation)
    filename='people.csv'
    fileloc = os.path.join(cacheLocation, filename)
    for item in peoplelist:
        entry = {}
        for key in item.keys():
            entry[key] = item[key]
        csvlist.append(entry)
    success, msg = writecsvfile(csvlist, fileloc)
    return success, filename, fileloc, msg

def getPSStudents():
    myDB = database.DBConnection()
    studentlist = myDB.select("SELECT people.username as peoplekey, ps_students.* from people, ps_students WHERE people.identifier = ps_students.studentid")
    csvlist = []
    userlist = []
    cacheLocation = schoolkeeper.CACHEDIR
    if not os.path.exists(cacheLocation):
        os.mkdir(cacheLocation)
    filename='ps_students.csv'
    fileloc = os.path.join(cacheLocation, filename)
    today = currentTime()
    for item in studentlist:
        entry = {}
        for key in item.keys():
            entry[key] = item[key]
        csvlist.append(entry)
    success, msg = writecsvfile(csvlist, fileloc)
    return success, filename, fileloc, msg


def getPSStaff():
    myDB = database.DBConnection()
    studentlist = myDB.select("SELECT people.username as peoplekey, ps_staff.* from people, ps_staff WHERE people.identifier = ps_staff.psid")
    csvlist = []
    userlist = []
    cacheLocation = schoolkeeper.CACHEDIR
    if not os.path.exists(cacheLocation):
        os.mkdir(cacheLocation)
    filename='ps_staff.csv'
    fileloc = os.path.join(cacheLocation, filename)
    today = currentTime()
    for item in studentlist:
        entry = {}
        for key in item.keys():
            entry[key] = item[key]
        csvlist.append(entry)
    success, msg = writecsvfile(csvlist, fileloc)
    return success, filename, fileloc, msg




def getSchools():
    myDB = database.DBConnection()
    studentlist = myDB.select("SELECT * from schools")
    csvlist = []
    userlist = []
    cacheLocation = schoolkeeper.CACHEDIR
    if not os.path.exists(cacheLocation):
        os.mkdir(cacheLocation)
    filename='schools.csv'
    fileloc = os.path.join(cacheLocation, filename)
    for item in studentlist:
        entry = {}
        for key in item.keys():
            entry[key] = item[key]
        csvlist.append(entry)
    success, msg = writecsvfile(csvlist, fileloc)
    return success, filename, fileloc, msg


def getDeviceModels():
    myDB = database.DBConnection()
    modellist = myDB.select("SELECT * from device_models")
    csvlist = []
    cacheLocation = schoolkeeper.CACHEDIR
    if not os.path.exists(cacheLocation):
        os.mkdir(cacheLocation)
    filename = 'devicemodels.csv'
    fileloc = os.path.join(cacheLocation, filename)
    for item in modellist:
        entry = {}
        for key in item.keys():
            entry[key] = item[key]
        csvlist.append(entry)
    success, msg = writecsvfile(csvlist, fileloc)
    return success, filename, fileloc, msg

def getDevices():
    myDB = database.DBConnection()
    devicelist = myDB.select("SELECT devices.*, device_models.modelname, people.username from devices LEFT JOIN people ON devices.ownerid = people.id LEFT JOIN device_models ON device_models.modelid = devices.modelid")
    csvlist = []
    cacheLocation = schoolkeeper.CACHEDIR
    if not os.path.exists(cacheLocation):
        os.mkdir(cacheLocation)
    filename = 'devices.csv'
    fileloc = os.path.join(cacheLocation, filename)
    for item in devicelist:
        entry = {}
        for key in item.keys():
            entry[key] = item[key]
        csvlist.append(entry)
    success, msg = writecsvfile(csvlist, fileloc)
    return success, filename, fileloc, msg

def getLocations():
    myDB = database.DBConnection()
    resultlist = myDB.select("SELECT * from locations")
    csvlist = []
    cacheLocation = schoolkeeper.CACHEDIR
    if not os.path.exists(cacheLocation):
        os.mkdir(cacheLocation)
    filename = 'locations.csv'
    fileloc = os.path.join(cacheLocation, filename)
    for item in resultlist:
        entry = {}
        for key in item.keys():
            entry[key] = item[key]
        csvlist.append(entry)
    success, msg = writecsvfile(csvlist, fileloc)
    return success, filename, fileloc, msg

def getISPROUTStudents(): # Unfinished
    myDB = database.DBConnection()
    resultlist = myDB.select("SELECT * from ps_students WHERE gradelevel = -1")
    csvlist = []
    cacheLocation = schoolkeeper.CACHEDIR
    if not os.path.exists(cacheLocation):
        os.mkdir(cacheLocation)
    filename = 'ps_preschoolers.csv'
    fileloc = os.path.join(cacheLocation, filename)
    for item in resultlist:
        entry = {}
        for key in item.keys():
            entry[key] = item[key]
        csvlist.append(entry)
    success, msg = writecsvfile(csvlist, fileloc)
    return success, filename, fileloc, msg

