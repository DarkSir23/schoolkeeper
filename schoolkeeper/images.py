import os
import schoolkeeper
from schoolkeeper import logger, cache, dblogger, dbactions, database
from shutil import copyfile, copy
import zipfile


def pictureImport(session=None, firstline='', lastline='', filehash=None, **kwargs):
    _session = session
    myDB = database.DBConnection()
    msg = ""
    if filehash:
        logger.debug("[PICTUREIMPORT] File present, process file lines")
        importlist, success = cache.cache_get(filehash=filehash, type='CSV')
        if success:
            for index in xrange(int(firstline), int(lastline ) +1):
                person = None
                personsearch = {}
                for subindex in xrange(0, len(importlist[index])):
                    colid = kwargs['col_%s' % subindex].encode('utf-8')
                    currentitem = importlist[index][subindex].encode('utf-8')
                    if colid == 'STUDENT_ID' or colid == 'STAFF_ID':
                        personsearch['identifier'] = currentitem
                    elif colid == 'INTERNAL_ID':
                        personsearch['id'] = currentitem
                    elif colid == 'FILENAME':
                        personsearch['filename'] = currentitem
                if 'id' in personsearch.keys() and len(personsearch['id']) > 1:
                    person = myDB.match('SELECT * from people WHERE id=?', (personsearch['id'],))
                elif 'identifier' in personsearch.keys() and len(personsearch['identifier']) > 1:
                    person = myDB.match('SELECT * from people WHERE identifier=?', (personsearch['identifier'],))
                if not person or len(person) == 0:
                    msg += 'Line %s not imported: Person not found<br>' % index
                    continue
                if 'filename' in personsearch.keys():
                    filename = os.path.join(schoolkeeper.CONFIG['FOLDER_OTHER_IMPORT'], 'images', personsearch['filename'])
                    if os.path.exists(filename) and os.path.isfile(filename):
                        filebase, fileext = os.path.splitext(filename)
                        newfilename = os.path.join(schoolkeeper.CONFIG['IMAGEDIR'], person['id'] + fileext)
                        for ext in ['.jpg', '.JPG', '.png', '.PNG']:
                            checkfilename = os.path.join(schoolkeeper.CONFIG['IMAGEDIR'],person['id'] + ext)
                            if os.path.exists(checkfilename):
                                os.remove(checkfilename)
                        try:
                            os.rename(filename, newfilename)
                            dblogger.person("Image imported.", _session['user'], person['id'])
                            personname = dbactions.getName(id=person['id'])
                            dblogger.user("Imported image for %s" % personname, _session['user'], _session['user'])
                            msg += 'Line %s imported: %s<br>' % (index, personname)
                        except Exception as e:
                            msg += 'Line %s not imported: %s<br>' % (index, e)
                    else:
                        msg += 'Line %s not imported: File not found: %s<br>' % (index, filename)
                else:
                    msg += 'Line %s not imported: No filename specificed.<br>'

    else:
        logger.debug('[PICTUREIMPORT] File not present.  Attempting to import files.')
        imagepath = os.path.join(schoolkeeper.CONFIG['FOLDER_OTHER_IMPORT'], 'images')
        if os.path.exists(imagepath):
            for (dirpath, dirnames, filenames) in os.walk(imagepath):
                logger.debug('[PICTUREIMPORT] %s - %s - %s' % (dirpath, dirnames, filenames))
                for basename in filenames:
                    filename = os.path.join(dirpath, basename)
                    identifier, extension = os.path.splitext(basename)
                    try:
                        search = int(identifier)
                        person = myDB.match('SELECT * from people where identifier=?', (search,))
                    except:
                        person = myDB.match('SELECT * from people where id=?', (identifier,))
                    if not person or len(person) == 0:
                        msg += 'File %s not imported:  Person not found</br>' % basename
                        continue
                    newfilename = os.path.join(schoolkeeper.CONFIG['IMAGEDIR'], person['id'] + extension)
                    if os.path.exists(newfilename):
                        os.remove(newfilename)
                    try:
                        os.rename(filename, newfilename)
                        dblogger.person("Image imported.", _session['user'], person['id'])
                        personname = dbactions.getName(id=person['id'])
                        dblogger.user("Imported image for %s" % personname, _session['user'], _session['user'])
                        msg += 'File %s imported: %s<br>' % (basename, personname)
                    except Exception as e:
                        msg += 'File %s not imported: %s<br>' % (basename, e)
        else:
            msg += 'Import Path Missing'
    return msg


def pictureUpload(session=None, id=None, uploadfile=None):
    _session = session
    msg = ""
    if id and uploadfile:
        filename = uploadfile.filename
        filebase, extension = os.path.splitext(filename)
        newfilename = os.path.join(schoolkeeper.CONFIG['IMAGEDIR'], id + extension)
        if os.path.exists(newfilename):
            logger.debug('[UPLOADFILE] Deleting existing image')
            os.remove(newfilename)
        result = ''
        while True:
            data = uploadfile.file.read(8192)
            if not data:
                break
            result += data
        try:
            with open(newfilename, "w") as outfile:
                outfile.write(result)
            msg += 'Image uploaded'
        except Exception as e:
            msg += 'Image NOT uploaded'
    return msg

def pictureExport(session=None, personlist=[], format="IDENTIFIER"):
    _session = session
    msg = ""
    filemap = ""
    destinymap = ""
    myDB = database.DBConnection()
    exportfolder = os.path.join(schoolkeeper.CONFIG['IMAGEDIR'], 'export')
    if not os.path.exists(exportfolder):
        os.mkdir(exportfolder)
    clearfiles = os.listdir(exportfolder)
    for f in clearfiles:
        if os.path.isfile(os.path.join(exportfolder, f)):
            os.remove(os.path.join(exportfolder, f))
    tempfolder = os.path.join(exportfolder, 'temp')
    if not os.path.exists(tempfolder):
        os.mkdir(tempfolder)
    clearfiles = os.listdir(tempfolder)
    for f in clearfiles:
        if os.path.isfile(os.path.join(tempfolder, f)):
            os.remove(os.path.join(tempfolder, f))
    for id in personlist:
        person = myDB.match('SELECT * from people WHERE id=?', (id,))
        filename = None
        myextension = None
        for extension in ['.jpg', '.JPG', '.png', '.PNG']:
            if os.path.exists(os.path.join(schoolkeeper.CONFIG['IMAGEDIR'],id + extension)):
                filename = os.path.join(schoolkeeper.CONFIG['IMAGEDIR'],id + extension)
                myextension = extension
                # logger.debug('Filename Found: %s' % filename)
        if filename:
            newname = id
            if format == "IDENTIFIER":
                newname = str(person['identifier'])
            newfilename = os.path.join(tempfolder, newname + myextension.lower())
            filemap += str(person['identifier'])+'\t'+ os.path.basename(newfilename) + '\n'
            destinymap += str(person['identifier']) +',' + os.path.basename(newfilename) + '\n'
            try:
                copyfile(filename, newfilename)
            except Exception as e:
                logger.debug('[PICTUREEXPORT] Error: %s' % e)
    map_file = open(os.path.join(tempfolder, 'picturemap.txt'), "w")
    map_file.write(filemap)
    map_file.close()
    destiny_file = open(os.path.join(tempfolder,'idlink.txt'), "w")
    destiny_file.write(destinymap)
    destiny_file.close()
    filename = os.path.join(exportfolder, 'export.zip')
    zipf = zipfile.ZipFile(filename, 'w', zipfile.ZIP_DEFLATED)
    zipFolder(tempfolder, zipf)
    zipf.close()
    return filename

def getPictureFilename(person):
    filename = None
    for extension in ['.jpg', '.JPG', '.png', '.PNG']:
        if os.path.exists(os.path.join(schoolkeeper.CONFIG['IMAGEDIR'], person + extension)):
            filename = os.path.join(schoolkeeper.CONFIG['IMAGEDIR'], person + extension)
    return filename

def zipFolder(path, zipf):
    for (dirpath, dirnames, filenames) in os.walk(path):
        for f in filenames:
            zipf.write(os.path.join(dirpath, f),f)
