
from __future__ import with_statement

import datetime
import os
import shutil
import threading
import time
import traceback
import psycopg2

import schoolkeeper
from schoolkeeper import logger, database
from schoolkeeper.common import restartJobs, currentTime



def upgrade_needed():
    """
    Check if database needs upgrading
    Return zero if up-to-date
    Return current version if needs upgrade
    """

    myDB = database.DBConnection()
    db_version = int(myDB.get_version())

    # database version history:
    # 0 original version or new empty database
    info = has_column(myDB, 'ad_students', 'username')
    db_current_version = 26
    if db_version < db_current_version:
        return db_current_version
    return 0


def has_column(myDB, table, column):
    if schoolkeeper.CONFIG['DB_TYPE'] == 'SQLITE3':
        columns = myDB.select('PRAGMA table_info(%s)' % table)
        itemnumber = 1
    elif schoolkeeper.CONFIG['DB_TYPE'] == 'POSTGRESQL':
        columns = myDB.select("SELECT column_name from information_schema.columns where table_name='%s';" % table )
        itemnumber = 0
    if not columns:  # no such table
        return False
    for item in columns:
        if item[itemnumber] == column:
            return True
    # no such column
    return False

def index_key():
    if schoolkeeper.CONFIG['DB_TYPE'] == 'SQLITE3':
        return 'INTEGER PRIMARY KEY AUTOINCREMENT'
    elif schoolkeeper.CONFIG['DB_TYPE'] == 'POSTGRESQL':
        return 'SERIAL PRIMARY KEY'


def dbupgrade(db_current_version):
    logger.debug("Cur: %s" % db_current_version)
    with open(os.path.join(schoolkeeper.CONFIG['LOGDIR'], 'dbupgrade.log'), 'a') as upgradelog:
        try:
            myDB = database.DBConnection()
            db_version = int(myDB.get_version())
            logger.debug("Db: %s" % db_version)
            if schoolkeeper.CONFIG['DB_TYPE'] == 'SQLITE3':
                check = myDB.match('PRAGMA integrity_check')
                if check and check[0]:
                    result = check[0]
                    if result == 'ok':
                        logger.debug('Database integrity check: %s' % result)
                    else:
                        logger.error('Database integrity check: %s' % result)
                        # should probably abort now

            if db_version < db_current_version:
                myDB = database.DBConnection(autocommit=True)
                if db_version:
                    schoolkeeper.UPDATE_MSG = 'Updating database to version %s, current version is %s' % (
                        db_current_version, db_version)
                    logger.info(schoolkeeper.UPDATE_MSG)
                    upgradelog.write("%s v0: %s\n" % (time.ctime(), schoolkeeper.UPDATE_MSG))
                else:
                    if not has_column(myDB, "users", "UserID"):
                        # it's a new database. Create tables but no need for any upgrading
                        db_version = db_current_version
                        schoolkeeper.UPDATE_MSG = 'Creating new database, version %s' % db_version
                        upgradelog.write("%s v0: %s\n" % (time.ctime(), schoolkeeper.UPDATE_MSG))
                        logger.info(schoolkeeper.UPDATE_MSG)
                    myDB.action('CREATE TABLE IF NOT EXISTS checksums (id TEXT PRIMARY KEY, checksum TEXT)')

                    myDB.action('CREATE TABLE IF NOT EXISTS personactivity (timestamp TEXT, personid TEXT,  \
                        activitydate TEXT, activitydesc TEXT, username TEXT)')

                    myDB.action('CREATE TABLE IF NOT EXISTS deviceactivity (TimeStamp TEXT, deviceid int, \
                        activitydate TEXT, activitydesc TEXT, username TEXT)')

                    myDB.action('CREATE TABLE IF NOT EXISTS users (userid TEXT PRIMARY KEY, interface TEXT, interfacelook TEXT, bootstrap4look TEXT)')

                    myDB.action('CREATE TABLE IF NOT EXISTS useractivity (timestamp TEXT, userid text, activitydate TEXT, \
                                activitydesc TEXT, username TEXT)')

                    myDB.action('CREATE TABLE IF NOT EXISTS ad_students (firstname TEXT, lastname TEXT, displayname TEXT, \
                                username TEXT PRIMARY KEY, description TEXT, department TEXT, email TEXT, accountstatus TEXT, \
                                lastlogon TEXT, distinguishedname TEXT)')

                    myDB.action('CREATE TABLE IF NOT EXISTS ad_staff (firstname TEXT, lastname TEXT, displayname TEXT, \
                                username TEXT PRIMARY KEY, description TEXT, department TEXT, email TEXT, accountstatus TEXT, \
                                lastlogon TEXT, distinguishedname TEXT)')

                    myDB.action('CREATE TABLE IF NOT EXISTS ps_students (PSID INTEGER PRIMARY KEY, StudentID INTEGER UNIQUE, \
                                LastName TEXT, FirstName TEXT, StudentEmail TEXT, PSUserName TEXT, PSPassword TEXT, \
                                SchoolID INTEGER, DOB TEXT, GradeLevel INTEGER, STN TEXT, Gender TEXT, \
                                EthnicityCode INTEGER, CafePIN INTEGER, insurance_decline INTEGER, insurance_form INTEGER, insurance_pmt TEXT, details TEXT)')

                    myDB.action('CREATE TABLE IF NOT EXISTS ps_student_override (PSID INTEGER PRIMARY KEY, \
                                ForceMiddleInitial TEXT, ForceLastName TEXT, ForceFirstName TEXT)')

                    myDB.action('CREATE TABLE IF NOT EXISTS device_models (ModelID %s, ModelName TEXT, \
                                ModelManufacturer TEXT, ModelType TEXT, ModelDescription TEXT)' % (index_key()))

                    myDB.action('CREATE TABLE IF NOT EXISTS entities (EntityID %s, \
                                Identifier INTEGER, EntityType TEXT)' % (index_key()))

                    myDB.action('CREATE TABLE IF NOT EXISTS devices (DeviceID %s, \
                                ModelID INTEGER, OwnerID TEXT, SerialNumber TEXT, Tag TEXT, MAC Text, ADName TEXT, \
                                Purchased TEXT, Notes TEXT, gdeviceid TEXT UNIQUE, locationid INTEGER, retired INTEGER)' % (index_key()))

                    myDB.action('CREATE TABLE IF NOT EXISTS schools (schoolid INTEGER PRIMARY KEY, schoolname TEXT, schoolabbrev TEXT, pearsonid TEXT, pearsonpassword TEXT, details TEXT)')

                    myDB.action('CREATE TABLE IF NOT EXISTS classes (ClassID INTEGER PRIMARY KEY, SchoolID INTEGER, \
                                StaffID INTEGER, ClassName TEXT, Period TEXT, FirstDay TEXT, LastDay TEXT, Room TEXT, \
                                GradeLevel INTEGER, Term TEXT, statecourseid INTEGER)')

                    myDB.action('CREATE TABLE IF NOT EXISTS schedules (PSID INTEGER, SchoolID INTEGER, ClassID INTEGER)')

                    myDB.action('CREATE TABLE IF NOT EXISTS ps_staff (PSID INTEGER PRIMARY KEY, SchoolID INTEGER, LastName TEXT, \
                                FirstName TEXT, Email TEXT, FirstDotLast TEXT, PSUserName TEXT, Status INTEGER, SPN INTEGER, \
                                Title TEXT, teachernumber INTEGER)')

                    myDB.action('CREATE TABLE IF NOT EXISTS ps_staff_override (PSID INTEGER PRIMARY KEY, Ignore INTEGER, \
                                ForceFirstName TEXT, ForceLastName TEXT, ForceUserName TEXT)')

                    myDB.action('CREATE TABLE IF NOT EXISTS people (id TEXT PRIMARY KEY, identifier INTEGER UNIQUE, \
                                lastname TEXT, firstName TEXT, username TEXT, email TEXT, ignore INTEGER, schoolid INTEGER)')

                    myDB.action('CREATE TABLE IF NOT EXISTS device_cache (deviceid %s, tag TEXT, type TEXT, ownername TEXT, \
                                modeltype TEXT, modelname TEXT, serialnumber TEXT, adname TEXT, mac TEXT, purchased TEXT, notes TEXT, schoolabbrev TEXT, locationname TEXT, retired INTEGER)' % (index_key()))

                    myDB.action('CREATE TABLE IF NOT EXISTS locations (locationid %s, locationname TEXT, schoolid INTEGER)' % (index_key()))

                    myDB.action('CREATE TABLE IF NOT EXISTS accountlink (childid TEXT PRIMARY KEY, parentid TEXT)')

                    myDB.action('CREATE TABLE IF NOT EXISTS attachments (attachmentid TEXT PRIMARY KEY, filename TEXT, description TEXT, devices TEXT, people TEXT, timestamp TEXT)')

                # These are the incremental changes before database versioning was introduced.
                # Old database tables might already have these incorporated depending on version, so we need to check...
                if db_version < 4:
                    schoolkeeper.UPDATE_MSG = 'Updating database to version %s, current version is %s' % (db_current_version, db_version)
                    logger.info(schoolkeeper.UPDATE_MSG)
                    upgradelog.write("%s v2: %s\n" % (time.ctime(), schoolkeeper.UPDATE_MSG))

                    if has_column(myDB, "ps_student_archive", "lastname"):
                        schoolkeeper.UPDATE_MSG = 'Replacing ps_student_archive and staff_archive with people.'
                        logger.debug(schoolkeeper.UPDATE_MSG)
                        upgradelog.write("%s V4: %s\n" % (time.ctime(), schoolkeeper.UPDATE_MSG))
                        myDB.action('DROP TABLE ps_student_archive')
                        myDB.action('DROP TABLE staff_archive')
                        myDB.action('CREATE TABLE IF NOT EXISTS people (id INTEGER PRIMARY KEY, identifier INTEGER UNIQUE, \
                                    lastname TEXT, firstName TEXT, username TEXT, email TEXT, ignore INTEGER)')

                if db_version < 5:
                    schoolkeeper.UPDATE_MSG = 'Updating database to version %s, current version is %s' % (db_current_version, db_version)
                    logger.info(schoolkeeper.UPDATE_MSG)
                    upgradelog.write("%s v5: %s\n" % (time.ctime(), schoolkeeper.UPDATE_MSG))

                    if has_column(myDB, "people", "id"):
                        schoolkeeper.UPDATE_MSG = 'Replacing people table with new primary key.'
                        logger.debug(schoolkeeper.UPDATE_MSG)
                        upgradelog.write("%s V5: %s\n" % (time.ctime(), schoolkeeper.UPDATE_MSG))
                        myDB.action('DROP TABLE people')
                        myDB.action('CREATE TABLE IF NOT EXISTS people (id TEXT PRIMARY KEY, identifier INTEGER UNIQUE, \
                                    lastname TEXT, firstName TEXT, username TEXT, email TEXT, ignore INTEGER)')

                if db_version < 6:
                    schoolkeeper.UPDATE_MSG = 'Updating database to version %s, current version is %s' % (db_current_version, db_version)
                    logger.info(schoolkeeper.UPDATE_MSG)
                    upgradelog.write("%s v2: %s\n" % (time.ctime(), schoolkeeper.UPDATE_MSG))

                    if has_column(myDB, "studentactivity", "studentid"):
                        schoolkeeper.UPDATE_MSG = 'Updating dblogging tables.'
                        logger.debug(schoolkeeper.UPDATE_MSG)
                        upgradelog.write("%s V6: %s\n" % (time.ctime(), schoolkeeper.UPDATE_MSG))
                        myDB.action('DROP TABLE studentactivity')
                        myDB.action('DROP TABLE staffactivity')
                        myDB.action('DROP TABLE useractivity')
                        myDB.action('CREATE TABLE IF NOT EXISTS personactivity (timestamp TEXT, personid TEXT,  \
                            activitydate TEXT, activitydesc TEXT, username TEXT)')
                        myDB.action('CREATE TABLE IF NOT EXISTS deviceactivity (TimeStamp TEXT, deviceid int, \
                            activitydate TEXT, activitydesc TEXT, username TEXT)')
                        myDB.action('CREATE TABLE IF NOT EXISTS useractivity (timestamp TEXT, userid text, activitydate TEXT, \
                                    activitydesc TEXT, username TEXT)')


                if db_version < 7:
                    schoolkeeper.UPDATE_MSG = 'Updating database to version %s, current version is %s' % (
                    db_current_version, db_version)
                    logger.info(schoolkeeper.UPDATE_MSG)
                    upgradelog.write("%s v2: %s\n" % (time.ctime(), schoolkeeper.UPDATE_MSG))

                    if not has_column(myDB, "ad_staff", "username"):
                        schoolkeeper.UPDATE_MSG = 'Adding ad_staff tables.'
                        logger.debug(schoolkeeper.UPDATE_MSG)
                        upgradelog.write("%s V7: %s\n" % (time.ctime(), schoolkeeper.UPDATE_MSG))

                        myDB.action('CREATE TABLE IF NOT EXISTS ad_staff (firstname TEXT, lastname TEXT, displayname TEXT, \
                                    username TEXT PRIMARY KEY, description TEXT, department TEXT, email TEXT, accountstatus TEXT, \
                                    lastlogon TEXT, distinguishedname TEXT)')

                if db_version < 8:
                    schoolkeeper.UPDATE_MSG = 'Updating database to version %s, current version is %s' % (
                    db_current_version, db_version)
                    logger.info(schoolkeeper.UPDATE_MSG)
                    upgradelog.write("%s v2: %s\n" % (time.ctime(), schoolkeeper.UPDATE_MSG))

                    if not has_column(myDB, "schools", "schoolabbrev"):
                        schoolkeeper.UPDATE_MSG = 'Adding school abbreviation column.'
                        logger.debug(schoolkeeper.UPDATE_MSG)
                        upgradelog.write("%s V8: %s\n" % (time.ctime(), schoolkeeper.UPDATE_MSG))
                        myDB.action('ALTER TABLE schools ADD schoolabbrev TEXT')

                if db_version < 9:
                    schoolkeeper.UPDATE_MSG = 'Updating database to version %s, current version is %s' % (
                    db_current_version, db_version)
                    logger.info(schoolkeeper.UPDATE_MSG)
                    upgradelog.write("%s v9: %s\n" % (time.ctime(), schoolkeeper.UPDATE_MSG))
                    schoolkeeper.UPDATE_MSG = 'Changing device owner column to TEXT'
                    logger.debug(schoolkeeper.UPDATE_MSG)
                    upgradelog.write("%s V9: %s\n" % (time.ctime(), schoolkeeper.UPDATE_MSG))
                    myDB.action('ALTER TABLE devices ALTER COLUMN ownerid TYPE TEXT')

                if db_version < 10:
                    schoolkeeper.UPDATE_MSG = 'Updating database to version %s, current version is %s' % (
                    db_current_version, db_version)
                    logger.info(schoolkeeper.UPDATE_MSG)
                    upgradelog.write("%s v10: %s\n" % (time.ctime(), schoolkeeper.UPDATE_MSG))
                    schoolkeeper.UPDATE_MSG = 'Rebuilding User Table'
                    logger.debug(schoolkeeper.UPDATE_MSG)
                    upgradelog.write("%s V10: %s\n" % (time.ctime(), schoolkeeper.UPDATE_MSG))
                    myDB.action('DROP TABLE users')
                    myDB.action('CREATE TABLE IF NOT EXISTS users (userid TEXT PRIMARY KEY, interfacelook TEXT)')

                if db_version < 11:
                    schoolkeeper.UPDATE_MSG = 'Updating database to version %s, current version is %s' % (
                    db_current_version, db_version)
                    logger.info(schoolkeeper.UPDATE_MSG)
                    upgradelog.write("%s v11: %s\n" % (time.ctime(), schoolkeeper.UPDATE_MSG))
                    schoolkeeper.UPDATE_MSG = 'Adding google ID to devices'
                    logger.debug(schoolkeeper.UPDATE_MSG)
                    upgradelog.write("%s V11: %s\n" % (time.ctime(), schoolkeeper.UPDATE_MSG))
                    myDB.action('ALTER TABLE devices ADD gdeviceid TEXT UNIQUE')

                if db_version < 12:
                    schoolkeeper.UPDATE_MSG = 'Updating database to version %s, current version is %s' % (
                    db_current_version, db_version)
                    logger.info(schoolkeeper.UPDATE_MSG)
                    upgradelog.write("%s v12: %s\n" % (time.ctime(), schoolkeeper.UPDATE_MSG))
                    schoolkeeper.UPDATE_MSG = 'Changing STN column to TEXT'
                    logger.debug(schoolkeeper.UPDATE_MSG)
                    upgradelog.write("%s V12: %s\n" % (time.ctime(), schoolkeeper.UPDATE_MSG))
                    myDB.action('ALTER TABLE ps_students ALTER COLUMN stn TYPE TEXT')

                if db_version < 13:
                    schoolkeeper.UPDATE_MSG = 'Updating database to version %s, current version is %s' % (
                        db_current_version, db_version)
                    logger.info(schoolkeeper.UPDATE_MSG)
                    upgradelog.write("%s v13: %s\n" % (time.ctime(), schoolkeeper.UPDATE_MSG))
                    schoolkeeper.UPDATE_MSG = 'Adding device_cache table'
                    logger.debug(schoolkeeper.UPDATE_MSG)
                    upgradelog.write("%s V13: %s\n" % (time.ctime(), schoolkeeper.UPDATE_MSG))
                    myDB.action('CREATE TABLE IF NOT EXISTS device_cache (deviceid %s, tag TEXT, ownertype TEXT, ownername TEXT, \
                                modeltype TEXT, modelname TEXT, serialnumber TEXT, adname TEXT, mac TEXT, purchased TEXT, notes TEXT)' % (index_key()))

                if db_version < 14:
                    schoolkeeper.UPDATE_MSG = 'Updating database to version %s, current version is %s' % (
                        db_current_version, db_version)
                    logger.info(schoolkeeper.UPDATE_MSG)
                    upgradelog.write("%s v14: %s\n" % (time.ctime(), schoolkeeper.UPDATE_MSG))
                    schoolkeeper.UPDATE_MSG = 'Adding location table - school and location entries to devices'
                    logger.debug(schoolkeeper.UPDATE_MSG)
                    upgradelog.write("%s V14: %s\n" % (time.ctime(), schoolkeeper.UPDATE_MSG))
                    myDB.action('ALTER TABLE people ADD schoolid INTEGER')
                    myDB.action('ALTER TABLE devices ADD locationid INTEGER')
                    myDB.action('ALTER TABLE device_cache ADD locationname TEXT')
                    myDB.action('CREATE TABLE IF NOT EXISTS locations (locationid %s, locationname TEXT, schoolid INTEGER)' % (index_key()))

                if db_version < 15:
                    schoolkeeper.UPDATE_MSG = 'Updating database to version %s, current version is %s' % (
                        db_current_version, db_version)
                    logger.info(schoolkeeper.UPDATE_MSG)
                    upgradelog.write("%s v15: %s\n" % (time.ctime(), schoolkeeper.UPDATE_MSG))
                    schoolkeeper.UPDATE_MSG = 'Adding schoolabbrev to device_cache'
                    logger.debug(schoolkeeper.UPDATE_MSG)
                    upgradelog.write("%s V15: %s\n" % (time.ctime(), schoolkeeper.UPDATE_MSG))
                    myDB.action('ALTER TABLE device_cache ADD schoolabbrev TEXT')

                if db_version < 16:
                    schoolkeeper.UPDATE_MSG = 'Updating database to version %s, current version is %s' % (
                        db_current_version, db_version)
                    logger.info(schoolkeeper.UPDATE_MSG)
                    upgradelog.write("%s v16: %s\n" % (time.ctime(), schoolkeeper.UPDATE_MSG))
                    schoolkeeper.UPDATE_MSG = 'Creating accountlink table'
                    logger.debug(schoolkeeper.UPDATE_MSG)
                    upgradelog.write("%s V16: %s\n" % (time.ctime(), schoolkeeper.UPDATE_MSG))
                    myDB.action('CREATE TABLE IF NOT EXISTS accountlink (childid TEXT PRIMARY KEY, parentid TEXT)')

                if db_version < 17:
                    schoolkeeper.UPDATE_MSG = 'Updating database to version %s, current version is %s' % (
                    db_current_version, db_version)
                    logger.info(schoolkeeper.UPDATE_MSG)
                    upgradelog.write("%s v17: %s\n" % (time.ctime(), schoolkeeper.UPDATE_MSG))
                    schoolkeeper.UPDATE_MSG = 'Adding retired flag to devices'
                    logger.debug(schoolkeeper.UPDATE_MSG)
                    upgradelog.write("%s V17: %s\n" % (time.ctime(), schoolkeeper.UPDATE_MSG))
                    myDB.action('ALTER TABLE devices ADD retired INTEGER')
                    myDB.action('ALTER TABLE device_cache ADD retired INTEGER')

                if db_version < 19:
                    schoolkeeper.UPDATE_MSG = 'Updating database to version %s, current version is %s' % (
                        db_current_version, db_version)
                    logger.info(schoolkeeper.UPDATE_MSG)
                    upgradelog.write("%s v19: %s\n" % (time.ctime(), schoolkeeper.UPDATE_MSG))
                    schoolkeeper.UPDATE_MSG = 'Attachments Database'
                    logger.debug(schoolkeeper.UPDATE_MSG)
                    upgradelog.write("%s V19: %s\n" % (time.ctime(), schoolkeeper.UPDATE_MSG))
                    myDB.action('CREATE TABLE IF NOT EXISTS attachments (attachmentid TEXT PRIMARY KEY, filename TEXT, description TEXT, devices TEXT, people TEXT, timestamp TEXT)')

                if db_version < 20:
                    schoolkeeper.UPDATE_MSG = 'Updating database to version %s, current version is %s' % (
                        db_current_version, db_version)
                    logger.info(schoolkeeper.UPDATE_MSG)
                    upgradelog.write("%s v20: %s\n" % (time.ctime(), schoolkeeper.UPDATE_MSG))
                    schoolkeeper.UPDATE_MSG = 'Adding pearsonid to schools'
                    logger.debug(schoolkeeper.UPDATE_MSG)
                    upgradelog.write("%s V20: %s\n" % (time.ctime(), schoolkeeper.UPDATE_MSG))
                    myDB.action('ALTER TABLE schools ADD pearsonid TEXT')

                if db_version < 21:
                    schoolkeeper.UPDATE_MSG = 'Updating database to version %s, current version is %s' % (
                        db_current_version, db_version)
                    logger.info(schoolkeeper.UPDATE_MSG)
                    upgradelog.write("%s v21: %s\n" % (time.ctime(), schoolkeeper.UPDATE_MSG))
                    schoolkeeper.UPDATE_MSG = 'Adding pearsonpassword to schools'
                    logger.debug(schoolkeeper.UPDATE_MSG)
                    upgradelog.write("%s V21: %s\n" % (time.ctime(), schoolkeeper.UPDATE_MSG))
                    myDB.action('ALTER TABLE schools ADD pearsonpassword TEXT')

                if db_version < 22:
                    schoolkeeper.UPDATE_MSG = 'Updating database to version %s, current version is %s' % (
                        db_current_version, db_version)
                    logger.info(schoolkeeper.UPDATE_MSG)
                    upgradelog.write("%s v22: %s\n" % (time.ctime(), schoolkeeper.UPDATE_MSG))
                    schoolkeeper.UPDATE_MSG = 'Adding insurance info to PS_Students'
                    logger.debug(schoolkeeper.UPDATE_MSG)
                    upgradelog.write("%s V22: %s\n" % (time.ctime(), schoolkeeper.UPDATE_MSG))
                    myDB.action('ALTER TABLE ps_students ADD insurance_decline INTEGER')
                    myDB.action('ALTER TABLE ps_students ADD insurance_form INTEGER')
                    myDB.action('ALTER TABLE ps_students ADD insurance_pmt TEXT')

                if db_version < 23:
                    schoolkeeper.UPDATE_MSG = 'Updating database to version %s, current version is %s' % (
                        db_current_version, db_version)
                    logger.info(schoolkeeper.UPDATE_MSG)
                    upgradelog.write("%s v23: %s\n" % (time.ctime(), schoolkeeper.UPDATE_MSG))
                    schoolkeeper.UPDATE_MSG = 'Adding other details to PS_Students'
                    logger.debug(schoolkeeper.UPDATE_MSG)
                    upgradelog.write("%s V23: %s\n" % (time.ctime(), schoolkeeper.UPDATE_MSG))
                    myDB.action('ALTER TABLE ps_students ADD details TEXT')

                if db_version < 24:
                    schoolkeeper.UPDATE_MSG = 'Updating database to version %s, current version is %s' % (
                        db_current_version, db_version)
                    logger.info(schoolkeeper.UPDATE_MSG)
                    upgradelog.write("%s v24: %s\n" % (time.ctime(), schoolkeeper.UPDATE_MSG))
                    schoolkeeper.UPDATE_MSG = 'Adding course ID and details to schools'
                    logger.debug(schoolkeeper.UPDATE_MSG)
                    upgradelog.write("%s V24: %s\n" % (time.ctime(), schoolkeeper.UPDATE_MSG))
                    myDB.action('ALTER TABLE classes ADD statecourseid INTEGER')
                    myDB.action('ALTER TABLE schools ADD details TEXT')

                if db_version < 25:
                    schoolkeeper.UPDATE_MSG = 'Updating database to version %s, current version is %s' % (
                        db_current_version, db_version)
                    logger.info(schoolkeeper.UPDATE_MSG)
                    upgradelog.write("%s v25: %s\n" % (time.ctime(), schoolkeeper.UPDATE_MSG))
                    schoolkeeper.UPDATE_MSG = 'Adding Bootstrap 4 Options'
                    logger.debug(schoolkeeper.UPDATE_MSG)
                    upgradelog.write("%s V25: %s\n" % (time.ctime(), schoolkeeper.UPDATE_MSG))
                    myDB.action('ALTER TABLE users ADD interface TEXT')
                    myDB.action('ALTER TABLE users ADD bootstrap4look TEXT')

                if db_version < 26:
                    schoolkeeper.UPDATE_MSG = 'Updating database to version %s, current version is %s' % (
                        db_current_version, db_version)
                    logger.info(schoolkeeper.UPDATE_MSG)
                    upgradelog.write("%s v26: %s\n" % (time.ctime(), schoolkeeper.UPDATE_MSG))
                    schoolkeeper.UPDATE_MSG = 'Adding TeacherNumber to ps_staff'
                    upgradelog.write("%s V26: %s\n" % (time.ctime(), schoolkeeper.UPDATE_MSG))
                    myDB.action('ALTER TABLE ps_staff ADD teachernumber INTEGER')



                myDB.set_version(db_current_version)
                schoolkeeper.UPDATE_MSG = 'Cleaning Database'
                upgradelog.write("%s: %s\n" % (time.ctime(), schoolkeeper.UPDATE_MSG))
                myDB.action('vacuum')
                schoolkeeper.UPDATE_MSG = 'Database upgraded to version %s' % db_current_version
                logger.info(schoolkeeper.UPDATE_MSG)
                upgradelog.write("%s: %s\n" % (time.ctime(), schoolkeeper.UPDATE_MSG))

                restartJobs(start='Start')

            schoolkeeper.UPDATE_MSG = ''

        except Exception:
            msg = 'Unhandled exception in database upgrade: %s' % traceback.format_exc()
            upgradelog.write("%s: %s\n" % (time.ctime(), msg))
            logger.error(msg)
            schoolkeeper.UPDATE_MSG = ''
