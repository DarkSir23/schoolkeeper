import datetime
import hashlib
import os
import random
import re
import threading
import time
import urllib
import calendar
from shutil import copyfile, rmtree
import codecs


import csv
import cherrypy
import schoolkeeper
import lib.simplejson as simplejson
from cherrypy.lib.static import serve_file, serve_download
from lib.hashing_passwords import make_hash
from mako import exceptions
from mako.lookup import TemplateLookup
from schoolkeeper.webauth import AuthController, requireAuth, member_of, any_of, name_is, has_access_level
from schoolkeeper.formatter import check_int, plural
from schoolkeeper.common import checkRunningJobs, dateToTimestamp, datetimeToTimestamp, timestampToDateTime, currentTime, checkToBool, checkToInt, intToYesNo, getTotal, stringToInt, uni, convertMac, showJobs
from schoolkeeper.sessions import get_session_info
from schoolkeeper import dblogger, cache, images, reporting, attachment
from schoolkeeper import dbactions, versioncheck, SYS_ENCODING
from schoolkeeper import gam

from schoolkeeper import logger, database, activedirectory, utilities


def serve_template(templatename, **kwargs):
    _session = get_session_info()
    interface_dir = os.path.join(str(schoolkeeper.PROG_DIR), 'data/interfaces/')
    if _session['theme']:
        template_dir = os.path.join(str(interface_dir), _session['theme'])
    else:
        template_dir = os.path.join(str(interface_dir), schoolkeeper.CONFIG['HTTP_LOOK'])
    if not os.path.isdir(template_dir):
        logger.error("Unable to locate template [%s], reverting to default" % template_dir)
        schoolkeeper.CONFIG['HTTP_LOOK'] = 'bootstrap'
        template_dir = os.path.join(str(interface_dir), schoolkeeper.CONFIG['HTTP_LOOK'])

    _hplookup = TemplateLookup(directories=[template_dir])
    _session = get_session_info()
    userInfo = getPageInfo(_session)
    try:
        if schoolkeeper.UPDATE_MSG:
            template = _hplookup.get_template("dbupdate.html")
            return template.render(message="Database upgrade in progress, please wait...", title="Database Upgrade", timer=5)
        else:
            template = _hplookup.get_template(templatename)
            return template.render(http_root=schoolkeeper.HTTP_ROOT, _session=_session, userInfo=userInfo, **kwargs)
    except Exception:
        return exceptions.html_error_template().render()

def getPageInfo(session = None):
    if session:
        _session = session
    else:
        _session = get_session_info()
    page_info = {'alert_totals': 0, 'alerts': [], 'messages': {}}
    if 'Admin' in _session['access_level'] or 'Staff' in _session['access_level']:
        count = len(dbactions.findNewStudents())
        page_info['alert_totals'] += count
        alinfo = {'label': 'New Students', 'count': count, 'id': 'new_StudentCheck'}
        page_info['alerts'].append(alinfo)
    if 'Admin' in _session['access_level']:
        count = len(dbactions.findNewStaff())
        page_info['alert_totals'] += count
        alinfo = {'label': 'New Staff', 'count': count, 'id': 'new_StaffCheck'}
        page_info['alerts'].append(alinfo)
    return page_info

class WebInterface(object):

    auth = AuthController()


##### Base Pages #####

    @cherrypy.expose
    def index(self):
        raise cherrypy.HTTPRedirect("home")

    @cherrypy.expose
    @requireAuth()
    def home(self, msg=None):
        _session = get_session_info()
        userperson = dbactions.getPersonByUserName(_session['user_id'])
        if userperson:
            if userperson['type'] == 'STUDENT':
                schedule = sorted(dbactions.getSchedule(userperson['identifier']), key=lambda k: k['period'])
            elif userperson['type'] == 'STAFF':
                schedule = sorted(dbactions.getStaffSchedule(userperson['identifier']), key=lambda k: k['period'])
            else:
                schedule = []
        else:
            schedule = []
        logger.info(_session)
        newstudents = len(dbactions.findNewStudents())
        newstaff = len(dbactions.findNewStaff())
        title = "Home"
        test_graphs = {'building': reporting.getDevicesPerBuilding(), 'modeltype': reporting.getDevicesPerType()}
        return serve_template(templatename="index.html", title=title, msg=msg, graph=test_graphs, schedule=schedule, userperson=userperson, newstudents=newstudents, newstaff=newstaff, section='home')

    # PERSON ######



    # STUDENTS ######

    @cherrypy.expose
    @requireAuth(any_of(has_access_level('Staff'),has_access_level('Admin')))
    def students(self, msg=None, cmd=None, importfile=None):
        _session = get_session_info()
        title = "Students"
        csvdict = None
        filehash = None
        if cmd:
            if cmd == 'ImportPics' and importfile:
                filename = _session['user'] + currentTime()
                filehash, success = cache.cache_save(importfile, filename)
                cache.cache_cleanup()
                if success:
                    csvdict = {}
                    csvlist = []
                    importfile.file.seek(0)
                    csvreader = csv.reader(importfile.file)
                    for row in csvreader:
                        csvlist.append(row)
                    csvdict['csvlist'] = csvlist
                    csvdict['lines'] = len(csvlist)
                else:
                    msg = "CSV import failed"

        return serve_template(templatename="students.html", title=title, msg=msg, cmd=cmd, csvdict=csvdict, filehash=filehash, section='students')

    @cherrypy.expose
    def getStudentList(self, iDisplayStart=0, iDisplayLength=100, iSortCol_0=0, sSortDir_0="desc", sSearch="", **kwargs):
        myDB = database.DBConnection()
        iDisplayStart = int(iDisplayStart)
        iDisplayLength = int(iDisplayLength)
        schoolkeeper.CONFIG['DISPLAYLENGTH'] = iDisplayLength
        cmd = "SELECT people.lastname, people.firstname, people.username, ps_students.studentid, ps_students.gradelevel, ad_students.distinguishedname from people LEFT JOIN ad_students ON people.username = ad_students.username LEFT JOIN ps_students ON people.identifier = ps_students.studentid WHERE people.id LIKE 'STU%' "
        cmd += "order by lastName, firstName COLLATE NOCASE"
        rowlist = myDB.select(cmd)
        return self.rows2table(data=rowlist, iDisplayStart=iDisplayStart, iDisplayLength=iDisplayLength,
                               iSortCol_0=iSortCol_0, sSortDir_0=sSortDir_0, sSearch=sSearch)


    @cherrypy.expose
    @requireAuth(any_of(has_access_level('Staff'),has_access_level('Admin')))
    def studentPage(self, msg=None, StudentID=None, UserName=None, cmd=None, data=None, id=None):
        myDB = database.DBConnection()
        ps_student = None
        ad_student = None
        ps_override = None
        student = None
        locations = None
        grouplist = None
        attachments = []
        if id:
            student = myDB.match("SELECT * from people WHERE id = ?", (id,))
        elif StudentID:
            student = myDB.match("SELECT * from people WHERE identifier=? and id LIKE ?", (StudentID, 'STU%',))
        elif UserName:
            student = myDB.match("SELECT * from people WHERE LOWER(username)=?", (UserName.lower(),))
        if len(student) > 0:
            # logger.debug(student)
            ps_student = myDB.match("SELECT * from ps_students WHERE studentid=?", (student['identifier'],))
            if len(ps_student) > 0:
                ps_override = myDB.match("SELECT * from ps_student_override WHERE psid=?", (ps_student['psid'],))
                if len(ps_override) == 0:
                    ps_override = None
            else:
                ps_student = None
            schedule = sorted(dbactions.getSchedule(student['identifier']), key=lambda k: k['period'])
            # ad_student = myDB.match("SELECT * from ad_students WHERE LOWER(username)=?", (student['username'].lower(),))
            myLDAP = activedirectory.ldapConnection()
            ad_student = myLDAP.getlogininfo(username=student['username'])
            if ad_student:
                for key in ad_student.keys():
                    ad_student[key] = dbactions.getADVal(ad_student,key)
                if len(ad_student) == 0:
                    ad_student = None
            g_student = None
            gtemp = gam.get("info users %s formatjson nolicenses" % student['email'])
            if gtemp:
                g_student = simplejson.loads(gtemp)
            devices = dbactions.getDevices(student['id'])
            if cmd:
                if cmd=='Move' or cmd=='CreateUser':
                    locations = self.getOUs(baseDN=schoolkeeper.CONFIG['LDAP_BASESTUDENTDN'])
                    #logger.debug('Locations: %s' % locations)
                if cmd=='AddGroup':
                    tempgrouplist = gam.get('print groups')
                    tempgrouplist = tempgrouplist.split('\n')
                    grouplist = []
                    for group in tempgrouplist:
                        if group.lower().startswith('class.'):
                            grouplist.append(group)
                if cmd == "AddDevice":
                    if data:
                        data = dbactions.search(searchstring=data, type="Devices")
                if cmd == "RemoveDevice":
                    locations = myDB.select('SELECT * from locations')
                    # logger.debug("Locations: %s" % locations)
            attachments = dbactions.getAttachments(student['id'])
            photo = images.getPictureFilename(student['id'])
            if photo:
                photo = os.path.basename(photo)

            title = dbactions.getName(student['id'], FirstName=student['firstname'], LastName=student['lastname'])
            return serve_template(templatename='student.html', StudentName=title, title=title, ps_student=ps_student, ad_student=ad_student, g_student=g_student, ps_override=ps_override, schedule=schedule, id=student['id'], devices=devices, msg=msg, cmd=cmd, locations=locations, grouplist=grouplist, data=data, attachments=attachments, photo=photo, section="students")
        else:
            return serve_template(templatename="public_message.html", title="Student Not Found", msg="Error: Student Not Found")

    # STAFF ######

    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def staff(self, msg=None, showRemoved=0, showHidden=0, cmd=None, importfile=None):
        _session = get_session_info()
        title = "Staff"
        csvdict = None
        filehash = None
        if cmd:
            if cmd == 'ImportPics' and importfile:
                filename = _session['user'] + currentTime()
                filehash, success = cache.cache_save(importfile, filename)
                cache.cache_cleanup()
                if success:
                    csvdict = {}
                    csvlist = []
                    importfile.file.seek(0)
                    csvreader = csv.reader(importfile.file)
                    for row in csvreader:
                        csvlist.append(row)
                    csvdict['csvlist'] = csvlist
                    csvdict['lines'] = len(csvlist)
                else:
                    msg = "CSV import failed"
        return serve_template(templatename="stafflist.html", title=title, msg=msg, showRemoved=showRemoved, showHidden=showHidden, cmd=cmd, csvdict=csvdict, filehash=filehash, section='staff')

    @cherrypy.expose
    def getStaffList(self, iDisplayStart=0, iDisplayLength=100, iSortCol_0=0, sSortDir_0="desc", sSearch="", **kwargs):
        myDB = database.DBConnection()
        iDisplayStart = int(iDisplayStart)
        iDisplayLength = int(iDisplayLength)

        schoolkeeper.CONFIG['DISPLAYLENGTH'] = iDisplayLength
        cmd = "SELECT people.lastname, people.firstname, people.username, ps_staff.psid, ps_staff.title, ad_staff.distinguishedname, \
                schools.schoolabbrev, people.id from people LEFT JOIN ad_staff ON people.username = ad_staff.username LEFT JOIN \
                ps_staff ON people.identifier = ps_staff.psid, schools WHERE ps_staff.schoolid = schools.schoolid and people.id LIKE 'STA%' "
        if int(kwargs['showRemoved']) == 1:
            pass
        else:
            cmd += 'and ps_staff.status=1'
        if int(kwargs['showHidden']) == 1:
            pass
        else:
            cmd += 'and people.ignore=0'
        cmd += "order by lastName, firstName COLLATE NOCASE"
        temprowlist = myDB.select(cmd)
        rowlist = []
        for row in temprowlist:
            if dbactions.getParentID(row['id']) == row['id']:
                rowlist.append(row)
        return self.rows2table(data=rowlist, iDisplayStart=iDisplayStart, iDisplayLength=iDisplayLength,
                               iSortCol_0=iSortCol_0, sSortDir_0=sSortDir_0, sSearch=sSearch)


    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def staffPage(self, msg=None, StaffID=None, UserName=None, cmd=None, data=None,id=None):
        myDB = database.DBConnection()
        ps_student = None
        ad_student = None
        ps_override = None
        staff = None
        locations = None
        grouplist = None
        attachments = []
        if id:
            staff = myDB.match("SELECT * from people WHERE id = ?", (id,))
        elif StaffID:
            staff = myDB.match("SELECT * from people WHERE identifier=? and id LIKE ?", (StaffID, 'STA%',))
        elif UserName:
            staff = myDB.match("SELECT * from people WHERE LOWER(username)=?", (UserName.lower(),))
        if len(staff) > 0:
            if staff['id'] != dbactions.getParentID(staff['id']):
                staff = myDB.match('SELECT * from people where id = ?', (dbactions.getParentID(staff['id']),))
            ps_staff = myDB.match("SELECT * from ps_staff WHERE psid=?", (staff['identifier'],))
            if len(ps_staff) > 0:
                ps_override = myDB.match("SELECT * from ps_staff_override WHERE psid=?", (ps_staff['psid'],))
                if len(ps_override) == 0:
                    ps_override = None
            else:
                ps_staff = None
            schedule = sorted(dbactions.getStaffSchedule(staff['identifier']), key=lambda k: k['period'])
            #ad_staff = myDB.match("SELECT * from ad_staff WHERE LOWER(username)=?", (staff['username'].lower(),))
            myLDAP = activedirectory.ldapConnection()
            ad_staff = myLDAP.getlogininfo(username=staff['username'])
            if ad_staff:
                for key in ad_staff.keys():
                    ad_staff[key] = dbactions.getADVal(ad_staff,key)
                if len(ad_staff) == 0:
                    ad_staff = None
            g_staff = None
            g_staff_courses = None
            gtemp = gam.get("info users %s formatjson nolicenses" % staff['email'])
            if gtemp:
                #logger.debug(gtemp)
                g_staff = simplejson.loads(gtemp)
                #logger.debug(g_staff)
                # g_staff_courses = dbactions.getGoogleTeacherCourses(staff['email']) # Uncomment this when activating Classroom Support
            devices = dbactions.getDevices(staff['id'])
            if cmd:
                if cmd=='Move' or cmd=="CreateUser":
                    locations = self.getOUs(baseDN=schoolkeeper.CONFIG['LDAP_BASESTAFFDN'])
                    #logger.debug('Locations: %s' % locations)
                if cmd=='AddGroup':
                    tempgrouplist = gam.get('print groups')
                    tempgrouplist = tempgrouplist.split('\n')
                    grouplist = []
                    for group in tempgrouplist:
                        if group.lower().startswith('group'):
                            grouplist.append(group)
                if cmd=="AddDevice":
                    if data:
                        data = dbactions.search(searchstring=data, type="Devices")
                if cmd == "RemoveDevice":
                    locations = myDB.select('SELECT * from locations')
                    # logger.debug("Locations: %s" % locations)
                if cmd == "AssignParent":
                    if data:
                        data = dbactions.search(searchstring=data, type="PersonNoParent")
                        logger.debug(data)
            attachments = dbactions.getAttachments(staff['id'])
            photo = images.getPictureFilename(staff['id'])
            if photo:
                photo = os.path.basename(photo)
            title = "%s %s" % (staff['firstname'],staff['lastname'])
            return serve_template(templatename='staff.html', staff=staff, StaffName=title, title=title, ps_staff=ps_staff, ad_staff=ad_staff, g_staff=g_staff, ps_override=ps_override, schedule=schedule, id=staff['id'], devices=devices, msg=msg, cmd=cmd, locations=locations, grouplist=grouplist, data=data, attachments=attachments, photo=photo, section='staff', g_staff_courses=g_staff_courses)
        else:
            return serve_template(templatename="public_message.html", title="Staff Not Found", msg="Error: Staff Not Found")


    # PEOPLE ######

    def return_person(self, personid=None, msg=None):
        if personid:
            myDB = database.DBConnection()
            person = myDB.match('select * from people where id=?', (personid,))
            if person:
                if "STU" in personid:
                    return self.studentPage(StudentID=person['identifier'], msg=msg)
                elif "STA" in personid:
                    return self.staffPage(StaffID=person['identifier'], msg=msg)
                else:
                    return serve_template(templatename="public_message.html", title="Error", msg="User not found.  %s" % msg)
        else:
            return serve_template(templatename="public_message.html", title="Error", msg="No Person specified")

    def return_device(self, deviceid=None, msg=None):
        if deviceid:
            return self.devicePage(deviceid=deviceid, msg=msg)
        else:
            return serve_template(templatename="public_message.html", title="Error", msg="No device specified")


    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def movePerson(self, personid=None, location=None):
        _session = get_session_info()
        if personid:
            myLDAP = activedirectory.ldapConnection()
            adinfo = None
            myDB = database.DBConnection()
            person = myDB.match("SELECT * from people WHERE id=?", (personid,))
            # logger.debug(person)
            if len(person) == 0:
                return serve_template(templatename="public_message.html", title="Invalid Person", msg="Person (%s) does not exist." % personid)
            adinfo = myLDAP.getlogininfo(username=person['username'])
            for key in adinfo.keys():
                adinfo[key] = dbactions.getADVal(adinfo,key)
            # logger.debug(adinfo)
            if adinfo is None:
                return serve_template(templatename="public_message.html", msg="AD Info Not Found")
            if location:
                newlocation = location
                loc_split = location.split(',')
                loc_name = ""
                for loc_line in loc_split:
                    if "OU=" in loc_line:
                        loc_piece = loc_line.split("=")[1]
                        loc_name = "/" + loc_piece + loc_name
            else:
                return serve_template(template_name="public_message.html", title="No location provided", msg="You must provide a move location to move a student or staff member")
            adresult = myLDAP.moveUser(olddn=adinfo['distinguishedname'], cn='CN=%s' % adinfo['displayname'], newdn=newlocation)
            googleresult = gam.get('update user %s org "%s"' % (adinfo['mail'],loc_name))
            if adresult != "False":
                dblogger.person("Moved to %s" % loc_name, _session['user'], personid)
                dblogger.user("Moved %s to %s" % (adinfo['displayName'], loc_name), _session['user'], _session['user'])
            return self.return_person(personid=personid, msg="Move Results:<br>AD: %s <br>Google: %s" % (adresult,googleresult))

    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def notePerson(self, personid=None, note=None):
        _session = get_session_info()
        if personid:
            myDB = database.DBConnection()
            person = myDB.match("SELECT * from people WHERE id=?", (personid,))
            if len(person) == 0:
                return serve_template(templatename="public_message.html", title="Invalid Person", msg="Person (%s) does not exist." % personid)
            if note:
                dblogger.person("NOTE: %s" % note, _session['user'], personid)
                dblogger.user("Added note to %s %s" % (person['firstname'], person['lastname']), _session['user'], _session['user'])
            else:
                return serve_template(templatename="public_message.html", title="Missing Note", msg="No note provided." % personid)
            return self.return_person(personid=personid, msg="Note Added")

    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def createUser(self, personid=None, location=None, redirect="Person"):
        _session = get_session_info()
        if personid:
            myLDAP = activedirectory.ldapConnection()
            myDB = database.DBConnection()
            admessage = None
            gmessage = None
            person = myDB.match("SELECT * from people WHERE id=?", (personid,))
            if location:
                newlocation = location
                loc_split = location.split(',')
                loc_name = ""
                for loc_line in loc_split:
                    if "OU=" in loc_line:
                        loc_piece = loc_line.split("=")[1]
                        loc_name = "/" + loc_piece + loc_name
            else:
                return serve_template(template_name="public_message.html", title="No location provided", msg="You must provide a location to create a student or staff member")
            if person:
                ps_info = None
                ad_command = {}
                ad_cn = 'CN=%s,%s' % (dbactions.getName(id=personid),newlocation)
                g_command = ""
                temppass = ''

                if "STU" in personid:
                    ps_info = myDB.match("SELECT * from ps_students where studentid=?", (person['identifier'],))
                    if len(ps_info) > 0:
                        school = myDB.match("SELECT * from schools where schoolid=?", (ps_info['schoolid'],))
                        grad_year = schoolkeeper.CONFIG['CURRENT_SCHOOL_YEAR'] + 12 - ps_info['gradelevel']
                        ad_command['description'] = "%s %s" % (school['schoolabbrev'], grad_year)
                        temppass = ps_info['cafepin']
                        if not temppass:
                            temppass = ps_info['studentid']
                elif "STA" in personid:
                    ps_info = myDB.match("SELECT * from ps_staff where psid=?", (person['identifier'],))
                    if len(ps_info) > 0 and len(ps_info['title']) > 0:
                        ad_command['description'] = ps_info['title']
                    temppass = 'whitko%s%s' % (person['firstname'][0].lower(), person['lastname'][0].lower())
                if len(ps_info) > 0:
                    ad_command['sAMAccountName'] = dbactions.getUserName(id=personid)
                    ad_command['userPrincipalName'] = dbactions.getUserName(id=personid)
                    ad_command['displayName'] = dbactions.getName(id=personid)
                    ad_command['givenName'] = person['firstname']
                    ad_command['sn'] = person['lastname']
                    ad_command['mail'] = person['email']
                adinfo = myLDAP.getlogininfo(username=person['username'])
                if adinfo:
                    admessage = "Could not create user: %s.  Already exists." % person['username']
                else:
                    logger.debug(ad_command)
                    adresp = myLDAP.connection.add(ad_cn, 'User', ad_command)
                    logger.debug(myLDAP.connection.result)
                    if adresp:
                        admessage = "User created.<br>"
                        dblogger.user("AD user account created for %s" % dbactions.getName(id=personid), _session['user'], _session['user'])
                        dblogger.person("AD user account created", _session['user'], personid)
                        pwbool, pwresult = myLDAP.setPassword(username=person['username'], password=temppass, unlock=True)
                        admessage += pwresult
                    else:
                        admessage = "User not created.<br>"
                ginfo = gam.get('info users %s formatjson nolicenses' % person['email'])
                if ginfo:
                    ginfoparsed = simplejson.loads(ginfo)
                    if ginfoparsed['suspended']:
                        gmessage = gam.get('update user %s suspended off password "%s" org "%s"' % (person['email'], temppass, loc_name))
                    else:
                        gmessage = "Could not create google user: %s.  Already exists" % person['email']
                else:
                    gmessage = gam.get('create user %s firstname "%s" lastname "%s" password "%s" changepassword off org "%s"' % (person['email'], person['firstname'], person['lastname'], temppass, loc_name))
                    if gmessage:
                        dblogger.user("Google user account created for %s" % dbactions.getName(id=personid), _session['user'], _session['user'])
                        dblogger.person("Google user account created", _session['user'], personid)
                msg = "User Created - AD: %s - Google: %s<br/>" % (admessage, gmessage)
                if "STU" in personid:
                    dbactions.updateADStudents()
                elif "STA" in personid:
                    dbactions.updateADStaff()
                if redirect == "Person":
                    return self.return_person(personid=personid, msg=msg)
                else:
                    return msg

    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def resetPassword(self, personid=None, password=None):
        _session = get_session_info()
        if personid:
            myLDAP = activedirectory.ldapConnection()
            myDB = database.DBConnection()
            person = myDB.match("SELECT * from people WHERE id=?", (personid,))
            if not person or len(person) == 0:
                return self.return_person(personid=personid, msg="Error.  How did you get here?")
            adinfo = myLDAP.getlogininfo(username=person['username'])
            if not adinfo:
                return self.return_person(personid=personid, msg="Active Directory account not created.  Please use the Create AD/Google Account button first.")
            if not password or len(password) < 3:
                return self.return_person(personid=personid, msg="Insufficient password provided.")
            if myLDAP.checkldapgroups(adinfo['distinguishedName'], schoolkeeper.CONFIG['LDAP_ADMIN1_GROUP']) and not has_access_level('SuperAdmin'):
                return self.return_person(personid=personid, msg="Cannot change password for other administrators.  Please use the SuperAdmin account.")
            pwbool, admessage = myLDAP.setPassword(username=person['username'], password=password)
            gammsg = gam.setPassword(person['email'], password)
            if pwbool:
                dblogger.user("Password reset for %s" % dbactions.getName(id=personid), _session['user'],
                              _session['user'])
                dblogger.person("Password reset", _session['user'], personid)
            msg = "%s<br>%s" % (admessage, gammsg)

            return self.return_person(personid=personid, msg="Result: %s" % msg)
        else:
            return serve_template(templatename="public_message.html", title="Invalid Person", msg="No personid provided")

    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def unlockGoogleAccount(self, personid=None):
        _session = get_session_info()
        if personid:
            myDB = database.DBConnection()
            person = myDB.match("SELECT * from people WHERE id=?", (personid,))
            if not person or len(person) == 0:
                return self.return_person(personid=personid, msg="Error.  How did you get here?")
            ginfo = gam.get('info users %s formatjson nolicenses' % person['email'])
            if ginfo:
                ginfoparsed = simplejson.loads(ginfo)
                if not ginfoparsed['suspended']:
                    return self.return_person(personid=personid, msg="Error.  User is not suspended.  How did you get here?")
                else:
                    gmessage = gam.get('update user %s suspended off' % person['email'])
                    dblogger.user("Unlocked google account %s" % dbactions.getName(id=personid), _session['user'],
                                  _session['user'])
                    dblogger.person("Google account unlocked.", _session['user'], personid)
                    return self.return_person(personid=personid, msg="Result: %s" % gmessage)
            else:
                return self.return_person(personid=personid, msg="Error: Google account does not exist.")
        else:
            return serve_template(templatename="public_message.html", title="Invalid Person", msg="No personid provided")

    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def addGoogleGroup(self, personid=None, group=None, level=None):
        _session = get_session_info()
        if personid:
            if not group:
                return self.return_person(personid=personid, msg="Error.  No group selected.")
            myDB = database.DBConnection()
            person = myDB.match("SELECT * from people WHERE id=?", (personid,))
            if not person or len(person) == 0:
                return self.return_person(personid=personid, msg="Error.  How did you get here?")
            ginfo = gam.get('info users %s formatjson nolicenses' % person['email'])
            if ginfo:
                # gmessage = gam.addGroupUser(person['email'], group, level=level)
                gmessage = gam.get('update group %s add member %s' % (group, person['email']))
                dblogger.user("Added %s to group: %s" % (dbactions.getName(id=personid), group), _session['user'],
                              _session['user'])
                dblogger.person("Added to group %s" % (group), _session['user'], personid)
                return self.return_person(personid=personid, msg="Result: %s" % gmessage)
            else:
                return self.return_person(personid=personid, msg="Error: Google account does not exist.")
        else:
            return serve_template(templatename="public_message.html", title="Invalid Person", msg="No personid provided")

    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def removeGoogleGroup(self, personid=None, group=None):
        _session = get_session_info()
        if personid:
            if not group:
                return self.return_person(personid=personid, msg="Error.  No group selected.")
            myDB = database.DBConnection()
            person = myDB.match("SELECT * from people WHERE id=?", (personid,))
            if not person or len(person) == 0:
                return self.return_person(personid=personid, msg="Error.  How did you get here?")
            ginfo = gam.get('info users %s formatjson nolicenses' % person['email'])
            if ginfo:
                gmessage = gam.get('update group %s remove member %s' % (group, person['email']))
                dblogger.user("Removed %s from group: %s" % (dbactions.getName(id=personid), group), _session['user'],
                              _session['user'])
                dblogger.person("Removed from group %s" % (group), _session['user'], personid)
                return self.return_person(personid=personid, msg="Result: %s" % gmessage)
            else:
                return self.return_person(personid=personid, msg="Error: Google account does not exist.")
        else:
            return serve_template(templatename="public_message.html", title="Invalid Person", msg="No personid provided")


    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def removeDevice(self, personid=None, deviceid=None, note=None, redirect=None, locationid=None):
        _session = get_session_info()
        if not personid:
            return serve_template(templatename="public_message.html", title="Invalid Person", msg="No personid provided")
        if not deviceid:
            return serve_template(templatename="public_message.html", title="Invalid Device", msg="No deviceid provided")
        myDB = database.DBConnection()
        person = myDB.match("SELECT * from people where id=?", (personid,))
        if not person or len(person) == 0:
            return serve_template(templatename="public_message.html", title="Invalid Person", msg="Error:  How did you get here?")
        device = myDB.match("SELECT * from devices where deviceid=?", (deviceid,))
        if not device or len(device) == 0:
            return serve_template(templatename="public_message.html", title="Invalid Person", msg="Error:  How did you get here?")
        removeresult = dbactions.removeDevice(personid, deviceid, locationid)
        # logger.debug("Result: %s" % removeresult)
        if removeresult:
            if note:
                noteText = ' - Note: %s' % note
            else:
                noteText = ''
            dblogger.user('Removed device: <a href="devicePage?deviceid=%s">%s</a> from %s%s' % (deviceid, device['tag'], dbactions.getName(id=personid), noteText), _session['user'], _session['user'])
            dblogger.person('Device removed: <a href="devicePage?deviceid=%s">%s</a>%s' % (deviceid, device['tag'], noteText), _session['user'], personid)
            dblogger.device('Removed from %s%s' % (dbactions.getName(id=personid), noteText), _session['user'], deviceid)
            testdevice = dbactions.getDevice(deviceid)
            location = dbactions.getDeviceLoc(testdevice)
            # logger.debug("Location: %s" % location)
            dblogger.device('Location updated to %s' % location['locationname'], _session['user'], deviceid)
            removetext = "Device removed"
            dbactions.updateDeviceCache(deviceid)
        else:
            removetext = "Device not removed"
        if redirect == 'person':
            return self.return_person(personid=personid, msg=removetext)
        elif redirect == 'device':
            return self.return_device(deviceid=deviceid, msg=removetext)
        elif redirect == 'none':
            return removetext
        else:
            return self.home()

    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def assignDevice(self, personid=None, redirect=None, **args):
        _session = get_session_info()
        deviceid = None
        if not personid:
            return serve_template(templatename="public_message.html", title="Invalid Person", msg="No personid provided")
        if len(args) > 0:
            myDB = database.DBConnection()
            msg = ''
            for devid in args:
                if devid.startswith('note_'):
                    continue
                device = myDB.match("SELECT * from devices where deviceid=?", (devid,))
                if device and len(device) > 0:
                    oldloc = dbactions.getDeviceLoc(device)
                    deviceid = device['deviceid']
                    notevar = 'note_%s' % devid
                    if notevar in args:
                        note = ' (Note: %s)' % args[notevar]
                    else:
                        note = ''
                    prev_owner = None
                    if device['ownerid'] and len(device['ownerid']) > 0:
                        prev_owner = myDB.match("SELECT * from people where id=?", (device['ownerid'],))
                    myDB.upsert('devices', {'ownerid': personid, 'locationid': None}, {'deviceid': devid})
                    if prev_owner:
                        dblogger.user("Moved device %s from %s to %s%s" % (device['tag'], dbactions.getName(id=prev_owner['id']), dbactions.getName(id=personid),note), _session['user'], _session['user'])
                        dblogger.device("Assignment moved from %s to %s%s" % (dbactions.getName(id=prev_owner['id']), dbactions.getName(id=personid),note), _session['user'], devid)
                        dblogger.device("Location changed from %s to 'With User'" % oldloc['locationname'], _session['user'], devid)
                        dblogger.person("Device %s reassigned to %s%s" % (device['tag'], dbactions.getName(id=personid), note), _session['user'], prev_owner['id'])
                        dblogger.person("Device %s reassigned from %s%s" % (device['tag'], dbactions.getName(id=prev_owner['id']), note), _session['user'], personid)
                        msg += 'Device %s reassigned from %s to %s%s<br>' % (device['tag'], dbactions.getName(id=prev_owner['id']), dbactions.getName(id=personid),note)
                    else:
                        dblogger.user("Assigned device %s to %s.%s" % (device['tag'], dbactions.getName(id=personid), note), _session['user'], _session['user'])
                        dblogger.device("Assigned to %s.%s" % (dbactions.getName(id=personid), note), _session['user'], devid)
                        dblogger.device("Location changed from %s to 'With User'" % oldloc['locationname'], _session['user'], devid)
                        dblogger.person("Device %s assigned.%s" % (device['tag'], note), _session['user'], personid)
                        msg += 'Device %s assigned to %s%s<br>' % (device['tag'], dbactions.getName(id=personid), note)
                    dbactions.updateDeviceCache(devid)
        else:
            msg = 'No devices selected.'
        if redirect == 'person':
            return self.return_person(personid=personid, msg=msg)
        elif redirect == 'device':
            return self.return_device(deviceid=deviceid, msg=msg)
        elif redirect == 'none':
            return msg
        else:
            return self.home()

    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def pictureUpload(self, id=None, uploadfile=None):
        _session = get_session_info()
        msg = images.pictureUpload(session=_session, id=id, uploadfile=uploadfile)
        return self.return_person(personid=id, msg=msg)

    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def googlePictureUpdate(self, id=None):
        _session = get_session_info()
        msg = gam.uploadPhoto(account=id, session=_session)
        return self.return_person(personid=id, msg=msg)

    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def pictureImport(self, firstline='', lastline='', filehash=None, **kwargs):
        _session = get_session_info()
        msg = images.pictureImport(session=_session, firstline=firstline, lastline=lastline, filehash=filehash, **kwargs)
        if 'redirect' in kwargs.keys():
            if kwargs['redirect'] == 'students':
                return self.students(msg=msg)
            elif kwargs['redirect'] == 'staff':
                return self.staff(msg=msg)
            else:
                return self.home(msg=msg)
        else:
            return self.home(msg=msg)

    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def pictureExport(self, type=None, school=None, **kwargs):
        _session = get_session_info()
        myDB = database.DBConnection()
        personlist = []
        filename = 'export.zip'
        if type == 'STU':
            if school:
                pass
            else:
                students = myDB.select('SELECT id from people, ps_students where people.identifier = ps_students.studentid')
                for student in students:
                    personlist.append(student['id'])
                filename = "student_all_picture_export.zip"
        elif type == 'STA':
            if school:
                pass
            else:
                stafflist = myDB.select('SELECT id from people, ps_staff where people.identifier = ps_staff.psid')
                for staff in stafflist:
                    personlist.append(staff['id'])
                filename = "staff_all_picture_export.zip"
        export = images.pictureExport(session=_session, personlist=personlist)
        # cherrypy.response.headers['Content-Disposition'] = 'attachment; filename="export1.zip"'
        return serve_file(export, 'application/x-download', 'attachment', filename)

    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def assignParent(self, id=None, parentid=None, redirect=None):
        _session = get_session_info()
        msg = None
        if id and parentid:
            myDB = database.DBConnection()
            msg = myDB.upsert('accountlink', {'parentid': parentid}, {'childid': id})
            dbactions.moveDevicesToParent(id)
        if redirect == 'person':
            return self.return_person(personid=id, msg=msg)
        else:
            return self.home(msg=msg)

    # SCHOOLS ######

    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def schools(self, msg=None, EditSchool=None):
        myDB = database.DBConnection()
        title = "Schools"
        if EditSchool:
            school = myDB.match('SELECT * from schools WHERE schoolid=?', (EditSchool,))
            if len(school) == 0:
                school = {'schoolid': EditSchool, 'schoolname': '', 'schoolabbrev': '', 'pearsonid': '', 'pearsonpassword': '', 'details': {}}
            else:
                school['details'] = simplejson.loads(school['details']) if school['details'] else {}
        else:
            school = None
        return serve_template(templatename="schools.html", title=title, msg=msg, school=school, section='schools')


    @cherrypy.expose
    def getSchoolList(self, iDisplayStart=0, iDisplayLength=100, iSortCol_0=0, sSortDir_0="desc", sSearch="", **kwargs):
        myDB = database.DBConnection()
        iDisplayStart = int(iDisplayStart)
        iDisplayLength = int(iDisplayLength)
        schoolkeeper.CONFIG['DISPLAYLENGTH'] = iDisplayLength
        cmd = "SELECT schoolid, schoolname, schoolabbrev from schools "
        cmd += "order by schoolname COLLATE NOCASE"
        rowlist = myDB.select(cmd)
        return self.rows2table(data=rowlist, iDisplayStart=iDisplayStart, iDisplayLength=iDisplayLength,
                               iSortCol_0=iSortCol_0, sSortDir_0=sSortDir_0, sSearch=sSearch)


    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def schoolUpdate(self, schoolid='', schoolname='', schoolabbrev='', pearsonid='', pearsonpassword='', **kwargs):
        self.label_thread()
        _session = get_session_info()
        myDB = database.DBConnection()
        if schoolid:
            oldentry = myDB.match('SELECT * from schools where schoolid=?', (schoolid,))
            schooldetails = {}
            for key in kwargs.keys():
                schooldetails[key] = kwargs[key]

            controlValueDict = {'schoolid': schoolid}
            newValueDict = {
                'schoolname': schoolname,
                'schoolabbrev': schoolabbrev,
                'pearsonid': pearsonid,
                'pearsonpassword': pearsonpassword,
                'details': simplejson.dumps(schooldetails)
            }
            myDB.upsert('schools',newValueDict,controlValueDict)
            newentry = myDB.match('SELECT * from schools where schoolid=?', (schoolid,))
            if len(oldentry) == 0:
                dblogger.user('Added school: %s (%s)' % (schoolname, schoolid), _session['user'], _session['user'])
            else:
                for key in newentry.keys():
                    if oldentry[key] != newentry[key]:
                        dblogger.user('Modified %s (%s) from %s to %s' % (oldentry['schoolname'], key, oldentry[key], newentry[key]), _session['user'], _session['user'])
        raise cherrypy.HTTPRedirect("schools")


    # LOCATIONS ######

    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def locations(self, msg=None, EditLocation=None):
        myDB = database.DBConnection()
        title = "Locations"
        if EditLocation:
            location = myDB.match('SELECT * from locations WHERE locationid=?', (EditLocation,))
            if len(location) == 0:
                location = {'locationid': EditLocation, 'locationname': '', 'schoolid': None}
            schools = myDB.select('SELECT * from schools')
        else:
            location = None
            schools = None
        return serve_template(templatename="locations.html", title=title, msg=msg, schools=schools, location=location, section="devices")


    @cherrypy.expose
    def getLocationList(self, iDisplayStart=0, iDisplayLength=100, iSortCol_0=0, sSortDir_0="desc", sSearch="", **kwargs):
        myDB = database.DBConnection()
        iDisplayStart = int(iDisplayStart)
        iDisplayLength = int(iDisplayLength)
        schoolkeeper.CONFIG['DISPLAYLENGTH'] = iDisplayLength
        cmd = "SELECT locationid, locationname, locations.schoolid, schoolabbrev from locations, schools WHERE locations.schoolid = schools.schoolid"
        cmd += " order by locationname COLLATE NOCASE"
        rowlist = myDB.select(cmd)
        return self.rows2table(data=rowlist, iDisplayStart=iDisplayStart, iDisplayLength=iDisplayLength,
                               iSortCol_0=iSortCol_0, sSortDir_0=sSortDir_0, sSearch=sSearch)


    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def locationUpdate(self, locationid='', locationname='', schoolid=''):
        self.label_thread()
        _session = get_session_info()
        myDB = database.DBConnection()
        if locationid:
            oldentry = myDB.match('SELECT * from locations where locationid=?', (locationid,))
            controlValueDict = {'locationid': locationid}
            newValueDict = {
                'locationname': locationname,
                'schoolid': schoolid,
            }
            myDB.upsert('locations',newValueDict,controlValueDict)
            newentry = myDB.match('SELECT * from locations where locationid=?', (locationid,))
            if len(oldentry) == 0:
                dblogger.user('Added location: %s (%s)' % (locationname, locationid), _session['user'], _session['user'])
            else:
                for key in newentry.keys():
                    if oldentry[key] != newentry[key]:
                        dblogger.user('Modified %s (%s) from %s to %s' % (oldentry['locationname'], key, oldentry[key], newentry[key]), _session['user'], _session['user'])
        raise cherrypy.HTTPRedirect("locations")


    # DEVICES ######

    @cherrypy.expose
    @requireAuth(any_of(has_access_level('Staff'),has_access_level('Admin')))
    def devices(self, msg=None, showRemoved=0, showHidden=0, importfile=None, cmd=None):
        _session = get_session_info()
        csvdict = None
        filehash=None
        if importfile:
            filename = _session['user'] + currentTime()
            filehash, success = cache.cache_save(importfile, filename)
            cache.cache_cleanup()
            if success:
                csvdict = {}
                csvlist = []
                importfile.file.seek(0)
                csvreader = csv.reader(importfile.file)
                for row in csvreader:
                    csvlist.append(row)
                csvdict['csvlist'] = csvlist
                csvdict['lines'] = len(csvlist)
            else:
                msg = "CSV import failed"
        title = "Devices"
        return serve_template(templatename="devices.html", title=title, msg=msg, showRemoved=showRemoved, showHidden=showHidden, csvdict=csvdict, filehash=filehash, cmd=cmd, section="devices")


    @cherrypy.expose
    @requireAuth(any_of(has_access_level('Staff'),has_access_level('Admin')))
    def devicePage(self, msg=None, deviceid=None, edit=0, cmd=None, data=None):
        myDB = database.DBConnection()
        device = None
        adData = None
        model = None
        models = None
        locations = None
        g_device = None
        attachments = []
        if deviceid:
            device = myDB.match("SELECT * from devices WHERE deviceid=?", (deviceid,))

        if device and len(device) > 0:
            owner = dbactions.getPerson(device['ownerid'])
            model = myDB.match('SELECT * from device_models WHERE modelid = ?', (device['modelid'],))
            if device['adname']:
                adData = dbactions.getADSearch(type="Device", searchstring="%s" % device['adname'])
            elif device['tag'] and model and model['modeltype'] and 'Windows' in model['modeltype']:
                adData = dbactions.getADSearch(type="Device", searchstring="%s" % device['tag'])
            title = "%s (%s)" % (device['tag'],device['serialnumber'])
            if device['locationid']:
                location = myDB.match('SELECT * from locations WHERE locationid = ?', (device['locationid'],))
            else:
                if owner:
                    location = {'locationname': 'With User'}
                else:
                    location = {'locationname': 'Unknown'}
            editor = False
            if int(edit) == 1:
                editor = True
                models = myDB.select('SELECT * from device_models')
                locations = myDB.select('SELECT * from locations')
            if device['gdeviceid'] and len(device['gdeviceid']) > 10:
                gtemp = gam.get('info cros %s formatjson' % device['gdeviceid'])
                if gtemp:
                    g_device = simplejson.loads(gtemp)
            if cmd:
                if cmd == "AssignDevice":
                    if data:
                        data = dbactions.search(searchstring=data, type="Person")
                if cmd == "MoveDevice" and not data:
                    data = myDB.select('SELECT * from locations')
                if cmd == "UnassignDevice":
                    data = myDB.select('SELECT * from locations')
            attachments = dbactions.getAttachments(device['deviceid'])
            return serve_template(templatename='device.html', DeviceDisplay=title, title=title, id=device['deviceid'], device=device, owner=owner, editor=editor, model=model, adData=adData, models=models, cmd=cmd, data=data, locations=locations, location=location, msg=msg, attachments=attachments, g_device = g_device, section="devices")
        else:
            device = {'deviceid': -1, 'modelid': '', 'ownerid': '', 'serialnumber': '', 'tag': '', 'mac': '', 'adname': '', 'purchased': '', 'notes': '', 'locationid': '', 'retired': 0}
            return serve_template(templatename="device.html", DeviceDisplay="Add Device", title="Add Device", id=-1, device=device, owner=None, editor=True, models=myDB.select('SELECT * from device_models'), model=None, adData=None, cmd=None, data=None, locations=myDB.select('SELECT * from locations'), location=None, attachments=[])

    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def deviceUpdate(self, deviceid=None, redirect="devices", **kwargs):
        self.label_thread()
        _session = get_session_info()
        myDB = database.DBConnection()
        msg=""
        if deviceid:
            # logger.debug(kwargs)
            macstring = None
            serialnumber = None
            tag = None
            modelid = None
            if 'mac' in kwargs.keys():
                mac = kwargs.pop('mac')
                if len('mac') > 0:
                    success, macstring = convertMac(mac)
                    logger.debug("%s - %s" % (success, macstring))
            if 'serialnumber' in kwargs.keys():
                serialnumber = kwargs.pop('serialnumber')
                if len(serialnumber) < 1:
                    serialnumber = None
            if 'tag' in kwargs.keys():
                tag = kwargs.pop('tag')
                if len(tag) < 1:
                    tag = None
            oldentry = myDB.match('SELECT * from devices where deviceid=?', (deviceid,))
            if len(oldentry) > 0:
                if not tag:
                    tag = oldentry['tag']
            if int(deviceid) < 0:
                if serialnumber:
                    controlValueDict = {'serialnumber': serialnumber}
                    newValueDict = {
                        'mac': macstring,
                        'tag': tag
                    }
                elif tag:
                    controlValueDict = {'tag': tag}
                    newValueDict = {
                        'serialnumber': serialnumber,
                        'mac': macstring,
                    }
                else:
                    return
            else:
                controlValueDict = {'deviceid': deviceid}
                newValueDict = {
                    'mac': macstring,
                    'tag': tag,
                    'serialnumber': serialnumber,
                }
            # logger.debug(kwargs.keys())
            for key in kwargs.keys():
                newValueDict[key] = kwargs[key]

            myDB.upsert('devices',newValueDict,controlValueDict)
            newentry = myDB.match('SELECT * from devices where deviceid=?', (deviceid,))
            if len(oldentry) == 0:
                dblogger.user('Added device: %s (%s)' % (tag, serialnumber), _session['user'], _session['user'])
                dbactions.updateDeviceCache()
                msg = 'Device created: %s (%s)' % (tag, serialnumber)
            else:
                for key in newentry.keys():
                    if oldentry[key] != newentry[key]:
                        dblogger.device("Modified %s from %s to %s" % (key, oldentry[key], newentry[key]), _session['user'], deviceid)
                        dblogger.user('Modified %s (%s) from %s to %s' % (oldentry['deviceid'], key, oldentry[key], newentry[key]), _session['user'], _session['user'])
                        msg += 'Modified %s from %s to %s<br/>' % (key, oldentry[key], newentry[key])
                dbactions.updateDeviceCache(oldentry['deviceid'])
                dbactions.updateDeviceCache(newentry['deviceid'])
        if redirect == "devices":
            return self.devices(msg=msg)
        elif redirect == "thisdevice":
            return self.return_device(deviceid=deviceid, msg=msg)
        elif redirect == "none":
            return msg
        else:
            return home(msg=msg)

    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def devicemodels(self, msg=None, EditDeviceModel=None):
        myDB = database.DBConnection()
        title = "Device Models"
        if EditDeviceModel:
            model = myDB.match('SELECT * from device_models WHERE modelid=?', (EditDeviceModel,))
            if len(model) == 0:
                model = {'modelid': EditDeviceModel, 'modelname': '', 'modelmanufacturer': '', 'modeltype': '', 'modeldescription': '',}
        else:
            model = None
        return serve_template(templatename="devicemodels.html", title=title, msg=msg, model=model, section="devices")

    @cherrypy.expose
    def getDeviceModelList(self, iDisplayStart=0, iDisplayLength=100, iSortCol_0=0, sSortDir_0="desc", sSearch="", **kwargs):
        myDB = database.DBConnection()
        iDisplayStart = int(iDisplayStart)
        iDisplayLength = int(iDisplayLength)
        schoolkeeper.CONFIG['DISPLAYLENGTH'] = iDisplayLength
        cmd = "SELECT modelid, modelname, modelmanufacturer, modeltype, modeldescription from device_models "
        cmd += "order by modelname COLLATE NOCASE"
        rowlist = myDB.select(cmd)
        return self.rows2table(data=rowlist, iDisplayStart=iDisplayStart, iDisplayLength=iDisplayLength,
                               iSortCol_0=iSortCol_0, sSortDir_0=sSortDir_0, sSearch=sSearch)

    @cherrypy.expose
    def getDeviceList(self, iDisplayStart=0, iDisplayLength=100, iSortCol_0=0, sSortDir_0="desc", sSearch="", unFiltered=0, **kwargs):
        logger.debug("show removed: %s" % unFiltered)
        devicelist = dbactions.getDeviceCache(unFiltered=unFiltered)
        iDisplayStart = int(iDisplayStart)
        iDisplayLength = int(iDisplayLength)
        schoolkeeper.CONFIG['DISPLAYLENGTH'] = iDisplayLength
        return self.rows2table(data=devicelist, iDisplayStart=iDisplayStart, iDisplayLength=iDisplayLength,
                               iSortCol_0=iSortCol_0, sSortDir_0=sSortDir_0, sSearch=sSearch)

    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def deviceModelUpdate(self, modelid='', modelname='', modelmanufacturer='', modeltype='', modeldescription=''):
        self.label_thread()
        _session = get_session_info()
        myDB = database.DBConnection()
        if modelid:
            oldentry = myDB.match('SELECT * from device_models where modelid=?', (modelid,))
            if len(oldentry) == 0:
                modelid = None
            controlValueDict = {'modelid': modelid}
            newValueDict = {
                'modelname': modelname,
                'modelmanufacturer': modelmanufacturer,
                'modeltype': modeltype,
                'modeldescription': modeldescription,
            }
            myDB.upsert('device_models',newValueDict,controlValueDict)
            newentry = myDB.match('SELECT * from device_models where modelid=?', (modelid,))
            if len(oldentry) == 0:
                dblogger.user('Added device model: %s (%s)' % (modelname, modelid), _session['user'], _session['user'])
            else:
                for key in newentry.keys():
                    if oldentry[key] != newentry[key]:
                        dblogger.user('Modified %s (%s) from %s to %s' % (oldentry['modelname'], key, oldentry[key], newentry[key]), _session['user'], _session['user'])
            dbactions.updateDeviceCache()
        raise cherrypy.HTTPRedirect("devicemodels")



    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def deviceImport(self, firstline='', lastline='', filehash='', **kwargs):
        _session = get_session_info()
        myDB = database.DBConnection()
        msg = ""
        importlist, success = cache.cache_get(filehash=filehash, type='CSV')
        importitems = []
        if success:
            for index in xrange(int(firstline), int(lastline)+1):
                device = {}
                devicekey = {}
                devicemodel = {}
                devicepersonpair = {}
                devicelocation = {}
                # Get device attributes from line and static
                for subindex in xrange(0, len(importlist[index])):
                    colid = kwargs['col_%s' % subindex].encode('utf-8')
                    currentitem = importlist[index][subindex].encode('utf-8')
                    if colid == 'STUDENT_ID' and len(currentitem) > 0:
                        devicepersonpair['studentid'] = currentitem
                    elif colid == 'DEVICE_TAG':
                        device['tag'] = currentitem.zfill(4)
                    elif colid == 'STAFF_ID' and len(currentitem) > 0:
                        devicepersonpair['staffid'] = currentitem
                    elif colid == 'DEVICE_SERIAL' and len(currentitem) > 0:
                        device['serialnumber'] = currentitem
                    elif colid == 'DEVICE_NOTES' and len(currentitem) > 2:
                        device['notes'] = currentitem
                    elif colid == 'DEVICE_MODEL':
                        devicemodel['modelname'] = currentitem
                    elif colid == 'DEVICE_LOCATION':
                        devicelocation['locationname'] = currentitem
                    elif colid == 'DEVICE_PURCHASED' and len(currentitem) > 0:
                        device['purchased'] = currentitem
                    elif colid == 'STAFF_FIRST' and len(currentitem) > 0:
                        devicepersonpair['stafffirst'] = currentitem
                    elif colid == 'STAFF_LAST' and len(currentitem) > 0:
                        devicepersonpair['stafflast'] = currentitem
                    elif colid == 'DEVICE_ADNAME' and len(currentitem) > 0:
                        device['adname'] = currentitem
                    elif colid == 'DEVICE_MAC' and len(currentitem) > 0:
                        device['mac'] = currentitem
                for subindex in xrange(0,3):
                    colid = kwargs['static_%s' % subindex].encode('utf-8')
                    currentitem = kwargs['staticval_%s' % subindex].encode('utf-8')
                    if colid == 'STAFF_ID':
                        devicepersonpair['staffid'] = currentitem
                    elif colid == 'DEVICE_NOTES':
                        device['notes'] = currentitem
                    elif colid == 'DEVICE_MODEL':
                        devicemodel['modelname'] = currentitem
                    elif colid == 'DEVICE_PURCHASED':
                        device['purchased'] = currentitem
                    elif colid == 'STAFF_FIRST':
                        devicepersonpair['stafffirst'] = currentitem
                    elif colid == 'STAFF_LAST':
                        devicepersonpair['stafflast'] = currentitem
                    elif colid == 'DEVICE_LOCATION':
                        devicelocation['locationname'] = currentitem
                model = None
                dbdevice = None
                person = None
                olddevice = None
                location = None
                ownerstring = ''
                devicestring = ''
                # Set Location if Location exists
                if len(devicelocation) > 0:
                    logger.debug('Searching for: %s' % devicelocation['locationname'])
                    location = myDB.match('SELECT * from locations WHERE upper(locationname) = ?', (devicelocation['locationname'].upper(),))
                    logger.debug('Found: %s' % location)
                    if location:
                        device['locationid'] = location['locationid']
                # Set Model ID or create a new Model ID
                if len(devicemodel) > 0 and len(devicemodel['modelname']) > 2:
                    model = myDB.match('SELECT * from device_models WHERE upper(modelname) = ?', (devicemodel['modelname'].upper(),))
                    if len(model) == 0:
                        myDB.upsert('device_models',{'modelmanufacturer': '',},{'modelname': devicemodel['modelname']})
                        model = myDB.match('SELECT * from device_models WHERE upper(modelname) = ?', (devicemodel['modelname'].upper(),))
                if model:
                    devicekey['modelid'] = model['modelid']
                    # Check for tag or serial number, set the devicekey
                    if 'tag' in device.keys() and device['tag'] != '0000' and len(device['tag']) > 1:
                        devicekey['tag'] = device['tag']
                        del device['tag']
                        if 'serialnumber' not in device.keys() or len(device['serialnumber']) <= 2:
                            device['serialnumber'] = None
                    if 'serialnumber' in device.keys() and device['serialnumber'] and len(device['serialnumber']) > 2:
                        devicekey['serialnumber'] = device['serialnumber']
                        del device['serialnumber']

                    if len(devicepersonpair):
                        if 'studentid' in devicepersonpair.keys():
                            logger.debug('StudentID: %s' % devicepersonpair['studentid'])
                            person = myDB.match("SELECT * from people where identifier = ? and id LIKE ?", (devicepersonpair['studentid'], 'STU%',))
                        elif 'staffid' in devicepersonpair.keys():
                            person = myDB.match("SELECT * from people where identifier = ? and id LIKE ?", (devicepersonpair['staffid'], 'STA%',))
                        elif 'stafffirst' in devicepersonpair.keys() and 'stafflast' in devicepersonpair.keys():
                            name = devicepersonpair['stafffirst'].lower() + '.' + devicepersonpair['stafflast'].lower()
                            person = myDB.match("SELECT * from people where username = ? and id LIKE ?", (name, 'STA%',))
                    if person:
                        device['ownerid'] = person['id']
                        ownerstring = ", registered to %s" % person['username']
                    else:
                        ownerstring = ", no owner specified"
                    if any(['tag' in devicekey.keys(), 'serialnumber' in devicekey.keys()]):
                        logger.debug("Index %s: %s" % (index, devicekey))
                        if len(device) == 0:
                            device['notes'] = ''
                        cmd = 'SELECT * from devices WHERE '
                        args = []
                        for key in devicekey.keys():
                            if 'tag' in devicekey.keys() and key == 'serialnumber':
                                logger.debug('Searching without serialnumber: %s' % devicekey.keys())
                            else:
                                cmd += '%s = ? AND ' % key
                                args.append(devicekey[key])
                        cmd = cmd[:-5]
                        olddevice = myDB.action(cmd,args).fetchone()
                        if olddevice:
                            device.update(devicekey)
                            myDB.upsert('devices', device, {'deviceid': olddevice['deviceid']})
                        else:
                            myDB.upsert('devices', device, devicekey)
                        dbdevice = myDB.action(cmd,args).fetchone()
                        devicestring = "Tag: %s - Serial: %s" % (dbdevice['tag'], dbdevice['serialnumber'])
                    if dbdevice and len(dbdevice) > 0:
                        if olddevice:
                            for key in dbdevice.keys():
                                if dbdevice[key] != olddevice[key]:
                                    if key=='ownerid':
                                        oldowner = dbactions.getPerson(olddevice['ownerid'])
                                        if oldowner and len(oldowner) > 0:
                                            newowner = dbactions.getPerson(dbdevice['ownerid'])
                                            if newowner and len(newowner) > 0:
                                                dblogger.person("Device %s reassigned to %s (Import)" % (devicestring, newowner['username']), _session['user'], oldowner['id'])
                                                dblogger.person("Device %s reassigned from %s (Import)" % (devicestring, oldowner['username']), _session['user'], newowner['id'])
                                            else:
                                                dblogger.person("Device %s unassigned (Import)" % (devicestring), _session['user'], oldowner['id'])
                                        else:
                                            newowner = dbactions.getPerson(dbdevice['ownerid'])
                                            if len(newowner) > 0:
                                                dblogger.person("Device %s assigned (Import)" % (devicestring), _session['user'], newowner['id'])
                                        dblogger.person("Device ")
                                    dblogger.device("Device updated - %s: %s to %s (imported)" % (key, olddevice[key], dbdevice[key]),_session['user'], dbdevice['deviceid'])
                        else:
                            dblogger.device("Device imported", _session['user'],dbdevice['deviceid'])
                            newowner = dbactions.getPerson(dbdevice['ownerid'])
                            if newowner:
                                dblogger.device("Assigned to %s (Import)" % newowner['username'], _session['user'], dbdevice['deviceid'])
                                dblogger.person("Device %s assigned (Import)" % (devicestring), _session['user'], newowner['id'])

                        msg += "Line %s imported: %s%s<br>" % (index, devicestring, ownerstring)
                    else:
                        msg += "Line %s NOT imported<br>" % index
                else:
                    msg += "Line %s not imported: Mo Model specified<br>" % index
            dbactions.updateDeviceCache()
        else:
            msg += "<br>Failed to locate import file."
        return self.devices(msg=msg)

    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def noteDevice(self, deviceid=None, note=None):
        _session = get_session_info()
        if deviceid:
            myDB = database.DBConnection()
            device = myDB.match("SELECT * from devices WHERE deviceid=?", (deviceid,))
            if len(device) == 0:
                return serve_template(templatename="public_message.html", title="Invalid Device", msg="Device (%s) does not exist." % deviceid)
            if note:
                dblogger.device("NOTE: %s" % note, _session['user'], deviceid)
                dblogger.user('Added note to device <a href="devicePage?deviceid=%s">%s' % (device['deviceid'], device['tag']), _session['user'], _session['user'])
            else:
                return serve_template(templatename="public_message.html", title="Missing Note", msg="No note provided." % personid)
            return self.return_device(deviceid=deviceid, msg="Note Added")

    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def retireDevice(self, deviceid=None, note=None, redirect=None):
        _session = get_session_info()
        if deviceid:
            msg = ''
            myDB = database.DBConnection()
            device = myDB.match("SELECT * from devices WHERE deviceid=?", (deviceid,))
            if len(device) == 0:
                return serve_template(templatename="public_message.html", title="Invalid Device", msg="Device (%s) does not exist." % deviceid)
            else:
                if device['ownerid']:
                    oldOwner = dbactions.getPerson(device['ownerid'])
                else:
                    oldOwner = None
                myDB.upsert('devices', {'retired': 1, 'locationid': None, 'ownerid': None}, {'deviceid': deviceid})
                dblogger.device("DEVICE RETIRED: %s" % note, _session['user'], deviceid)
                dblogger.user('Retired device <a href="devicePage?deviceid=%s">%s' % (device['deviceid'], device['tag']), _session['user'], _session['user'])
                if oldOwner:
                    dblogger.person('Device retired: <a href="devicePage?deviceid=%s">%s</a>%s' % (
                    deviceid, device['tag'], note), _session['user'], oldOwner['id'])
                    dblogger.device('Removed from %s (Retired)' % (dbactions.getName(id=oldOwner['id'])), _session['user'],
                                    deviceid)
                    msg += "DEVICE RETIRED: %s (Removed from user: %s)" % (note, dbactions.getName(id=oldOwner['id']))
                else:
                    msg += "DEVICE RETIRED: %s" % note
                dbactions.updateDeviceCache(deviceid)
            if redirect == 'none':
                return msg
            else:
                return self.return_device(deviceid=deviceid, msg=msg)

    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def restoreDevice(self, deviceid=None, note=None, redirect=None):
        _session = get_session_info()
        if deviceid:
            msg = ''
            myDB = database.DBConnection()
            device = myDB.match("SELECT * from devices WHERE deviceid=?", (deviceid,))
            if len(device) == 0:
                return serve_template(templatename="public_message.html", title="Invalid Device", msg="Device (%s) does not exist." % deviceid)
            else:
                myDB.upsert('devices', {'retired': 0}, {'deviceid': deviceid})
                dbactions.updateDeviceCache(deviceid)
                dblogger.device("DEVICE RESTORED: %s" % note, _session['user'], deviceid)
                dblogger.user('Restored device <a href="devicePage?deviceid=%s">%s' % (device['deviceid'], device['tag']), _session['user'], _session['user'])
                msg += "DEVICE RESTORED: %s" % note
            if redirect == 'none':
                return msg
            else:
                return self.return_device(deviceid=deviceid, msg=msg)


    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def deviceActions(self, action='', data=None, devices=None, redirect=None, devicelist='[]', device_table_length='', **args):
        _session = get_session_info()
        devicelist = simplejson.loads(devicelist)
        myDB = database.DBConnection()
        logger.debug("Action: %s" % action)
        logger.debug("Args: %s" % args)
        if not devices:
            devices = []
            if len(devicelist) > 0:
                for devid in devicelist:
                    device = dbactions.getDeviceCache(deviceid=devid)
                    if device:
                        devices.append(device)
            else:
                for key in args.keys():
                    device = dbactions.getDeviceCache(deviceid=key)
                    if device:
                        devices.append(device)
        devicelist = []
        for device in devices:
            devicelist.append(device['deviceid'])
        if action:
            if action == "AssignDevice":
                if data:
                    data = dbactions.search(searchstring=data, type="Person")
            if action == "MoveDevice":
                data = myDB.select("SELECT * FROM locations")
            if action == "UnassignDevice":
                data = myDB.select("SELECT * FROM locations")
        return serve_template(templatename='bulkdeviceaction.html', title='Bulk Device Actions', devices=devices, redirect=redirect, action=action, data=data, devicelist=devicelist)

    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def bulkAssignDevice(self, devicelist='[]', personid=None, note=None, redirect=None):
        msg = ''
        logger.debug('devicelist: %s' % devicelist)
        devicelist = simplejson.loads(devicelist)
        logger.debug('devicelist: %s' % devicelist)
        for devid in devicelist:
            notename = 'note_%s' % devid
            kwargs = {str(devid): devid, notename: note}
            msg += self.assignDevice(personid=personid, redirect='none', **kwargs)
            msg +='<br/>'
        if redirect == "Devices":
            return self.devices(msg=msg)
        else:
            return self.home(msg=msg)

    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def bulkMoveDevice(self, devicelist='[]', locationid=None, redirect=None):
        msg = ''
        devicelist = simplejson.loads(devicelist)
        for deviceid in devicelist:
            kwargs = {'locationid': locationid}
            msg += self.deviceUpdate(deviceid=deviceid, redirect='none', **kwargs)
            msg +='<br/>'
        if redirect == "Devices":
            return self.devices(msg=msg)
        else:
            return self.home(msg=msg)

    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def bulkUnassignDevice(self, devicelist='[]', note=None, redirect=None, locationid=None):
        msg = ''
        devicelist = simplejson.loads(devicelist)
        for deviceid in devicelist:
            device = dbactions.getDevice(deviceid)
            if device['ownerid']:
                msg += self.removeDevice(personid=device['ownerid'], deviceid=deviceid, locationid=locationid, note=note, redirect='none')
            else:
                kwargs = {'locationid': locationid}
                msg += self.deviceUpdate(deviceid=deviceid, redirect='none', **kwargs)
            msg += '<br/>'
        if redirect == "Devices":
            return self.devices(msg=msg)
        else:
            return self.home(msg=msg)


    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def bulkRetireDevice(self, devicelist='[]', note=None, redirect=None):
        msg = ''
        devicelist = simplejson.loads(devicelist)
        for deviceid in devicelist:
            msg += self.retireDevice(deviceid=deviceid, note=note, redirect='none')
            msg +='<br/>'
        if redirect == "Devices":
            return self.devices(msg=msg)
        else:
            return self.home(msg=msg)

    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def bulkRestoreDevice(self, devicelist='[]', note=None, redirect=None):
        msg = ''
        devicelist = simplejson.loads(devicelist)
        for deviceid in devicelist:
            msg += self.restoreDevice(deviceid=deviceid, note=note, redirect='none')
            msg +='<br/>'
        if redirect == "Devices":
            return self.devices(msg=msg)
        else:
            return self.home(msg=msg)


    # ATTACHMENTS ######

    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def uploadAttachment(self, personid=None, deviceid=None, redirect=None, description=None, uploadfile=None, includeowner=None, **kwargs):
        msg = ''
        if uploadfile:
            devices = []
            people = []
            if deviceid:
                devices.append(deviceid)
            if includeowner and personid:
                people.append(personid)
            att = attachment.attachment()
            success, msg = att.upload(uploadfile=uploadfile,description=description, devices=devices, people=people)
        else:
            msg = "No file specified"
        if redirect == "device" and deviceid:
            return self.return_device(deviceid=deviceid, msg=msg)


    # UTILITIES ######

    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def dataUtils(self, action=None, redirect='Home'):
        _session = get_session_info()
        msg = None
        if action == 'UpdateADStudents':
            msg = dbactions.updateADStudents(session=_session)
        if action == 'UpdatePSStudents':
            msg = dbactions.updatePSStudents(session=_session)
        if action == 'UpdatePSStaff':
            msg = dbactions.updatePSStaff(session=_session)
        if action == 'UpdateStudentOverrides':
            msg = dbactions.updateStudentOverrides(session=_session)
        if action == 'UpdateStaffOverrides':
            msg = dbactions.updateStaffOverrides(session=_session)
        if action == 'UpdateClasses':
            msg = dbactions.updateClasses(session=_session)
        if action == 'UpdateSchedules':
            msg = dbactions.updateSchedules(session=_session)
        if action == 'UpdateADStaff':
            msg = dbactions.updateADStaff(session=_session)
        if action == 'CreateKindergartenUsers':
            msg = self.createStudents(session=_session, grade=0)
        if action == 'CreateG1Students':
            msg = self.createStudents(session=_session, grade=1)
        if redirect == 'Home':
            return self.home(msg=msg)

    def createStudents(self, session=None, grade=None):
        myDB = database.DBConnection()
        msg = ''
        cmd = "SELECT people.id, people.lastname, people.firstname, people.username, ps_students.studentid, ps_students.gradelevel, ad_students.distinguishedname from people LEFT JOIN ad_students ON people.username = ad_students.username LEFT JOIN ps_students ON people.identifier = ps_students.studentid WHERE people.id LIKE ? AND ad_students.distinguishedname IS NULL"
        args = ['STU%']
        if grade is not None:
            cmd += ' AND ps_students.gradelevel = ?'
            args.append(grade)
        # logger.debug('%s -- %s' % (cmd, args))
        rowlist = myDB.select(myDB.dbformat(cmd), tuple(args))
        year = schoolkeeper.CONFIG['CURRENT_SCHOOL_YEAR'] + 12 - grade
        location = 'OU=%s,%s' % (year,schoolkeeper.CONFIG['LDAP_BASESTUDENTDN'])
        for student in rowlist:
            msg += self.createUser(personid=student['id'],location=location,redirect='none')
        return msg

    @cherrypy.expose
    @requireAuth(any_of(has_access_level('Staff'),has_access_level('Admin')))
    def utilities(self, msg=None, cmd=None, data=None):
        myDB = database.DBConnection()
        title = "Utilities"
        if cmd:
            if cmd == 'getSchoolPhotosChoice' or cmd == 'getMobyMaxStudents' or cmd == 'getMobyMaxTeachers' or cmd == 'getOLSATStudents' or cmd == 'getStudentDevicesByTeacher':
                data = myDB.select('SELECT * from schools')
        else:
            pass
        return serve_template(templatename="utilities.html", title=title, msg=msg, cmd=cmd, data=data)

    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def directCert(self, importfile=None):
        success, filename, fileloc, msg = utilities.directCert(importfile)
        if success:
            return serve_download(fileloc, filename)
        else:
            return self.utilities(msg=msg)

    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def getAdobeUsers(self):
        success, filename, fileloc, msg = utilities.getAdobeUserCSV()
        if success:
            return serve_download(fileloc, filename)
        else:
            return self.utilities(msg=msg)

    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def getPearsonUsers(self):
        success, filename, fileloc, msg = utilities.getPearsonUserCSV()
        if success:
            return serve_download(fileloc, filename)
        else:
            return self.utilities(msg=msg)

    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def getSchoolPhotos(self, school=None, **kwargs):
        logger.debug(kwargs)
        if 'classname' in kwargs.keys():
            classname = kwargs['classname']
        else:
            classname = None
        success, filename, fileloc, msg = utilities.getSchoolPhotosCSV(schoolid=school, classname=classname)
        if success:
            return serve_download(fileloc, filename)
        else:
            return self.utilities(msg=msg)

    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def getMobyMaxStudents(self, schoolid=None, classname=None):
        success, filename, fileloc, msg = utilities.getMobyMaxStudents(schoolid=schoolid, classname=classname)
        if success:
            return serve_download(fileloc, filename)
        else:
            return self.utilities(msg=msg)

    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def getMobyMaxTeachers(self, schoolid=None, classname=None):
        success, filename, fileloc, msg = utilities.getMobyMaxTeachers(schoolid=schoolid, classname=classname)
        if success:
            return serve_download(fileloc, filename)
        else:
            return self.utilities(msg=msg)

    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def getOLSATStudents(self, schoolid=None):
        success, filename, fileloc, msg = utilities.getOLSATStudents(schoolid=schoolid)
        if success:
            return serve_download(fileloc, filename)
        else:
            return self.utilities(msg=msg)

    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def getKonicaAddressBook(self):
        success, filename, fileloc, msg = utilities.getKonicaAddressBookCSV()
        if success:
            return serve_download(fileloc, filename)
        else:
            return self.utilities(msg=msg)

    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def getStudentGuardianEmails(self):
        success, filename, fileloc, msg = utilities.getStudentGuardianEmails()
        if success:
            return serve_download(fileloc, filename)
        else:
            return self.utilities(msg=msg)

    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def getStudentsWithoutDevice(self):
        success, filename, fileloc, msg = utilities.getStudentsWithoutDevice()
        if success:
            return serve_download(fileloc, filename)
        else:
            return self.utilities(msg=msg)

    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def updateChromeDevices(self):
        dbactions.updateGoogleDeviceID()

    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def updateStudentGroups(self):
        msg = gam.updateStudentGroups()
        return self.utilities(msg=msg)

    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def getStudentDevicesByTeacher(self, schoolid):
        success, filename, fileloc, msg = utilities.getStudentsDevicesByTeacher(schoolid)
        if success:
            return serve_download(fileloc, filename)
        else:
            return self.utilities(msg=msg)

    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def getKnowBe4CSV(self):
        success, filename, fileloc, msg = utilities.getKnowBe4StaffCSV()
        if success:
            return serve_download(fileloc, filename)
        else:
            return self.utilities(msg=msg)

    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def getPeople(self):
        success, filename, fileloc, msg = utilities.getPeople()
        if success:
            return serve_download(fileloc, filename)
        else:
            return self.utilities(msg=msg)


    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def getPSStudents(self):
        success, filename, fileloc, msg = utilities.getPSStudents()
        if success:
            return serve_download(fileloc, filename)
        else:
            return self.utilities(msg=msg)

    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def getPSStaff(self):
        success, filename, fileloc, msg = utilities.getPSStaff()
        if success:
            return serve_download(fileloc, filename)
        else:
            return self.utilities(msg=msg)


    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def getSchools(self):
        success, filename, fileloc, msg = utilities.getSchools()
        if success:
            return serve_download(fileloc, filename)
        else:
            return self.utilities(msg=msg)

    @cherrypy.expose
    @requireAuth(any_of(has_access_level('Staff'),has_access_level('Admin'),has_access_level('Teacher')))
    def getStudentLists(self, classname=None, staffid=None):
        success, filename, fileloc, msg = utilities.getStudentLists(classname=classname, staffid=staffid)
        if success:
            dblogger.user('Downloaded Student List')
            return serve_download(fileloc, filename)

        else:
            return self.utilities(msg=msg)


    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def getDeviceModels(self):
        success, filename, fileloc, msg = utilities.getDeviceModels()
        if success:
            dblogger.user('Downloaded Device Models')
            return serve_download(fileloc, filename)

        else:
            return self.utilities(msg=msg)


    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def getDevices(self):
        success, filename, fileloc, msg = utilities.getDevices()
        if success:
            dblogger.user('Downloaded Devices')
            return serve_download(fileloc, filename)

        else:
            return self.utilities(msg=msg)


    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def getLocations(self):
        success, filename, fileloc, msg = utilities.getLocations()
        if success:
            dblogger.user('Downloaded Locations')
            return serve_download(fileloc, filename)

        else:
            return self.utilities(msg=msg)


    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def getISPROUTStudents(self):
        success, filename, fileloc, msg = utilities.getISPROUTStudents()
        if success:
            dblogger.user('Downloaded ISPROUT')
            return serve_download(fileloc, filename)

        else:
            return self.utilities(msg=msg)

    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def getClassList(self, ClassID, ClassName, ClassInstructor, msg=None):
        tempstudentlist = dbactions.getClassStudents(ClassID)
        studentlist = []
        for tempstudent in tempstudentlist:
            student = {}
            for key in tempstudent.keys():
                student[key] = tempstudent[key]
            student['devices'] = dbactions.getDevices(student['id'])
            studentlist.append(student)
        return serve_template(templatename="classlist.html", title='Class Roster', msg=msg, studentlist=studentlist, ClassName=ClassName, ClassInstructor=ClassInstructor)

    # ATTACHMENTS ######

    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def showAttachment(self, attachmentname=None):
        if attachmentname:
            filepath = os.path.join(schoolkeeper.CONFIG['FOLDER_ATTACHMENTS'], os.path.basename(attachmentname))
            if os.path.exists(filepath):
                return serve_file(filepath)

    # THREAD LABELING ######

    @staticmethod
    def label_thread(name=None):
        threadname = threading.currentThread().name
        if "Thread=" in threadname:
            if name:
                threading.currentThread().name = name
            else:
                threading.currentThread().name = "WEBSERVER"

    # USERS ######

    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def users(self):
        title = "Users"
        return serve_template(templatename="users.html", title=title, section="users")

    @cherrypy.expose
    def getUsers(self, iDisplayStart=0, iDisplayLength=100, iSortCol_0=0, sSortDir_0="desc", sSearch="", **kwargs):
        myDB = database.DBConnection()
        iDisplayStart = int(iDisplayStart)
        iDisplayLength = int(iDisplayLength)
        schoolkeeper.CONFIG['DISPLAYLENGTH'] = iDisplayLength
        cmd = 'SELECT userid,interfacelook from users'

        args = []

        rowlist = myDB.select(cmd, tuple(args))
        rows = []
        filtered = []
        logger.debug('User Rows: %s' % len(rowlist))
        if len(rowlist):
            for row in rowlist:
                temprow = []
                temprow.append(row['userid'])
                temprow.append(row['userid'])
                temprow.append(row['interfacelook'])
                rows.append(temprow)

            if sSearch:
                filtered = filter(lambda x: sSearch.lower() in str(x).lower(), rows)
            else:
                filtered = rows

            sortcolumn = int(iSortCol_0)
            filtered.sort(key=lambda x: x[sortcolumn], reverse=sSortDir_0 == "desc")

            if iDisplayLength < 0:
                rows = filtered
            else:
                rows = filtered[iDisplayStart:(iDisplayStart + iDisplayLength)]
        mydict = {'iTotalDisplayRecords': len(filtered),
                  'iTotalRecords': len(rowlist),
                  'aaData': rows,
                  }
        s = simplejson.dumps(mydict)
        return s

    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def userPage(self, UserID):
        myDB = database.DBConnection()
        user = myDB.match("SELECT * from users WHERE userid=?", (UserID,))
        username = user['userid'].encode(schoolkeeper.SYS_ENCODING)
        return serve_template(
            templatename="user.html", title=urllib.quote_plus(username), user=user, section="users")


    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def editUser(self, UserID=None, msg=None):
        self.label_thread()
        myDB = database.DBConnection()
        data = myDB.match('SELECT * from users WHERE userid=?', (UserID,))
        if data:
            response = data
        else:
            response = {
                'userid': -1,
                'interfacelook': "",
            }
        _session = get_session_info()
        if _session['user_id'] == UserID:
            title="My Preferences"
        elif response['userid'] == -1:
            title = "Add User"
        else:
            title = "Edit User"
        http_look_dir = os.path.join(schoolkeeper.PROG_DIR, 'data' + os.sep + 'interfaces')
        http_look_list = [name for name in os.listdir(http_look_dir) if os.path.isdir(os.path.join(http_look_dir, name))]
        themelist = http_look_list


        return serve_template(templatename="edituser.html", title=title, config=response, msg=msg, themelist=themelist, section="users")

    @cherrypy.expose
    @requireAuth()
    def userUpdate(self, userid='', interfacelook='', interface='', bootstrap4look=''):
        self.label_thread()
        _session = get_session_info()
        myDB = database.DBConnection()
        if userid:
            if userid != '':
                controlValueDict = {'userid': userid}
                newValueDict = {
                    'interface': interface,
                    'interfacelook': interfacelook,
                    'bootstrap4look': bootstrap4look,
                }
                myDB.upsert('users',newValueDict,controlValueDict)
                logger.info('%s modified user [%s]' % (_session['user'],userid))
                dblogger.user('Modified User %s' % userid, _session['user'], _session['user_id'])
                dblogger.user('Modified by %s' % _session['user'], userid, userid)
                logger.debug("Userid: %s - Session: %s" % (userid, _session['user_id']))
                if userid == _session['user_id']:
                    logger.debug('Users the same')
                    _session['theme'] = interface
                    if interface == 'bootstrap4':
                        _session['themelook'] = bootstrap4look
                    else:
                        _session['themelook'] = interfacelook
                    _session['user'] = userid
            raise cherrypy.HTTPRedirect("users")


    # CONFIG ######



    @cherrypy.expose
    @requireAuth(has_access_level('SuperAdmin'))
    def config(self):
        self.label_thread()
        http_look_dir = os.path.join(schoolkeeper.PROG_DIR, 'data' + os.sep + 'interfaces')
        http_look_list = [name for name in os.listdir(http_look_dir) if os.path.isdir(os.path.join(http_look_dir, name))]
        config = {
            "http_look_list": http_look_list
        }
        return serve_template(templatename="config.html", title="Settings", config=config)

    @cherrypy.expose
    @requireAuth(has_access_level('SuperAdmin'))
    def configUpdate(self, **kwargs):
        self.label_thread()
        if 'current_tab' in kwargs:
            schoolkeeper.CURRENT_TAB = kwargs['current_tab']

        interface = schoolkeeper.CFG.get('General', 'http_look')
        for key in schoolkeeper.CONFIG_DEFINITIONS.keys():
            item_type, section, default = schoolkeeper.CONFIG_DEFINITIONS[key]
            if key.lower() in kwargs:
                value=kwargs[key.lower()]
                if item_type == 'bool':
                    if not value or value == 'False' or value == '0':
                        value = 0
                    else:
                        value = 1
                elif item_type == 'int':
                    value = check_int(value, default)
                schoolkeeper.CONFIG[key] = value
            else:
                # no key is returned for strings not available in config html page so leave those unchanged
                if key in schoolkeeper.CONFIG_NONWEB or key in schoolkeeper.CONFIG_GIT:
                    pass
                #no key is returned for empty tickboxes...
                elif item_type == 'bool':
                    schoolkeeper.CONFIG[key] = 0
                # or empty string values
                else:
                    schoolkeeper.CONFIG[key] = ''

        schoolkeeper.config_write()
        checkRunningJobs()

        raise cherrypy.HTTPRedirect("config")



    # ACTIVITY ######

    @cherrypy.expose
    def getActivity(self, iDisplayStart=0, iDisplayLength=100, iSortCol_0=0, sSortDir_0="desc", sSearch="", **kwargs):
        myDB = database.DBConnection()
        iDisplayStart = int(iDisplayStart)
        iDisplayLength = int(iDisplayLength)
        cmd = ''
        args=[]
        if kwargs['source'] == "person":
            cmd = 'SELECT timestamp, activitydate, activitydesc, username from personactivity WHERE personid=?'
            args.append(kwargs['personid'])
        elif kwargs['source'] == "device":
            cmd = 'SELECT timestamp, activitydate, activitydesc, username from deviceactivity WHERE deviceid=?'
            args.append(kwargs['deviceid'])
        elif kwargs['source'] == "user":
            cmd = 'SELECT timestamp, activitydate, activitydesc, username from useractivity WHERE userid=?'
            args.append(kwargs['userid'])
        rowlist = myDB.select(myDB.dbformat(cmd), tuple(args))
        return self.rows2table(data=rowlist, iDisplayStart=iDisplayStart, iDisplayLength=iDisplayLength, iSortCol_0=iSortCol_0, sSortDir_0=sSortDir_0,sSearch=sSearch)


    # SEARCH #######

    @cherrypy.expose
    @requireAuth(any_of(has_access_level('Staff'),has_access_level('Admin')))
    def search(self, name):
        if name is None or not name:
            raise cherrypy.HTTPRedirect("home")

        myDB = database.DBConnection()
        resultlist = []
        peoplesearch = myDB.select("SELECT id, identifier, lastname, firstname, username, email from people")
        for item in peoplesearch:
            if (name.lower() in str(item['identifier'])) or (name.lower() in item['lastname'].lower()) or (name.lower() in item['firstname'].lower()) or (name.lower() in item['username'].lower()) or (name.lower() in item['email'].lower()):
                if item['id'][:3] == 'STA':
                    resultlist.append({'Type': 'Staff', 'Name': '%s, %s' % (item['lastname'], item['firstname']), 'URL': 'staffPage?UserName=%s' % item['username']})
                elif item['id'][:3] == 'STU':
                    resultlist.append({'Type': 'Student', 'Name': '%s, %s' % (item['lastname'], item['firstname']), 'URL': 'studentPage?UserName=%s' % item['username']})

        devicesearch = myDB.select("SELECT deviceid, serialnumber, tag, mac, adname from devices")
        for item in devicesearch:
            if (item['serialnumber'] and name.lower() in item['serialnumber'].lower()) or (item['tag'] and name.lower() in item['tag'].lower()) or (item['mac'] and name.lower() in item['mac'].lower()) or (item['adname'] and name.lower() in item['adname'].lower()):
                resultlist.append({'Type': 'Device', 'Name': item['tag'], 'URL': 'devicePage?deviceid=%s' % item['deviceid']})


        # logger.debug(resultlist)
        return serve_template(templatename="searchresults.html", title='Search Results: "' + name + '"', resultlist=resultlist)


    # JOB CONTROL ######

    @cherrypy.expose
    @requireAuth(has_access_level('SuperAdmin'))
    def shutdown(self):
        schoolkeeper.config_write()
        schoolkeeper.SIGNAL = 'shutdown'
        message = 'closing ... '
        return serve_template(templatename="shutdown.html", prefix='SchoolKeeper is ', title= "Close keeper", \
                                message=message, timer=15)

    @cherrypy.expose
    @requireAuth(has_access_level('SuperAdmin'))
    def restart(self):
        schoolkeeper.SIGNAL = 'restart'
        message = 'reopening ... '
        return serve_template(templatename="shutdown.html", prefix='SchoolKeeper is ', title = "Reopen keeper",\
                              message=message, timer=30)

    # UPDATES ######
    @cherrypy.expose
    @requireAuth(has_access_level('SuperAdmin'))
    def checkForUpdates(self):
        self.label_thread()
        versioncheck.checkForUpdates()
        if schoolkeeper.CONFIG['COMMITS_BEHIND'] == 0:
            if schoolkeeper.COMMIT_LIST:
                message = "unknown status"
                messages = schoolkeeper.COMMIT_LIST.replace('\n', '<br>')
                message = message + '<br><small>' + messages
            else:
                message = "up to date"
            return serve_template(templatename="shutdown.html", prefix='SchoolKeeper is ',
                                  title="Version Check", message=message, timer=5)

        elif schoolkeeper.CONFIG['COMMITS_BEHIND'] > 0:
            message = "behind by %s commit%s" % (schoolkeeper.CONFIG['COMMITS_BEHIND'],
                                                 plural(schoolkeeper.CONFIG['COMMITS_BEHIND']))
            messages = schoolkeeper.COMMIT_LIST.replace('\n', '<br>')
            message = message + '<br><small>' + messages
            return serve_template(templatename="shutdown.html", title="Commits", prefix='SchoolKeeper is ',
                                  message=message, timer=15)

        else:
            message = "unknown version"
            messages = "Your version is not recognised at<br>https://github.com/%s/%s  Branch: %s" % (
                schoolkeeper.CONFIG['GIT_USER'], schoolkeeper.CONFIG['GIT_REPO'], schoolkeeper.CONFIG['GIT_BRANCH'])
            message = message + '<br><small>' + messages
            return serve_template(templatename="shutdown.html", title="Commits", prefix='SchoolKeeper is ',
                                  message=message, timer=15)

            # raise cherrypy.HTTPRedirect("config")

    @cherrypy.expose
    @requireAuth(has_access_level('SuperAdmin'))
    def update(self):
        logger.debug('(webServe-Update) - Performing update')
        schoolkeeper.SIGNAL = 'update'
        message = 'Updating...'
        return serve_template(templatename="shutdown.html", prefix='SchoolKeeper is ', title="Updating",
                              message=message, timer=30)


    @cherrypy.expose
    @requireAuth(has_access_level('Password'))
    def resetpassword(self):
        _session = get_session_info()
        return serve_template(templatename="resetpassword.html", title="Change Password")

    @cherrypy.expose
    @requireAuth(has_access_level('Password'))
    def changepass(self, oldpass=None, newpass=None, confirmpass=None):
        _session = get_session_info()
        if oldpass and newpass and confirmpass:
            person = dbactions.getPersonByUserName(_session['user_id'])
            if newpass != confirmpass:
                return self.home(msg="New Passwords Do Not Match")

            logger.debug("Changing Pass - %s" % _session)
            myLdap = activedirectory.ldapConnection(ldapusername=_session['user_id'], ldappassword=oldpass)
            if myLdap.is_connected:
                logger.debug("Yes")
                if myLdap.changeOwnPassword(username=_session['user_id'], oldpass=oldpass, newpass=newpass):
                    msg = "Password Changed"
                    if person:
                        msg = gam.setPassword(person['email'], newpass)
                    dblogger.user("Changed own password", _session['user_id'], _session['user_id'])
                    return self.home(msg=msg)
                else:
                    return self.home(msg="Password Not Changed")
            else:
                return self.home(msg="Invalid Old Password")

    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def show_Jobs(self):
        cherrypy.response.headers['Cache-Control'] = "max-age=0,no-cache,no-store"
        # show the current status of LL cron jobs in the log
        resultlist = showJobs()
        result = ''
        for line in resultlist:
            result = result + line + '\n'
        return result

    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def showNewStudents(self):
        cherrypy.response.headers['Cache-Control'] = "max-age=0,no-cache,no-store"
        resultlist = dbactions.findNewStudents()
        result = ''
        for line in resultlist:
            result = result + '<a href="%sstudentPage?StudentID=%s">%s %s</a> (%s) Grade %s' % (schoolkeeper.CONFIG['HTTP_ROOT'], line['studentid'], line['firstname'], line['lastname'], line['username'], line['gradelevel']) + '\n'
        return result

    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def showNewStaff(self):
        cherrypy.response.headers['Cache-Control'] = "max-age=0,no-cache,no-store"
        resultlist = dbactions.findNewStaff()
        result = ''
        for line in resultlist:
            result = result + '<a href="%sstaffPage?StaffID=%s">%s %s</a> (%s) Title: %s' % (schoolkeeper.CONFIG['HTTP_ROOT'], line['psid'], line['firstname'], line['lastname'], line['username'], line['title']) + '\n'
        return result


    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def restart_Jobs(self):
        restartJobs(start='Restart')
        # and list the new run-times in the log
        return self.show_Jobs()

    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def stop_Jobs(self):
        restartJobs(start='Stop')
        # and list the new run-times in the log
        return self.show_Jobs()

    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def pivot(self):
        dbactions.getPivotData()
        dbactions.getPowerSchoolChanges()

    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def transfinder(self):
        import transfinder
        transfinder.convertTransfinderData()

    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def powerschool(self):
        import powerschool
        powerschool.updatePowerSchoolEntries()

    @cherrypy.expose
    @requireAuth(has_access_level('Admin'))
    def efunds(self):
        import efunds
        funds = efunds.efundsConnection()
        outfile = funds.balanceConversion(schoolkeeper.CONFIG['EFUNDS_IMPORT_FILE'],
                                          schoolkeeper.CONFIG['EFUNDS_EXPORT_FILE'])
        if outfile:
            response = funds.upload(outfile)
            logger.debug(response)


    def getOUs(self, baseDN=None):
        myLDAP = activedirectory.ldapConnection()
        results = myLDAP.getOUs(baseDN=baseDN)
        tempOUList = []
        for entry in results:
            entry_dn = entry['distinguishedName'].split(',')
            entry_name = ""
            for entry_line in entry_dn:
                if "OU=" in entry_line:
                    entry_piece = entry_line.split("=")[1]
                    entry_name = "/" + entry_piece + entry_name
            entry_result = {'dn': entry['distinguishedName'], 'name': entry_name}
            tempOUList.append(entry_result)
        OUList = sorted(tempOUList, key=lambda k: k['name'])
        return OUList


    def rows2table(self, data=[], iDisplayStart=0, iDisplayLength=100, iSortCol_0=0, sSortDir_0="desc", sSearch=""):
        filtered = []
        rows = []
        if len(data):
            for row in data:
                rows.append(list(row))
            if sSearch:
                filtered = filter(lambda x: sSearch.lower() in str(x).lower(), rows)
            else:
                filtered = rows

            sortcolumn = int(iSortCol_0)
            filtered.sort(key=lambda x: x[sortcolumn], reverse=sSortDir_0 == "desc")

            if iDisplayLength < 0:
                rows = filtered
            else:
                rows = filtered[iDisplayStart:(iDisplayStart + iDisplayLength)]
        mydict = {'iTotalDisplayRecords': len(filtered),
                  'iTotalRecords': len(data),
                  'aaData': rows,
                  }
        s = simplejson.dumps(mydict)
        return s
