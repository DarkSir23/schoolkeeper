from __future__ import with_statement

import ldap3
import threading
import time
import traceback

import schoolkeeper
from schoolkeeper import logger
from ldap3 import Server, Connection, ALL, MODIFY_REPLACE

class ldapConnection:
    def __init__(self, ldapusername=None, ldappassword=None, ldapdomain=None):
        self.is_connected = False
        if not ldapusername:
            ldapusername = schoolkeeper.CONFIG['LDAP_USER']
        if not ldappassword:
            ldappassword = schoolkeeper.CONFIG['LDAP_PASS']
        if not ldapdomain:
            ldapdomain = schoolkeeper.CONFIG['LDAP_DOMAIN']
        #ldap.set_option(ldap.OPT_X_TLS_REQUIRE_CERT, ldap.OPT_X_TLS_NEVER)
        try:
            #self.connection = ldap.initialize("ldaps://%s" % schoolkeeper.CONFIG['LDAP_SERVER'])
            self.connection = ldap3.Connection(schoolkeeper.CONFIG['LDAP_SERVER'], '%s@%s' % (ldapusername, ldapdomain), ldappassword, auto_bind=True)
            self.connection.start_tls()
            self.connection.protocol_version = 3
            # self.connection.set_option(ldap.OPT_REFERRALS,0)
            self.ldapuser = ldapusername
            # logger.debug("TEMP: %s - %s" % ('%s' % ldapusername, ldappassword))
            # self.connection.simple_bind_s('%s@%s' % (ldapusername, ldapdomain), ldappassword)
            self.is_connected = True
        except Exception as e:
            logger.error("LDAP: %s" % e)

    def adsearch(self, type='Students', searchstring=None):
        retrieveAttributes = ['sn', 'givenName', 'displayName', 'sAMAccountName', 'description', 'objectCategory', 'mail', 'distinguishedName']
        if type=='Students':
            baseDN = schoolkeeper.CONFIG['LDAP_BASESTUDENTDN']
        elif type=='Staff':
            baseDN = schoolkeeper.CONFIG['LDAP_BASESTAFFDN']
        elif type=='Device':
            baseDN = schoolkeeper.CONFIG['LDAP_BASEDEVICEDN']
            retrieveAttributes = ['cn', 'description', 'distinguishedName', 'dNSHostName', 'lastLogonTimestamp', 'name', 'sAMAccountName', 'objectCategory']
        searchScope = ldap3.SUBTREE
        searchFilter = "(cn=*)"
        if searchstring:
            searchFilter = "(cn=*%s)" % searchstring
        result_set = []
        try:
            self.connection.search(search_base=baseDN, search_scope=searchScope, search_filter=searchFilter, attributes=retrieveAttributes)
            for entry in self.connection.response:
                # logger.debug("Entry: %s" % entry)
                if entry.get('dn') and entry.get('attributes'):
                    result_set.append(entry)
        except Exception as e:
            logger.error("LDAP: %s" % e)
        # logger.debug(result_set)
        return result_set

    def getOUs(self, baseDN=None):
        if not baseDN:
            baseDN = schoolkeeper.CONFIG['LDAP_BASEDN']
        retrieveAttributes = ['distinguishedName']
        searchScope = ldap3.SUBTREE
        searchFilter="(objectClass=organizationalunit)"
        result_set = []
        try:
            self.connection.search(search_base=baseDN, search_scope=searchScope, search_filter=searchFilter, attributes=retrieveAttributes)
            for entry in self.connection.response:
                if entry.get('dn') and entry.get('attributes'):
                    result_set.append(entry.get('attributes'))
        except Exception as e:
            logger.error("Traceback: %s" % traceback.print_exc())
            logger.error("LDAP: %s" % e)
            return {'Error': e}
        return result_set

    def getlogininfo(self, username=None):
        baseDN = schoolkeeper.CONFIG['LDAP_BASEDN']
        retrieveAttributes = ['sn', 'givenName', 'displayName', 'sAMAccountName', 'description', 'objectCategory',
                              'mail', 'distinguishedName', 'department']
        searchScope = ldap3.SUBTREE
        if username:
            searchFilter = "(sAMAccountName=%s)" % username
        else:
            searchFilter = "(sAMAccountName=%s)" % self.ldapuser
        result_set = []
        try:
            self.connection.search(search_base=baseDN, search_scope=searchScope, search_filter=searchFilter, attributes=retrieveAttributes)
            for entry in self.connection.response:
                if entry.get('dn') and entry.get('attributes'):
                    # logger.debug(entry)
                    result_set.append(entry.get('attributes'))
        except Exception as e:
            logger.error("Traceback: %s" % traceback.print_exc())
            logger.error("LDAP: %s" % e)
            return {'Error': e}
        if len(result_set):
            return result_set[0]
        else:
            return None

    def checkldapgroups(self, dn=None, groupName=None):
        # logger.debug("DN: %s" % dn)
        baseDN = schoolkeeper.CONFIG['LDAP_BASEDN']
        retrieveAttributes = ['cn',]
        searchScope = ldap3.SUBTREE
        searchFilter = '(|(&(objectClass=*)(member=%s)))' % dn

        try:
            self.connection.search(search_base=baseDN, search_scope=searchScope, search_filter=searchFilter, attributes=retrieveAttributes)
            for entry in self.connection.response:
                if entry.get('dn') and entry.get('attributes'):
                    logger.debug(entry)
                    if entry.get('attributes')['cn'] == groupName:
                        # logger.info("%s - %s" % (result_data, groupName))
                        # logger.debug('Returning True')
                        return True
        except Exception as e:
            logger.error("LDAP: %s" % e)
            return {'Error': e}
        logger.debug('Returning False')
        return False


    def getldapgroups(self, dn=None):
        baseDN = schoolkeeper.CONFIG['LDAP_BASEDN']
        retrieveAttributes = ['cn',]
        searchScope = ldap.SCOPE_SUBTREE
        searchFilter = '(|(&(objectClass=*)(member=%s)))' % dn

        result_set = []
        try:
            ldap_result_id = self.connection.search(baseDN, searchScope, searchFilter, retrieveAttributes)
            while 1:
                result_type, result_data = self.connection.result(ldap_result_id, 0)
                if (result_data == []):
                    break
                else:
                    if result_type == ldap.RES_SEARCH_ENTRY:
                        result_set.append(result_data)
        except Exception as e:
            logger.error("LDAP: %s" % e)
            return {'Error': e}
        return result_set

    def changeOwnPassword(self, username=None, oldpass=None, newpass=None):
        if username and oldpass and newpass:
            info = self.getlogininfo()
            result = ldap3.extend.microsoft.modifyPassword.ad_modify_password(self.connection, info['distinguishedName'], newpass, oldpass, controls=None)
            return result

    def setPassword(self, username=None, password=None, unlock=False):
        if username and password:
            info = self.getlogininfo(username=username)
            result = ''
            boolresult = True
            if unlock:
                unlock_resp = ldap3.extend.microsoft.unlockAccount.ad_unlock_account(self.connection, info['distinguishedName'])
                if unlock_resp:
                    result += "Unlocked.<br>"
                else:
                    result += "Not unlocked.<br>"
                    boolresult = False
            pw_resp = ldap3.extend.microsoft.modifyPassword.ad_modify_password(self.connection, info['distinguishedName'], password, None, controls=None)
            if pw_resp:
                result += "Password set.<br>"
            else:
                result += "Password not set.<br>"
                boolresult = False
            if unlock:
                changeUACattribute = {"userAccountControl": (MODIFY_REPLACE, [512])}
                change_resp = self.connection.modify(info['distinguishedName'],changes=changeUACattribute)
                if change_resp:
                    result += "Attribute changed.<br>"
                else:
                    result += "Attribute not changed.<br>"
                    boolresult = False
            return boolresult, result


    def moveUser(self, olddn=None, cn=None, newdn=None):
        if olddn and cn and newdn:
            logger.debug("Old: %s - New: %s - CN: %s" % (olddn, newdn, cn))
            result = self.connection.modify_dn(olddn, cn, new_superior=newdn)
            return result
