import schoolkeeper
from schoolkeeper import logger, database
import os
import csv

def updatePowerSchoolEntries():
    myDB = database.DBConnection()
    studentlist = myDB.select("SELECT username, email, ps_students.studentid, psusername, pspassword, studentemail, cafepin from people, ps_students WHERE people.identifier = ps_students.studentid AND people.id LIKE ? AND ps_students.gradelevel > -1", ('STU%',))
    csvlist = []
    for entry in studentlist:
        password = entry['studentid']
        if entry['cafepin'] and len(str(entry['cafepin'])) > 2:
            password = entry['cafepin']
        if entry['psusername'] != entry['username'] or entry['studentemail'] != entry['email'] or entry['pspassword'] != password:
            newline = [entry['studentid'], entry['username'], password, entry['email'],1,1]
            csvlist.append(newline)
    outfilename = '/home/import/import/powerschool/update_records.csv'
    try:
        with open(outfilename, 'wb') as csvfile:
            writer = csv.writer(csvfile, delimiter='\t', lineterminator='\r\n', quoting=csv.QUOTE_MINIMAL)
            for line in csvlist:
                writer.writerow(line)
        msg = '[POWERSCHOOL] Output file created.'
        logger.debug(msg)
        return msg
    except Exception as e:
        msg = 'Could not write output file: %s' % e
        logger.debug(msg)
        return msg
