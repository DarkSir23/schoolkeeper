import logger
from subprocess import Popen, PIPE
import shlex
import schoolkeeper
import dbactions
import lib.simplejson as simplejson
import os
import dblogger

def get(cmd):
    command = "%s %s" % (schoolkeeper.CONFIG['GAMADV_PATH'], cmd)
    logger.debug(command)
    process = Popen(shlex.split(command), stdout=PIPE, stderr=PIPE)
    out, err = process.communicate()
    exit_code = process.returncode
    if exit_code == 0:
        return out
    else:
        return None

def getClassGroups(grade=None):
    classGroups = {}
    if schoolkeeper.CONFIG['GROUPS_ENABLE']:
        for grade in range(schoolkeeper.CONFIG['GROUPS_MIN_GRADELEVEL'], 13):
            gradyear = schoolkeeper.CONFIG['CURRENT_SCHOOL_YEAR'] + 12 -  grade
            address = 'class.of.%s@%s' % (gradyear, schoolkeeper.CONFIG['CORP_DOMAIN'])
            classGroups[grade] = getGroupInfo(address)
    return classGroups

def getGroupInfo(group):
    groupinfo = None
    try:
        groupinfo = simplejson.loads(get('info group %s formatjson' % group))
    except Exception as e:
        logger.debug('Unable to find google group: %s - %s' % (group, e))
    return groupinfo

def setPassword(email, passwd):
    try:
        passmsg = get('update user %s password %s' % (email, passwd))
    except Exception as e:
        logger.debug('Unable to set password for %s: %s' % (email, e))
        passmsg = ('Error: %s' % e)
    return passmsg


def updateStudentGroups():
    msg = ''
    activeStudents = dbactions.getActiveStudents()
    classGroups = getClassGroups()
    logger.debug('Students: %s - Groups: %s' % (len(activeStudents), len(classGroups)))
    for student in activeStudents:
        grade = int(student['gradelevel'])
        if grade in classGroups.keys():
            if not checkGroupMembership(student, classGroups[grade]):
                msg += addGroupUser(student, classGroups[grade]) + '\n'
    return msg

def checkGroupMembership(account, group):
    found = False
    for member in group['members']:
        if account['email'] == member['email']:
            found = True
    return found

def addGroupUser(account, group, level='member'):
    msg = get('update group %s add %s user %s' % (group['email'], level, account['email']))
    return msg

def uploadPhoto(account, photopath=None, session=None):
    msg = ''
    person = dbactions.getPerson(PersonID=account)
    if not photopath:
        photopath = schoolkeeper.images.getPictureFilename(account)
    else:
        if not os.path.exists(photopath):
            photopath = None
    if not photopath:
        msg += "Photo not found.  Nothing Done."
        return
    else:
        msg += get('user %s update photo %s' % (person['email'], photopath))
        user = 'System'
        if session:
            user = session['user']
        dblogger.person(msg, user, account)
        msg += "\nNOTE: It takes up to 24 hours before Google will show the image in SchoolKeeper."
    return msg