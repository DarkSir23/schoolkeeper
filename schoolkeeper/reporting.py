import hashlib
import os

import schoolkeeper
from schoolkeeper import database, dbactions, logger, activedirectory, dblogger, pivot
from schoolkeeper.common import intToYesNo, currentTime, dbDateFormat
import datetime
from plotly.offline import plot
from plotly.graph_objs import Scatter, Pie

def getDevicesPerBuilding():
    myDB = database.DBConnection()
    devices = dbactions.getDeviceCache()
    devicecount = {}
    labels = []
    values = []
    for device in devices:
        if device['schoolabbrev'] in devicecount.keys():
            devicecount[device['schoolabbrev']] += 1
        else:
            devicecount[device['schoolabbrev']] = 1
    for key in devicecount.keys():
        labels.append(key)
        values.append(devicecount[key])
    return plot([Pie(labels=labels, values=values, marker=dict(line=dict(color='#000000', width=1)))],output_type='div')

def getDevicesPerType():
    myDB = database.DBConnection()
    devices = dbactions.getDeviceCache()
    devicecount = {}
    labels = []
    values = []
    for device in devices:
        if device['modeltype'] in devicecount.keys():
            devicecount[device['modeltype']] += 1
        else:
            devicecount[device['modeltype']] = 1
    for key in devicecount.keys():
        labels.append(key)
        values.append(devicecount[key])
    return plot([Pie(labels=labels, values=values, marker=dict(line=dict(color='#000000', width=1)))],output_type='div')

