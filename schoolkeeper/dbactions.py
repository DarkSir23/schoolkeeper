import uuid
import hashlib
import csv
import os
import lib.simplejson as simplejson

import schoolkeeper
from schoolkeeper import database, logger, activedirectory, dblogger, pivot
from schoolkeeper.common import intToYesNo, currentTime, dbDateFormat, checkint
import datetime
import gam

from StringIO import StringIO

def updateSchoolSomething(StudentID):
    myDB = database.DBConnection()
    event = myDB.match("SELECT * from students where studentid=?", (StudentID,))

# search Active Directory for a string, and return all results
def getADSearch(type=None, searchstring=None):
    ldapresults = []
    if type:
        myLDAP = activedirectory.ldapConnection()
        results = myLDAP.adsearch(type=type, searchstring=searchstring)
        for result in results:
            data = result['attributes']
            entry = {}
            for key in data.keys():
                entry[key] = getADVal(data,key)
            ldapresults.append(entry)
        return ldapresults

# Import Active Directory students into database
def updateADStudents(session=None):
    myDB = database.DBConnection()
    myLDAP = activedirectory.ldapConnection()
    results = myLDAP.adsearch(type="Students")
    checksum = hashlib.md5(str(results)).hexdigest()
    msg = None
    # logger.debug(checksum)
    lastChecksum = myDB.match('SELECT checksum from checksums WHERE id=?', ('LDAP',))
    if lastChecksum and str(checksum) == lastChecksum['checksum']:
        msg = "No new updates"
    else:
        if len(results) > 0:
            myDB.action("DELETE from ad_students")
            # logger.debug("Results: %s" % len(results))
            dbcount = 0
            for result in results:
                data = result['attributes']
                if "Person" not in getADVal(data,'objectCategory'):
                    logger.debug(data)
                else:
                    controlValueDict = {'username': getADVal(data,'sAMAccountName').lower()}
                    newValueDict = {
                        'firstname': getADVal(data,'givenName'),
                        'lastname': getADVal(data,'sn'),
                        'displayname': getADVal(data,'displayName'),
                        'description': getADVal(data,'description'),
                        'email': getADVal(data,'mail'),
                        'distinguishedname': getADVal(data,'distinguishedName')
                    }
                    # logger.debug('%s - %s' % (controlValueDict, newValueDict))
                    myDB.upsert('ad_students', newValueDict, controlValueDict)
                    dbcount += 1
            msg = "AD Students Updated (%s imported)" % dbcount
            myDB.upsert('checksums',{'checksum': str(checksum)}, {'id': 'LDAP'})
        else:
            msg = "Unable to retrieve AD Students"
    return msg

# Import Active Directory staff into database
def updateADStaff(session=None):
    myDB = database.DBConnection()
    myLDAP = activedirectory.ldapConnection()
    results = myLDAP.adsearch(type="Staff")
    checksum = hashlib.md5(str(results)).hexdigest()
    msg = None
    logger.debug(checksum)
    lastChecksum = myDB.match('SELECT checksum from checksums WHERE id=?', ('LDAPSTAFF',))
    if lastChecksum and str(checksum) == lastChecksum['checksum']:
        msg = "No new updates"
    else:
        if len(results) > 0:
            myDB.action("DELETE from ad_staff")
            dbcount = 0
            for result in results:
                data = result['attributes']
                if "Person" not in getADVal(data,'objectCategory'):
                    logger.debug(data)
                else:
                    controlValueDict = {'username': getADVal(data,'sAMAccountName').lower()}
                    newValueDict = {
                        'firstname': getADVal(data,'givenName'),
                        'lastname': getADVal(data,'sn'),
                        'displayname': getADVal(data,'displayName'),
                        'description': getADVal(data,'description'),
                        'email': getADVal(data,'mail'),
                        'distinguishedname': getADVal(data,'distinguishedName')
                    }
                    # logger.debug('%s - %s' % (controlValueDict, newValueDict))
                    myDB.upsert('ad_staff', newValueDict, controlValueDict)
                    dbcount += 1
            msg = "AD Staff Updated (%s imported)" % dbcount
            myDB.upsert('checksums',{'checksum': str(checksum)}, {'id': 'LDAPSTAFF'})
        else:
            msg = "Unable to retrieve AD Staff"
    return msg

# Import PowerSchool student export file, if changed.
def updatePSStudents(session=None):
    filename = schoolkeeper.CONFIG['FILE_PS_STUDENTS']
    file = open(filename, "r")
    checksum = hashlib.md5(file.read()).hexdigest()
    file.close()
    msg = None
    myDB = database.DBConnection()
    lastChecksum = myDB.match('SELECT checksum from checksums WHERE id=?', (filename,))
    if lastChecksum and str(checksum) == lastChecksum['checksum']:
        msg = "No new updates"
    else:
        with open(filename) as csvfile:
            headers = ['psid', 'studentid', 'lastname', 'firstname', 'studentemail', 'psusername', 'pspassword', 'schoolid', 'dob', 'gradelevel', 'stn', 'gender', 'ethnicitycode', 'cafepin', 'insurance_decline', 'insurance_form', 'insurance_pmt', 'guardian_email', 'ss_status', 'ss_primary', 'ss_secondary', 'lunch_status', 'ell_status', 'teacher_of_record']
            intheaders = ['psid', 'studentid', 'schoolid', 'gradelevel', 'ethnicitycode', 'cafepin', 'insurance_decline', 'insurance_form', 'ss_status', 'ss_primary', 'ss_secondary']
            detailheaders = ['guardian_email', 'ss_status', 'ss_primary', 'ss_secondary', 'lunch_status', 'ell_status', 'teacher_of_record']
            valueheaders = list(headers)
            valueheaders.remove('psid')
            reader = csv.DictReader(csvfile, headers, delimiter='\t')
            readerlist=list(reader)
            # logger.debug(len(readerlist))
            dbcount = 0
            if len(readerlist):
                myDB.action('DELETE from ps_students')
                for row in readerlist:
                    # logger.debug(row)
                    controlValueDict = {}
                    newValueDict = {}
                    detailsDict = {}
                    for key in row.keys():
                        if key in intheaders:
                            value = checkint(row[key])
                        else:
                            value = row[key]
                        if key in detailheaders:
                            detailsDict[key] = value
                        elif key in valueheaders:
                            newValueDict[key] = value
                        else:
                            controlValueDict[key] = value
                    newValueDict['details'] = simplejson.dumps(detailsDict)
                    myDB.upsert('ps_students', newValueDict, controlValueDict)
                    updatePeople(row, session=session)
                    dbcount += 1
                msg = "PS Students Updated (%s imported)" % dbcount
                myDB.upsert('checksums', {'checksum': str(checksum)}, {'id': filename})
    return msg

# Import PowerSchool staff export file, if changed.
def updatePSStaff(session=None):
    filename = schoolkeeper.CONFIG['FILE_PS_STAFF']
    file = open(filename, "r")
    checksum = hashlib.md5(file.read()).hexdigest()
    file.close()
    msg = None
    myDB = database.DBConnection()
    lastChecksum = myDB.match('SELECT checksum from checksums WHERE id=?', (filename,))
    if lastChecksum and str(checksum) == lastChecksum['checksum']:
        msg = "No new updates"
    else:
        with open(filename) as csvfile:
            headers = ['schoolid', 'psid', 'firstname', 'lastname', 'email', 'firstdotlast', 'psusername', 'status', 'spn', 'title', 'teachernumber']
            intheaders = ['schoolid', 'psid', 'status', 'spn', 'teachernumber']
            valueheaders = list(headers)
            valueheaders.remove('psid')
            reader = csv.DictReader(csvfile, headers)
            readerlist=list(reader)
            logger.debug(len(readerlist))
            dbcount = 0
            if len(readerlist):
                myDB.action('DELETE from ps_staff')
                for row in readerlist:
                    controlValueDict = {}
                    newValueDict = {}
                    for key in row.keys():
                        if key in intheaders:
                            value = checkint(row[key])
                        else:
                            value = row[key]
                        if key in valueheaders:
                            newValueDict[key] = value
                        else:
                            controlValueDict[key] = value

                    myDB.upsert('ps_staff', newValueDict, controlValueDict)
                    updatePeople(row, type="Staff", session=session)
                    dbcount += 1
                msg = "PS Staff Updated (%s imported)" % dbcount
                myDB.upsert('checksums', {'checksum': str(checksum)}, {'id': filename})
    return msg

# Return first result if in a list or tuple.  Format dates as timestamp strings.
def getADVal(record, key):
    if type(record[key]) in [unicode,list,tuple]:
        if key in record and len(record[key]) > 0:
            if type(record[key]) in [list,tuple]:
                return record[key][0]
            else:
                return record[key]
    elif type(record[key]) in [datetime.datetime]:
        return record[key].strftime('%Y-%m-%dT%H:%M')
    else:
        logger.debug('%s - %s - %s' % (key, record[key], type(record[key])))
        return ""

# Return the generated username for a person, taking overrides into consideration.
def getUserName(id,FirstName=None, LastName=None):
    myDB = database.DBConnection()
    if not FirstName and not LastName:
        person = myDB.match('SELECT identifier FROM people WHERE id=?', (id,))
        if id.startswith('STA'):
            dbname = myDB.match('SELECT firstname, lastname from ps_staff WHERE psid=?', (person['identifier'],))
            if dbname and len(dbname):
                ps_override = myDB.match('SELECT forcelastname, forcefirstname from ps_staff_override WHERE psid=?', (person['identifier'],))
            else:
                dbname = myDB.match('SELECT firstname, lastname from people WHERE id=?', (id,))
                ps_override = []
        elif id.startswith('STU'):
            dbname = myDB.match('SELECT firstname, lastname, psid from ps_students WHERE studentid=?', (person['identifier'],))
            if dbname and len(dbname):
                ps_override = myDB.match(
                'SELECT forcemiddleinitial, forcelastname, forcefirstname from ps_student_override WHERE psid=?', (dbname['psid'],))
            else:
                dbname = myDB.match('SELECT firstname, lastname from people WHERE id=?', (id,))
                ps_override = []
        FirstName = dbname['firstname']
        LastName = dbname['lastname']
    if len(ps_override):
        username = ""
        if str(ps_override['forcefirstname']) != "":
            username += "%s." % ps_override['forcefirstname'].lower()
        else:
            username += "%s." % FirstName.lower()
        if 'forcemiddleinitial' in ps_override.keys() and str(ps_override['forcemiddleinitial']) != "":
            username += "%s." % ps_override['forcemiddleinitial'].lower()
        if str(ps_override['forcelastname']) != "":
            username += ps_override['forcelastname'].lower()
        else:
            username += LastName.lower()
        return username
    else:
        return "%s.%s" % (FirstName.lower(),LastName.lower())

# Return the generated display name for a person, taking overrides into consideration.
def getName(id,FirstName=None, LastName=None):
    myDB = database.DBConnection()
    person = myDB.match('SELECT identifier FROM people WHERE id=?', (id,))
    if id.startswith('STA'):
        dbname = myDB.match('SELECT firstname, lastname from ps_staff WHERE psid=?', (person['identifier'],))
        if dbname and len(dbname):
            ps_override = myDB.match('SELECT forcelastname, forcefirstname from ps_staff_override WHERE psid=?', (person['identifier'],))
        else:
            dbname = myDB.match('SELECT firstname, lastname from people WHERE id=?', (id,))
            ps_override = []
    elif id.startswith('STU'):
        dbname = myDB.match('SELECT firstname, lastname, psid from ps_students WHERE studentid=?', (person['identifier'],))
        if dbname and len(dbname):
            ps_override = myDB.match(
            'SELECT forcemiddleinitial, forcelastname, forcefirstname from ps_student_override WHERE psid=?', (dbname['psid'],))
        else:
            dbname = myDB.match('SELECT firstname, lastname from people WHERE id=?', (id,))
            ps_override = []
    if not FirstName and not LastName:
        FirstName = dbname['firstname']
        LastName = dbname['lastname']
    if len(ps_override):
        name = ""
        if str(ps_override['forcefirstname']) != "":
            name += "%s " % ps_override['forcefirstname']
        else:
            name += "%s " % FirstName
        if 'forcemiddleinitial' in ps_override.keys() and str(ps_override['forcemiddleinitial']) != "":
            name += "%s. " % ps_override['forcemiddleinitial']
        if str(ps_override['forcelastname']) != "":
            name += ps_override['forcelastname']
        else:
            name += LastName
        return name
    else:
        return "%s %s" % (FirstName,LastName)

# Update the people table.  Triggered when powerschool data gets changed.
def updatePeople(datarow, type='Student', session=None):
    myDB = database.DBConnection()
    if type == 'Staff':
        ps_override = myDB.match('SELECT ignore, forcefirstname, forcelastname, forceusername from ps_staff_override WHERE psid=?', (datarow['psid'],))
        newid = "STA%05d" % int(datarow['psid'])
    else:
        ps_override = myDB.match('SELECT forcemiddleinitial, forcelastname, forcefirstname from ps_student_override WHERE psid=?', (datarow['psid'],))
        newid = "STU%05d" % int(datarow['psid'])
    username = ""
    personname = ""
    ignore = 0
    if len(ps_override):
        if str(ps_override['forcefirstname']) != "":
            username += "%s." % ps_override['forcefirstname'].lower()
            personname += "%s " % ps_override['forcefirstname']
            firstname = ps_override['forcefirstname']
        else:
            username += "%s." % datarow['firstname'].lower()
            personname += "%s " % datarow['firstname']
            firstname = datarow['firstname']
        if 'forcemiddleinitial' in ps_override.keys() and str(ps_override['forcemiddleinitial']) != "":
            username += "%s." % ps_override['forcemiddleinitial'].lower()
            personname += "%s. " % ps_override['forcemiddleinitial']
        if str(ps_override['forcelastname']) != "":
            username += ps_override['forcelastname'].lower()
            personname += ps_override['forcelastname']
            lastname = ps_override['forcelastname']
        else:
            username += datarow['lastname'].lower()
            personname += datarow['lastname']
            lastname = datarow['lastname']
        if 'ignore' in ps_override.keys():
            ignore = ps_override['ignore']

    else:
        username = "%s.%s" % (datarow['firstname'].lower(),datarow['lastname'].lower())
        personname = "%s %s" % (datarow['firstname'],datarow['lastname'])
        firstname = datarow['firstname']
        lastname = datarow['lastname']
    controlValueDict = {
        'id': newid,
    }
    identifier = None
    if 'studentid' in datarow.keys():
        identifier = datarow['studentid']
    else:
        identifier = datarow['psid']
    newValueDict = {
        'identifier': identifier,
        'lastname': lastname,
        'firstname': firstname,
        'email': "%s@%s" % (username, schoolkeeper.CONFIG['CORP_DOMAIN']),
        'username': username,
        'ignore': ignore,
        'schoolid': datarow['schoolid'],
    }
    user = "System"
    if session:
        user = session['user']
    oldentry = myDB.match('SELECT * from people WHERE id=?', (newid,))
    myDB.upsert('people', newValueDict, controlValueDict)
    newentry = myDB.match('SELECT * from people WHERE id=?', (newid,))
    if len(oldentry) == 0:
        dblogger.person('New entry created for %s' % personname, user, newid)
    else:
        for key in newentry.keys():
            if oldentry[key] != newentry[key]:
                dblogger.person('%s changed for %s' % (key, personname), user, newid)

# Import the classes
def updateClasses(session=None):
    myDB = database.DBConnection()
    import_files = os.listdir(schoolkeeper.CONFIG['FOLDER_PS_CLASSES'])
    newdata = False
    files = []
    for file in import_files:
        filename = os.path.join(schoolkeeper.CONFIG['FOLDER_PS_CLASSES'],file)
        file = open(filename, "r")
        checksum = hashlib.md5(file.read()).hexdigest()
        file.close()
        lastChecksum = myDB.match('SELECT checksum from checksums WHERE id=?', (filename,))
        if not lastChecksum or str(checksum) != lastChecksum['checksum']:
            newdata = True
        files.append(filename)
    if not newdata:
        return "No Class Changes"
    else:
        myDB.action('DELETE from classes')
        dbcount = 0
        for filename in files:
            file = open(filename, "r")
            checksum = hashlib.md5(file.read()).hexdigest()
            file.close()
            with open(filename) as csvfile:
                headers = ['schoolid', 'staffid', 'classid', 'classname', 'period', 'firstday', 'lastday', 'room', 'gradelevel', 'term', 'statecourseid']
                intheaders = ['schoolid', 'staffid', 'classid', 'gradelevel', 'statecourseid']
                valueheaders = list(headers)
                valueheaders.remove('classid')
                reader = csv.DictReader(csvfile, headers)
                readerlist = list(reader)
                if len(readerlist):
                    for row in readerlist:
                        controlValueDict = {}
                        newValueDict = {}
                        for key in row.keys():
                            if key in intheaders:
                                value = checkint(row[key])
                            else:
                                value = row[key]
                            if key in valueheaders:
                                newValueDict[key] = value
                            else:
                                controlValueDict[key] = value

                        myDB.upsert('classes', newValueDict, controlValueDict)
                        dbcount += 1
                myDB.upsert('checksums', {'checksum': str(checksum)}, {'id': filename})
        msg = "Classes Updated (%s imported)" % dbcount
        return msg

def updateSchedules(session=None):
    myDB = database.DBConnection()
    import_files = os.listdir(schoolkeeper.CONFIG['FOLDER_PS_SCHEDULES'])
    newdata = False
    files = []
    for file in import_files:
        filename = os.path.join(schoolkeeper.CONFIG['FOLDER_PS_SCHEDULES'],file)
        file = open(filename, "r")
        checksum = hashlib.md5(file.read()).hexdigest()
        file.close()
        lastChecksum = myDB.match('SELECT checksum from checksums WHERE id=?', (filename,))
        if not lastChecksum or str(checksum) != lastChecksum['checksum']:
            newdata = True
        files.append(filename)
    if not newdata:
        return "No Schedule Changes"
    else:
        myDB.action('DELETE from schedules')
        dbcount = 0
        for filename in files:
            file = open(filename, "r")
            checksum = hashlib.md5(file.read()).hexdigest()
            file.close()
            with open(filename) as csvfile:
                headers = ['psid', 'schoolid', 'classid']
                intheaders = ['psid', 'schoolid', 'classid']
                valueheaders = list(headers)
                valueheaders.remove('psid')
                valueheaders.remove('classid')
                reader = csv.DictReader(csvfile, headers)
                readerlist = list(reader)
                if len(readerlist):
                    for row in readerlist:
                        controlValueDict = {}
                        newValueDict = {}
                        for key in row.keys():
                            if key in intheaders:
                                value = checkint(row[key])
                            else:
                                value = row[key]
                            if key in valueheaders:
                                newValueDict[key] = value
                            else:
                                controlValueDict[key] = value

                        myDB.upsert('schedules', newValueDict, controlValueDict)
                        dbcount += 1
                myDB.upsert('checksums', {'checksum': str(checksum)}, {'id': filename})
        msg = "Schedules Updated (%s imported)" % dbcount
        return msg

def getSchedule(StudentID=None):
    if StudentID:
        myDB = database.DBConnection()
        today = currentTime()
        logger.debug("CurrentTime: %s" % today)
        ps_student = myDB.match("SELECT psid from ps_students WHERE studentid=?", (StudentID,))
        # logger.debug(student['PSID'])
        if not ps_student or len(ps_student) == 0:
            return []
        schedulelist = myDB.select("SELECT classname, period, term, room, firstday, lastday, lastname, classes.classid FROM schedules, classes,people WHERE schedules.classid=classes.Classid AND people.identifier=classes.staffid AND schedules.psid=?", (ps_student['psid'],))
        schedule =[]
        for item in schedulelist:
            if dbDateFormat(item['firstday']) <= today <= dbDateFormat(item['lastday']):
                entry = {
                    'classname': item['classname'],
                    'period': item['period'],
                    'term': item['term'],
                    'room': item['room'],
                    'lastname': item['lastname'],
                    'classid': item['classid']
                }
                schedule.append(entry)
        return schedule
    else:
        return []

def getStaffSchedule(StaffID=None):
    if StaffID:
        myDB = database.DBConnection()
        today = currentTime()
        logger.debug("CurrentTime: %s" % today)
        schedulelist = myDB.select("SELECT classname, period, term, room, firstday, lastday, classid FROM classes WHERE classes.staffid=?", (StaffID,))
        schedule =[]
        for item in schedulelist:
            if dbDateFormat(item['firstday']) <= today <= dbDateFormat(item['lastday']):
                entry = {
                    'classname': item['classname'],
                    'period': item['period'],
                    'term': item['term'],
                    'room': item['room'],
                    'classid': item['classid']
                }
                schedule.append(entry)
        return schedule
    else:
        return []

def getDevices(PersonID=None):
    myDB = database.DBConnection()
    if PersonID:
        deviceList = myDB.select("SELECT deviceid, ownerid, tag, modelname, serialnumber, mac, adname, purchased, notes, modeltype, locationid FROM devices, device_models WHERE devices.modelid = device_models.modelid AND devices.ownerid = ?", (PersonID,))
    else:
        deviceList = myDB.select("SELECT deviceid, ownerid, tag, modelname, serialnumber, mac, adname, purchased, notes, modeltype, locationid FROM devices, device_models WHERE devices.modelid = device_models.modelid")
    returnlist = []
    # for device in deviceList:
    #     dev = {
    #         'deviceid': device['deviceid'],
    #         'ownerid': device['ownerid'],
    #         'tag': device['tag'],
    #         'modelname': device['modelname'],
    #         'serialnumber': device['serialnumber'],
    #         'mac': device['mac'],
    #         'adname': device['adname'],
    #         'purchased': device['purchased'],
    #         'notes': device['notes'],
    #         'modeltype': device['modeltype'],
    #     }
    #     returnlist.append(dev)
    if len(deviceList) > 0:
        return deviceList
    else:
        return []

def getDeviceCache(unFiltered=0, deviceid=None):
    myDB = database.DBConnection()
    if deviceid:
        device = myDB.match("SELECT * from device_cache WHERE deviceid=?", (deviceid,))
        return device
    else:
        if int(unFiltered):
            deviceList = myDB.select(
                "SELECT deviceid, tag, ownertype, ownername, modeltype, modelname, serialnumber, adname, mac, purchased, notes, schoolabbrev, locationname, retired from device_cache")
        else:
            deviceList = myDB.select("SELECT deviceid, tag, ownertype, ownername, modeltype, modelname, serialnumber, adname, mac, purchased, notes, schoolabbrev, locationname, retired from device_cache WHERE retired IS NULL OR retired=0")
        if len(deviceList) > 0:
            return deviceList
        else:
            return []

def getDeviceLoc(device = None):
    if device:
        loc = {'locationname': 'Unknown', 'schoolabbrev': 'UNKNOWN', 'locationid': None, 'schoolid': None}
        myDB = database.DBConnection()
        if device['locationid']:
            location = myDB.match("SELECT locationid, locationname, schoolid FROM locations WHERE locationid = ?", (device['locationid'],))
            if location:
                school = myDB.match("SELECT schoolid, schoolname, schoolabbrev FROM schools WHERE schoolid = ?", (location['schoolid'],))
                if school:
                    loc['schoolid'] = school['schoolid']
                    loc['schoolabbrev'] = school['schoolabbrev']
                loc['locationid'] = location['locationid']
                loc['locationname'] = location['locationname']
            else:
                owner = getPerson(device['ownerid'])
                if owner:
                    loc['locationname'] = 'With User'
                    school = myDB.match("select schoolid, schoolabbrev FROM schools WHERE schoolid = ?", (owner['schoolid'],))
                    if school:
                        loc['schoolid'] = school['schoolid']
                        loc['schoolabbrev'] = school['schoolabbrev']
        return loc

def updateDeviceCache(deviceid = None):
    myDB = database.DBConnection()
    if not deviceid:
        myDB.action('DELETE from device_cache')
        devicelist = myDB.select(
            "SELECT deviceid, ownerid, tag, modelname, serialnumber, mac, adname, purchased, notes, modeltype, locationid, retired FROM devices, device_models WHERE devices.modelid = device_models.modelid")
    else:
        devicelist = myDB.select(
            "SELECT deviceid, ownerid, tag, modelname, serialnumber, mac, adname, purchased, notes, modeltype, locationid, retired FROM devices, device_models WHERE devices.modelid = device_models.modelid AND deviceid = ?", (deviceid,))
    logger.debug('Caching %s device(s)' % len(devicelist))
    for device in devicelist:
        owner = getPerson(device['ownerid'])
        schoolabbrev = "UNKNOWN"
        locationname = "Unknown"
        if owner:
            ownertype = owner['type']
            username = owner['username']
            school = myDB.match("SELECT schoolabbrev FROM schools WHERE schoolid = ?", (owner['schoolid'],))
            if school:
                schoolabbrev = school['schoolabbrev']
            else:
                schoolabbrev = None
            if device['locationid']:
                location = myDB.match("SELECT locationname, schoolid FROM locations WHERE locationid = ?", (device['locationid'],))
                if location:
                    locationname = location['locationname']
                    school = myDB.match("SELECT schoolabbrev FROM schools WHERE schoolid = ?", (location['schoolid'],))
                    if school:
                        schoolabbrev = school['schoolabbrev']
                else:
                    locationname = 'With User'
            else:
                locationname = 'With User'
        else:
            ownertype = 'NONE'
            username = ''
            if device['locationid']:
                location = myDB.match("SELECT locationname, schoolid FROM locations WHERE locationid = ?", (device['locationid'],))
                if location:
                    locationname = location['locationname']
                    school = myDB.match("SELECT schoolabbrev FROM schools WHERE schoolid = ?", (location['schoolid'],))
                    if school:
                        schoolabbrev = school['schoolabbrev']

        controlEntry = {
            'deviceid': device['deviceid']
        }
        lineEntry = {
            'tag': device['tag'],
            'ownertype': ownertype,
            'ownername': username,
            'modeltype': device['modeltype'],
            'modelname': device['modelname'],
            'serialnumber': device['serialnumber'],
            'adname': device['adname'],
            'mac': device['mac'],
            'purchased': device['purchased'],
            'notes': device['notes'],
            'schoolabbrev': schoolabbrev,
            'locationname': locationname,
            'retired': device['retired']
        }
        myDB.upsert('device_cache', lineEntry, controlEntry)

def removeDevice(personid=None, deviceid=None, locationid=None):
    if not personid or not deviceid:
        return False
    myDB = database.DBConnection()
    deviceList = myDB.select("SELECT deviceid, ownerid, tag, modelname, serialnumber, mac, adname, purchased, notes, modeltype, locationid FROM devices, device_models WHERE devices.modelid = device_models.modelid AND devices.ownerid = ?", (personid,))
    if deviceList and len(deviceList) > 0:
        myDB.upsert('devices', {'ownerid': None, 'locationid': locationid}, {'deviceid': deviceid})
        logger.debug("Returning true")
        return True
    else:
        logger.debug("Returning false")
        return False

def getPerson(PersonID=None):
    myDB = database.DBConnection()
    if PersonID:
        person = myDB.match("SELECT * from people WHERE id=?", (PersonID,))
        if len(person) > 0:
            persondict = {}
            for key in person.keys():
                persondict[key] = person[key]
            if person['id'][:3] == 'STA':
                persondict['type'] = 'STAFF'
            elif person['id'][:3] == 'STU':
                persondict['type'] = 'STUDENT'
            else:
                persondict['type'] = 'UNKNOWN'
            return persondict

def getPersonByUserName(username=None):
    myDB = database.DBConnection()
    logger.debug(username)
    if username:
        person = myDB.match("SELECT * from people WHERE username=?", (username,))

        if len(person) > 0:
            persondict = {}
            for key in person.keys():
                persondict[key] = person[key]
            if person['id'][:3] == 'STA':
                persondict['type'] = 'STAFF'
            elif person['id'][:3] == 'STU':
                persondict['type'] = 'STUDENT'
            else:
                persondict['type'] = 'UNKNOWN'
            return persondict
    else:
        return None

def getDevice(deviceid=None):
    myDB = database.DBConnection()
    if deviceid:
        device = myDB.match("SELECT * from devices WHERE deviceid=?", (deviceid,))
        return device

def search(searchstring=None, type=None):
    if searchstring is None or not searchstring:
        return False

    myDB = database.DBConnection()
    resultlist = []
    if not type or type == "Person" or type == "PersonNoParent":
        peoplesearch = myDB.select("SELECT id, identifier, lastname, firstname, username, email from people WHERE ignore=0")
        for item in peoplesearch:
            if (searchstring.lower() in str(item['identifier'])) or (searchstring.lower() in item['lastname'].lower()) or (
                    searchstring.lower() in item['firstname'].lower()) or (searchstring.lower() in item['username'].lower()) or (
                    searchstring.lower() in item['email'].lower()):
                if item['id'][:3] == 'STA':
                    if type == "PersonNoParent":
                        parentsearch = myDB.match("SELECT * FROM accountlink WHERE childid=?", (item['id'],))
                        if not parentsearch:
                            resultlist.append({'ID': item['id'], 'Type': 'Staff',
                                               'Name': '%s, %s' % (item['lastname'], item['firstname']),
                                               'URL': 'staffPage?UserName=%s' % item['username'], 'Details': ''})
                    else:
                        resultlist.append({'ID': item['id'], 'Type': 'Staff', 'Name': '%s, %s' % (item['lastname'], item['firstname']),
                                       'URL': 'staffPage?UserName=%s' % item['username'], 'Details': ''})
                elif item['id'][:3] == 'STU':
                    resultlist.append({'ID': item['id'], 'Type': 'Student', 'Name': '%s, %s' % (item['lastname'], item['firstname']),
                                       'URL': 'studentPage?UserName=%s' % item['username'], 'Details': ''})

    if not type or type == "Devices":
        devicesearch = myDB.select("SELECT deviceid, serialnumber, tag, mac, adname, modelname, ownerid, retired from devices, device_models WHERE devices.modelid=device_models.modelid")
        for item in devicesearch:
            if (item['serialnumber'] and searchstring.lower() in item['serialnumber'].lower()) or (
                    item['tag'] and searchstring.lower() in item['tag'].lower()) or (
                    item['mac'] and searchstring.lower() in item['mac'].lower()) or (
                    item['adname'] and searchstring.lower() in item['adname'].lower()):
                ownername = None
                if item['ownerid'] and len(item['ownerid']) > 0:
                    owner = myDB.match("SELECT * from people where id = ?", (item['ownerid'],))
                    if owner and len(owner) > 0:
                        ownername = getName(id=owner['id'])
                if item['retired'] == 1:
                    pass
                else:
                    resultlist.append(
                        {'ID': item['deviceid'], 'Type': 'Device', 'Name': item['tag'], 'URL': 'devicePage?deviceid=%s' % item['deviceid'], 'Details': {'model': item['modelname'], 'tag': item['tag'], 'owner': ownername}})


    if len(resultlist) > 0:
        return resultlist
    else:
        return False

def findNewStudents():
    myDB = database.DBConnection()
    studentlist = myDB.select("SELECT username, people.firstname, people.lastname, gradelevel, ps_students.studentid from people, ps_students, schools WHERE people.identifier = ps_students.studentid AND people.id LIKE ? AND ps_students.schoolid = schools.schoolid AND ps_students.gradelevel > -1", ('STU%',))
    adquery = myDB.select("SELECT username from ad_students")
    adlist = []
    for ad in adquery:
        adlist.append(ad['username'])
    newstudentlist = []
    for student in studentlist:
        if student['username'] not in adlist:
            newstudentlist.append(student)
    return newstudentlist

def findNewStaff():
    myDB = database.DBConnection()
    stafflist = myDB.select("SELECT username, people.firstname, people.lastname, title, ps_staff.psid from people, ps_staff  WHERE people.identifier = ps_staff.psid AND people.id LIKE ? AND ps_staff.status = 1 AND people.ignore = 0", ('STA%',))
    adquery = myDB.select("SELECT username from ad_staff")
    adlist = []
    for ad in adquery:
        adlist.append(ad['username'])
    newstafflist = []
    for staff in stafflist:
        if staff['username'] not in adlist:
            newstafflist.append(staff)
    return newstafflist

def getPivotData():
    myDB = database.DBConnection()
    outlist = myDB.select('SELECT ps_students.stn, ps_students.lastname, ps_students.firstname, ps_students.gradelevel, \
    ps_students.dob, classes.classname, ps_students.studentemail, ps_students.psusername, ps_staff.lastname AS stafflast, \
    ps_staff.firstname AS stafffirst, ps_staff.email, ps_staff.psid, ps_staff.spn, classes.classid, classes.statecourseid, \
    ps_students.gender, ps_students.ethnicitycode, ps_students.schoolid, schools.schoolabbrev FROM classes, schedules, ps_students, schools, \
    ps_staff WHERE classes.staffid = ps_staff.psid AND schedules.psid = ps_students.psid AND \
    schedules.schoolid = schools.schoolid AND schedules.classid = classes.classid')
    # logger.debug(len(outlist))
    outfile = os.path.join(schoolkeeper.CACHEDIR, 'pivot_export.csv')
    if os.path.exists(outfile):
        os.remove(outfile)
    with open(outfile, 'wb') as csvfile:
        csvwriter = csv.writer(csvfile, delimiter=',',quotechar='"', quoting=csv.QUOTE_MINIMAL)
        csvwriter.writerow([
            'School Year',
            'STN',
            'Student Last Name',
            'Student First Name',
            'Student Email Address',
            'Student User Name',
            'Grade Level',
            'Birth Date',
            'Course ID',
            'Section ID',
            'Course Name',
            'Teacher Last Name',
            'Teaher First Name',
            'SPN',
            'Teacher Email Address',
            'Ethnicity/Race',
            'Socio Economic Status',
            'Gender',
            'Special Education Participant',
            'ELL',
            'Grad Year',
            'Days Absent',
            'Behavior Count',
            'Behavior Grade',
            'Course Grade',
            'Building ID',
            'Building Name',
            'Guardian Full Name',
            'Guardian Phone',
            'High Ability Participation',
            'School Tardies',
            'GPA',
            'Credits Earned'
        ])
        for line in outlist:
            if line['gradelevel'] == 0:
                gradelevel = 'K'
            elif line['gradelevel'] < 0:
                gradelevel = 'PK'
            else:
                gradelevel = line['gradelevel']
            gradyear = (12 - line['gradelevel']) + schoolkeeper.CONFIG['CURRENT_SCHOOL_YEAR']
            if not line['spn'] or len(str(line['spn'])) < 1:
                spn = line['psid']
            else:
                spn = line['spn']
            # logger.debug(line['stn'])
            csvwriter.writerow([
                schoolkeeper.CONFIG['CURRENT_SCHOOL_YEAR'] - 1,
                line['stn'],
                line['lastname'],
                line['firstname'],
                line['studentemail'],
                line['psusername'],
                gradelevel,
                line['dob'],
                line['statecourseid'],
                line['classid'],
                line['classname'],
                line['stafflast'],
                line['stafffirst'],
                spn,
                line['email'],
                line['ethnicitycode'],
                '',
                line['gender'],
                '',
                '',
                gradyear,
                '',
                '',
                '',
                '',
                line['schoolid'],
                line['schoolabbrev'],
                '',
                '',
                '',
                '',
                '',
                '']
            )
    msg = "Data Uploaded"
    pc = pivot.pivotConnection()
    pc.upload(outfile)
    return msg

def getPowerSchoolChanges():
    myDB = database.DBConnection()
    templist = myDB.select('SELECT ps_students.studentid, people.username, people.email, ps_students.psusername, ps_students.studentemail from ps_students, people WHERE people.identifier = ps_students.studentid AND people.id LIKE ?', ('STU%',))
    logger.debug(len(templist))
    outfile = os.path.join(schoolkeeper.CACHEDIR, 'powerschool_export.csv')
    if os.path.exists(outfile):
        os.remove(outfile)
    with open(outfile, 'wb') as csvfile:
        csvwriter = csv.writer(csvfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        csvwriter.writerow(['Student_Number','Student_Web_ID', 'Student_Email', 'Student_AllowWebAccess', 'LDAPEnabled'])
        for line in templist:
            if any([not line['psusername'], not line['studentemail'], line['psusername'] != line['username'], line['studentemail'] != line['email']]):
                csvwriter.writerow([
                    line['studentid'],
                    line['username'],
                    line['email'],
                    1,
                    1]
                )
    return

def setStudentDesctiptions():
    myDB = database.DBConnection()
    studentlist = myDB.select("SELECT people.id, schools.schoolabbrev, ps_students.gradelevel, ad_students.distinguishedname FROM people, schools, ps_students WHERE people.identifier = ps_students.studentid AND ps_students.schoolid = schools.schoolid and people.id LIKE ? AND people.username = ad_students.username", ('STU%',))
    logger.debug(studentlist)
    for student in studentlist:
        ad_command = {}
        ad_cn = student['distinguishedname']
        grad_year = schoolkeeper.CONFIG['CURRENT_SCHOOL_YEAR'] + 12 - student['gradelevel']
        ad_command['description'] = "%s %s" % (student['schoolabbrev'], grad_year)
        logger.debug(ad_command)
        # adresp = myLDAP.connection.add(ad_cn, 'User', ad_command)
        # logger.debug(myLDAP.connection.result)
        # if adresp:
        #     admessage = "User created.<br>"
        #     dblogger.user("AD user account created for %s" % dbactions.getName(id=personid), _session['user'],
        #                   _session['user'])
        #     dblogger.person("AD user account created", _session['user'], personid)
        #     pwbool, pwresult = myLDAP.setPassword(username=person['username'], password=temppass, unlock=True)
        #     admessage += pwresult
        # else:
        #     admessage = "User not created.<br>"

def getParentID(personid=None):
    if personid:
        myDB = database.DBConnection()
        currentParentID = personid
        matching = 1
        while matching:
            parent = myDB.match('SELECT parentid from accountlink WHERE childid = ?', (currentParentID,))
            if parent:
                logger.debug("Found a parent: %s" % parent['parentid'])
                currentParentID = parent['parentid']
            else:
                matching = 0
        return currentParentID
    else:
        return

def moveDevicesToParent(personid=None):
    myDB = database.DBConnection()
    if personid:
        peoplelist = [personid]
    else:
        peoplelist = []
        accounts = myDB.select('SELECT * from accountlink')
        for account in accounts:
            peoplelist.append(account['childid'])
    for person in peoplelist:
        parentID = getParentID(person)
        devices = myDB.select('SELECT * FROM devices WHERE ownerid=?', (person,))
        for device in devices:
            myDB.upsert('devices',{'ownerid': parentID}, {'deviceid': device['deviceid']})
            updateDeviceCache(device['deviceid'])
            dblogger.device('Device moved to parent user account', 'system', device['deviceid'])

def getAttachments(search=None):
    attachments = []
    myDB = database.DBConnection()
    allattachments = myDB.select("SELECT * from attachments")
    for entry in allattachments:
        devices = simplejson.loads(entry['devices'])
        people = simplejson.loads(entry['people'])
        # logger.debug(devices)
        if str(search) in devices or search in people:
            # logger.debug('Found')
            attachments.append(entry)
    return attachments

def getGoogleChromeDevices():
    gamcsv = gam.get("print cros basic formatjson")
    entries = csv.reader(gamcsv.splitlines())
    devices = {}
    for entry in list(entries):
        try:
            devices[entry[0]] = simplejson.loads(entry[1])
        except:
            pass
    logger.debug("Gathered %s devices from Google" % len(devices))
    return devices

def getGoogleTeacherCourses(email):
    gamcsv = gam.get("print courses teacher %s formatjson" % email)
    entries = csv.reader(gamcsv.splitlines())
    teachercourses = {}
    for entry in list(entries):
        try:
            teachercourses[entry[0]] = simplejson.loads(entry[1])
        except:
            pass
    return teachercourses

def updateGoogleDeviceID():
    myDB = database.DBConnection()
    deviceList = myDB.select("SELECT deviceid from device_cache WHERE UPPER(modeltype) like ? AND (retired is NULL OR retired = 0)", ('%CHROME%',))
    logger.debug("Devices to check: %s" % len(deviceList))
    matchedcount = 0
    updatecount = 0
    failedcount = 0
    skippedcount = 0
    googleDevices = getGoogleChromeDevices()
    for listitem in deviceList:
        device = myDB.match("SELECT * FROM devices WHERE deviceid = ?", (listitem['deviceid'],))
        if device:
            for chromedevice in googleDevices.keys():
                try:
                    if googleDevices[chromedevice]['annotatedAssetId'] == device['tag']:
                        matchedcount += 1
                        if googleDevices[chromedevice]['serialNumber'] == device['serialnumber'] and googleDevices[chromedevice]['deviceId'] == device['gdeviceid']:
                            skippedcount += 1
                        else:
                            myDB.upsert('devices', {'gdeviceid': googleDevices[chromedevice]['deviceId'], 'serialnumber': googleDevices[chromedevice]['serialNumber']}, {'deviceid': device['deviceid']})
                            updatecount += 1
                except:
                            failedcount += 1
                            del googleDevices[chromedevice]
    logger.debug('Items Matched: %s' % matchedcount)
    logger.debug('Items Updated: %s' % updatecount)
    logger.debug('Items Skipped: %s' % skippedcount)
    logger.debug('Items Failed: %s' % failedcount)


def getActiveStudents(schoolid=None):
    myDB = database.DBConnection()
    args = []
    cmd = "SELECT people.id, people.lastname, studentid, people.email, dob, ps_students.gradelevel, people.firstname, firstday, lastday, gender \
            FROM classes, ps_students, schedules, people WHERE people.identifier = ps_students.studentid AND schedules.classid=classes.Classid \
            AND schedules.psid = ps_students.psid"
    if schoolid:
        cmd += " AND ps_students.schoolid=?"
        args.append(schoolid)
    mainlist = myDB.select(myDB.dbformat(cmd), tuple(args))
    studentlist = []
    countlist = []
    today = currentTime()
    for item in mainlist:
        if dbDateFormat(item['firstday']) <= today <= dbDateFormat(item['lastday']) and item['studentid'] not in countlist:
            countlist.append(item['studentid'])
            tempitem = {}
            for key in item.keys():
                tempitem[key] = item[key]
            studentlist.append(tempitem)
    logger.debug('Returning %s active students' % len(studentlist))
    return studentlist


def getClassStudents(ClassID):
    myDB = database.DBConnection()
    studentlist = myDB.select("SELECT people.lastname, people.firstname, people.id, ps_students.studentid FROM people, ps_students, schedules WHERE ps_students.psid = schedules.psid AND ps_students.studentid = people.identifier AND people.id LIKE ? and schedules.classid = ?", ('STU%', ClassID))
    return sorted(studentlist,key=lambda k: (k['lastname'], k['firstname']))

