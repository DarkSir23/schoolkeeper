import uuid
import schoolkeeper
import os
import lib.simplejson as simplejson
from schoolkeeper import database, common

class attachment:
    def __init__(self, guid=None):
        self.fileloc = schoolkeeper.CONFIG['FOLDER_ATTACHMENTS']
        if len(self.fileloc) > 2:
            if not os.path.exists(self.fileloc):
                os.mkdir(self.fileloc)

        self.guid = guid
        self.hasfile = False
        self.filename = None
        self.description = None
        self.timestamp = None
        self.people = []
        self.devices = []
        if self.guid:
            temp_attach = myDB.match('SELECT * from attachments WHERE attachmentid=?', (self.guid,))
            if temp_attach:
                self.filename = temp_attach['filename']
                self.description = temp_attach['description']
                self.timestamp = temp_attach['timestamp']
                self.people = simplejson.loads(temp_attach['people'])
                self.devices = simplejson.loads(temp_attach['devices'])
                fullfilename = os.path.join(self.fileloc, self.filename)
                if os.exists(fullfilename):
                    self.hasfile = True

    def upload(self, uploadfile=None, description=None, devices=[], people=[], replace=False):
        myDB = database.DBConnection()
        msg = ''
        if self.guid and self.hasfile and not replace:
            msg = "Attachment already has a file."
            return False, msg
        if not self.guid:
            self.guid = str(uuid.uuid4())
        filename = uploadfile.filename
        filebase, extension = os.path.splitext(filename)
        newfilename = os.path.join(self.fileloc, self.guid + extension)
        if os.path.exists(newfilename):
            if replace:
                logger.debug('[ATTACHMENT] Deleting existing attachment')
                os.remove(newfilename)
            else:
                msg = "Could not replace existing file."
                return False, msg
        result = ''
        while True:
            data = uploadfile.file.read(8192)
            if not data:
                break
            result += data
        try:
            with open(newfilename, "w") as outfile:
                outfile.write(result)
            msg += 'Attachment Uploaded'
            myDB.upsert('attachments',valueDict={'description': description, 'devices': simplejson.dumps(devices), 'people': simplejson.dumps(people), 'timestamp': common.currentTime(), 'filename': os.path.basename(newfilename)}, keyDict={'attachmentid': self.guid})
            return True, msg
        except Exception as e:
            msg = 'Attachment NOT uploaded (%s)' % e
            return True, msg
        return False, msg
