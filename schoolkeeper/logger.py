import logging
import os
import threading

from logging import handlers


import schoolkeeper
from schoolkeeper import formatter



class RotatingLogger(object):
    def __init__(self, filename):
        self.filename = filename
        self.filehandler = None
        self.consolehandler = None

    def stopLogger(self):
        l = logging.getLogger('schoolkeeper')
        l.removeHandler(self.filehandler)
        l.removeHandler(self.consolehandler)

    def initLogger(self, loglevel=1):

        l = logging.getLogger('schoolkeeper')
        l.setLevel(logging.DEBUG)

        self.filename = os.path.join(schoolkeeper.CONFIG['LOGDIR'], self.filename)

        filehandler = handlers.RotatingFileHandler(
            self.filename,
            maxBytes=schoolkeeper.CONFIG['LOGSIZE'],
            backupCount=schoolkeeper.CONFIG['LOGFILES'])

        filehandler.setLevel(logging.DEBUG)

        fileformatter = logging.Formatter('%(asctime)s - %(levelname)-7s :: %(message)s', '%d-%b-%Y %H:%M:%S')

        filehandler.setFormatter(fileformatter)
        l.addHandler(filehandler)

        if loglevel:
            consolehandler = logging.StreamHandler()
            if loglevel == 1:
                consolehandler.setLevel(logging.INFO)
            if loglevel == 2:
                consolehandler.setLevel(logging.DEBUG)
            consoleformatter = logging.Formatter('%(asctime)s - %(levelname)s :: %(message)s', '%d-%b-%Y %H:%M:%S')
            consolehandler.setFormatter(consoleformatter)
            l.addHandler(consolehandler)
            self.consolehandler = consolehandler

    @staticmethod
    def log(message, level):

        logger = logging.getLogger('schoolkeeper')

        threadname = threading.currentThread().getName()

        # Ensure messages are correctly encoded
        message = formatter.safe_unicode(message).encode(schoolkeeper.SYS_ENCODING)

        if level != 'DEBUG' or schoolkeeper.LOGLEVEL >= 2:
            # Limit the size of "in-memory" log.
            schoolkeeper.LOGLIST.insert(0, (formatter.now(), level, message))
            if len(schoolkeeper.LOGLIST) > schoolkeeper.CONFIG['LOGLIMIT']:
                del schoolkeeper.LOGLIST[-1]

        message = "%s : %s" % (threadname, message)

        if level == 'DEBUG':
            logger.debug(message)
        elif level == 'INFO':
            logger.info(message)
        elif level == 'WARNING':
            logger.warn(message)
        else:
            logger.error(message)

schoolkeeper_log = RotatingLogger('schoolkeeper.log')


def debug(message):
    schoolkeeper_log.log(message, level='DEBUG')

def info(message):
    schoolkeeper_log.log(message, level='INFO')

def warn(message):
    schoolkeeper_log.log(message, level='WARNING')

def error(message):
    schoolkeeper_log.log(message, level='ERROR')

