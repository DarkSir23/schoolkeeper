import schoolkeeper
from schoolkeeper import logger

def PS_Student_Update():
    response = schoolkeeper.dbactions.updatePSStudents()
    logger.info('[SCHEDULER] Update PS Students: %s' % response)

def PS_Staff_Update():
    response = schoolkeeper.dbactions.updatePSStaff()
    logger.info('[SCHEDULER] Update PS Staff: %s' % response)

def AD_Student_Update():
    response = schoolkeeper.dbactions.updateADStudents()
    logger.info('[SCHEDULER] Update AD Students: %s' % response)

def AD_Staff_Update():
    response = schoolkeeper.dbactions.updateADStaff()
    logger.info('[SCHEDULER] Update AD Staff: %s' % response)

def Classes_Update():
    response = schoolkeeper.dbactions.updateClasses()
    logger.info('[SCHEDULER] Update Classes: %s' % response)

def Schedule_Update():
    response = schoolkeeper.dbactions.updateSchedules()
    logger.info('[SCHEDULER] Update Schedules: %s' % response)


def PIVOT_Export():
    response = schoolkeeper.dbactions.getPivotData()
    logger.info('[SCHEDULER] Pivot Export: %s' % response)

def TRANSFINDER_Convert():
    response = schoolkeeper.transfinder.convertTransfinderData()
    logger.info('[SCHEDULER] Transfinder Convert: %s' % response)

def POWERSCHOOL_Update():
    response = schoolkeeper.powerschool.updatePowerSchoolEntries()
    logger.info('[SCHEDULER] Powerschool Update Entries: %s' % response)

def EFunds_Update():
    funds = schoolkeeper.efunds.efundsConnection()
    outfile = funds.balanceConversion(schoolkeeper.CONFIG['EFUNDS_IMPORT_FILE'],
                                      schoolkeeper.CONFIG['EFUNDS_EXPORT_FILE'])
    if outfile:
        response = funds.upload(outfile)
    else:
        response = "Unable to export file."
    logger.info('[SCHEDULER] EFunds TBR Update: %s' % response)