import logging
import os
import threading
import time
from datetime import datetime

from logging import handlers



import schoolkeeper
from schoolkeeper import formatter, database


class DBLogger(object):
    def __init__(self):
        placeholder = None

    @staticmethod
    def dblog(message, level, userName='', id=None):
        myDB = database.DBConnection()
        message = formatter.safe_unicode(message).encode(schoolkeeper.SYS_ENCODING)
        enactor = None
        logdb = None
        if level == 'USER':
            enactor = 'userid'
            logdb = 'useractivity'
        elif level == 'PERSON':
            enactor = 'personid'
            logdb = 'personactivity'
        elif level == 'DEVICE':
            enactor = 'deviceid'
            logdb = 'deviceactivity'
        if enactor and logdb:
            timestamp = datetime.now().strftime('%Y-%m-%dT%H:%M')
            activitydate = datetime.now().strftime('%Y-%m-%dT%H:%M')
            myDB.upsert(logdb, {'activitydate': activitydate, 'username': userName}, {'timestamp': timestamp, enactor: id, 'activitydesc': message})


schoolkeeper_dblog = DBLogger()


def user(message, userName='', id=None):
    schoolkeeper_dblog.dblog(message, level='USER', userName=userName, id=id)


def person(message, userName='', id=None):
    schoolkeeper_dblog.dblog(message, level='PERSON', userName=userName, id=id)


def device(message, userName='', id=None):
    schoolkeeper_dblog.dblog(message, level='DEVICE', userName=userName, id=id)
