import datetime
import os
import sys
import datetime
import calendar
import platform
import string
import shutil
import threading
import time
import traceback
import numbers
import shlex
from subprocess import Popen, PIPE

# import lib.zipfile as zipfile

import schoolkeeper
from schoolkeeper import logger, database, schedule
from schoolkeeper.formatter import next_run

USER_AGENT = 'SchoolKeeper' + ' (' + platform.system() + ' ' + platform.release() + ')'
# Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36

def getUserAgent():
    # Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36
    if schoolkeeper.CONFIG['USER_AGENT']:
        return schoolkeeper.CONFIG['USER_AGENT']
    else:
        return 'SchoolKeeper' + ' (' + platform.system() + ' ' + platform.release() + ')'


# Notification Types
NOTIFY_SNATCH = 1
NOTIFY_DOWNLOAD = 2

notifyStrings = {NOTIFY_SNATCH: "Started Download", NOTIFY_DOWNLOAD: "Added to Library"}


def error_page_401(status, message, traceback, version):
    """ Custom handler for 401 error """
    title = 'Access denied'
    body = 'Error %s: You need to provide a valid username and password.' % status
    return r'''
<html>
    <head>
    <h1>SchoolKeeper</h1>
    <title>%s</title>
    </head>
    <body>
    <br/><br/>
    <font color="#0000FF">%s</font>
    </body>
</html>
''' % (title, body)




def scheduleJob(action='Start', target=None):
    """ Start or stop or restart a cron job by name eg
        target=search_magazines, target=processDir, target=search_book """
    if target is None:
        return

    if action == 'Stop' or action == 'Restart':
        for job in schoolkeeper.SCHED.get_jobs():
            if target in str(job):
                schoolkeeper.SCHED.unschedule_job(job)
                logger.debug("Stop %s job" % target)

    if action == 'Start' or action == 'Restart':
        for job in schoolkeeper.SCHED.get_jobs():
            if target in str(job):
                logger.debug("%s %s job, already scheduled" % (action, target))
                return  # return if already running, if not, start a new one
        if 'checkForUpdates' in target and int(schoolkeeper.CONFIG['VERSIONCHECK_INTERVAL']):
            schoolkeeper.SCHED.add_interval_job(
                schoolkeeper.versioncheck.checkForUpdates,
                hours=int(schoolkeeper.CONFIG['VERSIONCHECK_INTERVAL']))
            logger.debug("%s %s job in %s hours" % (action, target, schoolkeeper.CONFIG['VERSIONCHECK_INTERVAL']))
        if 'PS_Student_Update' in target:
            schoolkeeper.SCHED.add_interval_job(
                schoolkeeper.schedule.PS_Student_Update,
                minutes=int(schoolkeeper.CONFIG['PS_TIMER']))
        if 'PS_Staff_Update' in target:
            schoolkeeper.SCHED.add_interval_job(
                schoolkeeper.schedule.PS_Staff_Update,
                minutes=int(schoolkeeper.CONFIG['PS_TIMER']))
        if 'AD_Student_Update' in target:
            schoolkeeper.SCHED.add_interval_job(
                schoolkeeper.schedule.AD_Student_Update,
                minutes=int(schoolkeeper.CONFIG['AD_TIMER']))
        if 'AD_Staff_Update' in target:
            schoolkeeper.SCHED.add_interval_job(
                schoolkeeper.schedule.AD_Staff_Update,
                minutes=int(schoolkeeper.CONFIG['AD_TIMER']))
        if 'Classes_Update' in target:
            schoolkeeper.SCHED.add_cron_job(schoolkeeper.schedule.Classes_Update, hour='01', minute='00', second='00')
        if 'Schedule_Update' in target:
            schoolkeeper.SCHED.add_cron_job(schoolkeeper.schedule.Schedule_Update, hour='01', minute='05', second='00')
        if 'PIVOT_Export' in target:
            schoolkeeper.SCHED.add_cron_job(schoolkeeper.schedule.PIVOT_Export,
                                            hour='12', minute='45', second='00')
        if 'TRANSFINDER_Convert' in target:
            schoolkeeper.SCHED.add_cron_job(schoolkeeper.schedule.TRANSFINDER_Convert, hour='01', minute='30', second='00')
        if 'POWERSCHOOL_Update' in target:
            schoolkeeper.SCHED.add_cron_job(schoolkeeper.schedule.POWERSCHOOL_Update, hour='00', minute='30', second='00')
        if 'EFunds_Update' in target:
            schoolkeeper.SCHED.add_cron_job(schoolkeeper.schedule.EFunds_Update, hour='03', minute='00', second='00')



def datetimeToTimestamp(dt):
    convtime = calendar.timegm(datetime.datetime.strptime(dt, "%Y-%m-%dT%H:%M").utctimetuple())
    return convtime


def dateToTimestamp(dt):
    convtime = calendar.timegm(datetime.datetime.strptime(dt, "%Y-%m-%d").utctimetuple())
    return convtime


def dbDateFormat(dt):
    convtime = datetime.datetime.strptime(dt, "%m/%d/%Y").strftime('%Y-%m-%dT%H:%M')
    return convtime


def timestampToDateTime(ts):
    convtime = datetime.datetime.fromtimestamp(int(ts)).strftime('%Y-%m-%dT%H:%M')
    return convtime


def currentTime():
    convtime = datetime.datetime.now().strftime('%Y-%m-%dT%H:%M')
    return convtime


def checkToBool(check):
    if check == 'on' or check is True:
        return True
    else:
        return False


def checkToInt(check):
    if check == 'on' or check is True:
        return 1
    else:
        return 0


def stringToInt(check):
    check = check.strip()
    return int(check) if check else 0


def intToYesNo(check):
    if check == 1 or check is True:
        return "Yes"
    else:
        return "No"


def uni(input):
    if isinstance(input, numbers.Number):
        return str(input).decode('utf')
    elif isinstance(input, basestring):
        return input
    else:
        return unicode(input, 'utf-8')


def convertMac(macstring):
    maclist = []
    if '-' in macstring:
        maclist = macstring.split('-')
    elif ':' in macstring:
        maclist = macstring.split(':')
    else:
        maclist = [macstring[i:i + 2] for i in range(0, len(macstring), 2)]
    if len(maclist) != 6:
        return False, ""
    else:
        return True, ':'.join(s for s in maclist)


def getTotal(set, key):
    logger.debug(set)
    total = 0
    for dict in set:
        total += dict[key]
    return total


def restartJobs(start='Restart'):
    scheduleJob(start, 'PS_Student_Update')
    scheduleJob(start, 'PS_Staff_Update')
    scheduleJob(start, 'AD_Student_Update')
    scheduleJob(start, 'AD_Staff_Update')
    scheduleJob(start, 'Classes_Update')
    scheduleJob(start, 'Schedule_Update')
    if schoolkeeper.CONFIG['PIVOT_ENABLE']:
        scheduleJob(start, 'PIVOT_Export')
    if schoolkeeper.CONFIG['TRANSFINDER_ENABLE']:
        scheduleJob(start, 'TRANSFINDER_Convert')
    if schoolkeeper.CONFIG['EFUNDS_ENABLE']:
        scheduleJob('EFunds_Update')
    scheduleJob(start, 'POWERSCHOOL_Update')

def ensureRunning(jobname):
    found = False
    for job in schoolkeeper.SCHED.get_jobs():
        if jobname in str(job):
            found = True
            break
    if not found:
        scheduleJob('Start', jobname)


def checkRunningJobs():
    # make sure the relevant jobs are running
    # search jobs start when something gets marked "wanted" but are
    # not aware of any config changes that happen later, ie enable or disable providers,
    # so we check whenever config is saved
    # processdir is started when something gets marked "snatched"
    # and cancels itself once everything is processed so should be ok
    # but check anyway for completeness...

    myDB = database.DBConnection()
    # snatched = myDB.match("SELECT count('Status') as counter from wanted WHERE Status = 'Snatched'")
    # wanted = myDB.match("SELECT count('Status') as counter FROM books WHERE Status = 'Wanted'")
    # if snatched:
    #     ensureRunning('processDir')
    # if wanted:
    #     if lazylibrarian.USE_NZB() or lazylibrarian.USE_TOR() or lazylibrarian.USE_DIRECT():
    #         ensureRunning('search_book')
    #     if lazylibrarian.USE_RSS():
    #         ensureRunning('search_rss_book')
    # else:
    #     scheduleJob('Stop', 'search_book')
    #     scheduleJob('Stop', 'search_rss_book')
    ensureRunning('checkForUpdate')
    ensureRunning('PS_Student_Update')
    ensureRunning('PS_Staff_Update')
    ensureRunning('AD_Student_Update')
    ensureRunning('AD_Staff_Update')
    ensureRunning('Classes_Update')
    ensureRunning('Schedule_Update')
    if schoolkeeper.CONFIG['PIVOT_ENABLE']:
        ensureRunning('PIVOT_Export')
    if schoolkeeper.CONFIG['TRANSFINDER_ENABLE']:
        ensureRunning('TRANSFINDER_Convert')
    if schoolkeeper.CONFIG['EFUNDS_ENABLE']:
        ensureRunning('EFunds_Update')
    ensureRunning('POWERSCHOOL_Update')





def showJobs():
    result = []
    myDB = database.DBConnection()
    for job in schoolkeeper.SCHED.get_jobs():
        job = str(job)
        if "PS_Student_Update" in job:
            jobname = "PowerSchool Student Update"
        elif "PS_Staff_Update" in job:
            jobname = "PowerSchool Staff Update"
        elif "Schedule_Update" in job:
            jobname = "PowerSchool Schedule Update"
        elif "Classes_Update" in job:
            jobname = "PowerSchool Class List Update"
        elif "AD_Student_Update" in job:
            jobname = "Active Directory Student Update"
        elif "AD_Staff_Update" in job:
            jobname = "Active Directory Staff Update"
        elif "EFunds_Update" in job:
            jobname = "EFunds Textbook Balance Upload"
        else:
            jobname = job.split(' ')[0].split('.')[2]

        # jobinterval = job.split('[')[1].split(']')[0]
        # logger.debug('%s - %s' % (jobname, job))
        jobtime = job.split('at: ')[1].split('.')[0]
        if jobtime.endswith(')'):
            jobtime = jobtime[:-1]
        # logger.debug(jobtime)
        jobtime = next_run(jobtime)
        jobinfo = "%s: Next run in %s" % (jobname, jobtime)
        result.append(jobinfo)
    return result


def clearLog():
    logger.schoolkeeper_log.stopLogger()
    error = False
    if os.path.exists(schoolkeeper.CONFIG['LOGDIR']):
        try:
            shutil.rmtree(schoolkeeper.CONFIG['LOGDIR'])
            os.mkdir(schoolkeeper.CONFIG['LOGDIR'])
        except OSError as e:
            error = e.strerror
    logger.schoolkeeper_log.initLogger(loglevel=schoolkeeper.LOGLEVEL)

    if error:
        return 'Failed to clear log: %s' % error
    else:
        schoolkeeper.LOGLIST = []
        return "Log cleared, level set to [%s]- Log Directory is [%s]" % (
            schoolkeeper.LOGLEVEL, schoolkeeper.CONFIG['LOGDIR'])


# def reverse_readline(filename, buf_size=8192):
#     """a generator that returns the lines of a file in reverse order"""
#     with open(filename) as fh:
#         segment = None
#         offset = 0
#         fh.seek(0, os.SEEK_END)
#         file_size = remaining_size = fh.tell()
#         while remaining_size > 0:
#             offset = min(file_size, offset + buf_size)
#             fh.seek(file_size - offset)
#             buf = fh.read(min(remaining_size, buf_size))
#             remaining_size -= buf_size
#             lines = buf.split('\n')
#             # the first line of the buffer is probably not a complete line so
#             # we'll save it and append it to the last line of the next buffer
#             # we read
#             if segment is not None:
#                 # if the previous chunk starts right from the beginning of line
#                 # do not concact the segment to the last line of new chunk
#                 # instead, yield the segment first
#                 if buf[-1] is not '\n':
#                     lines[-1] += segment
#                 else:
#                     yield segment
#             segment = lines[0]
#             for index in range(len(lines) - 1, 0, -1):
#                 if len(lines[index]):
#                     yield lines[index]
#         # Don't yield None if the file was empty
#         if segment is not None:
#             yield segment


def saveLog():
    if not os.path.exists(schoolkeeper.CONFIG['LOGDIR']):
        return 'LOGDIR does not exist'

    popen_list = [sys.executable, schoolkeeper.FULL_PATH]
    popen_list += schoolkeeper.ARGS
    header = "Startup cmd: %s\n" % str(popen_list)
    header += 'Interface: %s\n' % schoolkeeper.CONFIG['HTTP_LOOK']
    header += 'Loglevel: %s\n' % schoolkeeper.LOGLEVEL
    for item in schoolkeeper.CONFIG_GIT:
        header += '%s: %s\n' % (item.lower(), schoolkeeper.CONFIG[item])
    header += "Python version: %s\n" % sys.version.split('\n')
    header += "Distribution: %s\n" % str(platform.dist())
    header += "System: %s\n" % str(platform.system())
    header += "Machine: %s\n" % str(platform.machine())
    header += "Platform: %s\n" % str(platform.platform())
    header += "uname: %s\n" % str(platform.uname())
    header += "version: %s\n" % str(platform.version())
    header += "mac_ver: %s\n" % str(platform.mac_ver())

    basename = os.path.join(schoolkeeper.CONFIG['LOGDIR'], 'schoolkeeper.log')
    outfile = os.path.join(schoolkeeper.CONFIG['LOGDIR'], 'debug')
    passchars = string.ascii_letters + string.digits + '_/'  # _/ used by slack and googlebooks
    redactlist = ['api -> ', 'apikey -> ', 'pass -> ', 'password -> ', 'token -> ', 'using api [',
                  'apikey=', 'key=', 'apikey%3D', "apikey': u'", "apikey': '"]
    with open(outfile + '.tmp', 'w') as out:
        nextfile = True
        extn = 0
        redacts = 0
        while nextfile:
            fname = basename
            if extn > 0:
                fname = fname + '.' + str(extn)
            if not os.path.exists(fname):
                logger.debug("logfile [%s] does not exist" % fname)
                nextfile = False
            else:
                logger.debug('Processing logfile [%s]' % fname)
                linecount = 0
                for line in reverse_readline(fname):
                    for item in redactlist:
                        startpos = line.find(item)
                        if startpos >= 0:
                            startpos += len(item)
                            endpos = startpos
                            while endpos < len(line) and not line[endpos] in passchars:
                                endpos += 1
                            while endpos < len(line) and line[endpos] in passchars:
                                endpos += 1
                            if endpos != startpos:
                                line = line[:startpos] + '<redacted>' + line[endpos:]
                                redacts += 1

                    out.write("%s\n" % line)
                    if "Debug log ON" in line:
                        logger.debug('Found "Debug log ON" line %s in %s' % (linecount, fname))
                        nextfile = False
                        break
                    linecount += 1
                extn += 1

    with open(outfile + '.log', 'w') as logfile:
        logfile.write(header)
        lines = 0  # len(header.split('\n'))
        for line in reverse_readline(outfile + '.tmp'):
            logfile.write("%s\n" % line)
            lines += 1
    os.remove(outfile + '.tmp')
    logger.debug("Redacted %s passwords/apikeys" % redacts)
    logger.debug("%s log lines written to %s" % (lines, outfile + '.log'))
    with zipfile.ZipFile(outfile + '.zip', 'w') as myzip:
        myzip.write(outfile + '.log', 'debug.log')
    os.remove(outfile + '.log')
    return "Debug log saved as %s" % (outfile + '.zip')




# def cleanCache():
#     """ Remove unused files from the cache - delete if expired or unused.
#         Check JSONCache  WorkCache  XMLCache  SeriesCache Author  Book
#         Check covers and authorimages referenced in the database exist and change database entry if missing """
#
#     myDB = database.DBConnection()
#     result = []
#     cache = os.path.join(schoolkeeper.CACHEDIR, "JSONCache")
#     # ensure directory is unicode so we get unicode results from listdir
#     if isinstance(cache, str):
#         cache = cache.decode(lazylibrarian.SYS_ENCODING)
#     cleaned = 0
#     kept = 0
#     if os.path.isdir(cache):
#         for cached_file in os.listdir(cache):
#             target = os.path.join(cache, cached_file)
#             cache_modified_time = os.stat(target).st_mtime
#             time_now = time.time()
#             if cache_modified_time < time_now - (
#                                 lazylibrarian.CONFIG['CACHE_AGE'] * 24 * 60 * 60):  # expire after this many seconds
#                 # Cache is old, delete entry
#                 os.remove(target)
#                 cleaned += 1
#             else:
#                 kept += 1
#     msg = "Cleaned %i file%s from JSONCache, kept %i" % (cleaned, plural(cleaned), kept)
#     result.append(msg)
#     logger.debug(msg)
#
#     cache = os.path.join(lazylibrarian.CACHEDIR, "XMLCache")
#     # ensure directory is unicode so we get unicode results from listdir
#     if isinstance(cache, str):
#         cache = cache.decode(lazylibrarian.SYS_ENCODING)
#     cleaned = 0
#     kept = 0
#     if os.path.isdir(cache):
#         for cached_file in os.listdir(cache):
#             target = os.path.join(cache, cached_file)
#             cache_modified_time = os.stat(target).st_mtime
#             time_now = time.time()
#             if cache_modified_time < time_now - (
#                                 lazylibrarian.CONFIG['CACHE_AGE'] * 24 * 60 * 60):  # expire after this many seconds
#                 # Cache is old, delete entry
#                 os.remove(target)
#                 cleaned += 1
#             else:
#                 kept += 1
#     msg = "Cleaned %i file%s from XMLCache, kept %i" % (cleaned, plural(cleaned), kept)
#     result.append(msg)
#     logger.debug(msg)
#
#     cache = os.path.join(lazylibrarian.CACHEDIR, "WorkCache")
#     # ensure directory is unicode so we get unicode results from listdir
#     if isinstance(cache, str):
#         cache = cache.decode(lazylibrarian.SYS_ENCODING)
#     cleaned = 0
#     kept = 0
#     if os.path.isdir(cache):
#         for cached_file in os.listdir(cache):
#             target = os.path.join(cache, cached_file)
#             try:
#                 bookid = cached_file.split('.')[0]
#             except IndexError:
#                 logger.error('Clean Cache: Error splitting %s' % cached_file)
#                 continue
#             item = myDB.match('select BookID from books where BookID=?', (bookid,))
#             if not item:
#                 # WorkPage no longer referenced in database, delete cached_file
#                 os.remove(target)
#                 cleaned += 1
#             else:
#                 kept += 1
#     msg = "Cleaned %i file%s from WorkCache, kept %i" % (cleaned, plural(cleaned), kept)
#     result.append(msg)
#     logger.debug(msg)
#
#     cache = os.path.join(lazylibrarian.CACHEDIR, "SeriesCache")
#     # ensure directory is unicode so we get unicode results from listdir
#     if isinstance(cache, str):
#         cache = cache.decode(lazylibrarian.SYS_ENCODING)
#     cleaned = 0
#     kept = 0
#     if os.path.isdir(cache):
#         for cached_file in os.listdir(cache):
#             target = os.path.join(cache, cached_file)
#             try:
#                 seriesid = cached_file.split('.')[0]
#             except IndexError:
#                 logger.error('Clean Cache: Error splitting %s' % cached_file)
#                 continue
#             item = myDB.match('select SeriesID from series where SeriesID=?', (seriesid,))
#             if not item:
#                 # SeriesPage no longer referenced in database, delete cached_file
#                 os.remove(target)
#                 cleaned += 1
#             else:
#                 kept += 1
#     msg = "Cleaned %i file%s from SeriesCache, kept %i" % (cleaned, plural(cleaned), kept)
#     result.append(msg)
#     logger.debug(msg)
#
#     cache = lazylibrarian.CACHEDIR
#     cleaned = 0
#     kept = 0
#     cachedir = os.path.join(cache, 'author')
#     if os.path.isdir(cachedir):
#         for cached_file in os.listdir(cachedir):
#             target = os.path.join(cachedir, cached_file)
#             if os.path.isfile(target):
#                 try:
#                     imgid = cached_file.split('.')[0].rsplit(os.sep)[-1]
#                 except IndexError:
#                     logger.error('Clean Cache: Error splitting %s' % cached_file)
#                     continue
#                 item = myDB.match('select AuthorID from authors where AuthorID=?', (imgid,))
#                 if not item:
#                     # Author Image no longer referenced in database, delete cached_file
#                     os.remove(target)
#                     cleaned += 1
#                 else:
#                     kept += 1
#     cachedir = os.path.join(cache, 'book')
#     if os.path.isdir(cachedir):
#         for cached_file in os.listdir(cachedir):
#             target = os.path.join(cachedir, cached_file)
#             if os.path.isfile(target):
#                 try:
#                     imgid = cached_file.split('.')[0].rsplit(os.sep)[-1]
#                 except IndexError:
#                     logger.error('Clean Cache: Error splitting %s' % cached_file)
#                     continue
#                 item = myDB.match('select BookID from books where BookID=?', (imgid,))
#                 if not item:
#                     # Book Image no longer referenced in database, delete cached_file
#                     os.remove(target)
#                     cleaned += 1
#                 else:
#                     kept += 1
#
#     # at this point there should be no more .jpg files in the root of the cachedir
#     # any that are still there are for books/authors deleted from database
#     for cached_file in os.listdir(cache):
#         if cached_file.endswith('.jpg'):
#             os.remove(os.path.join(cache, cached_file))
#             cleaned += 1
#     msg = "Cleaned %i file%s from ImageCache, kept %i" % (cleaned, plural(cleaned), kept)
#     result.append(msg)
#     logger.debug(msg)
#
#     # verify the cover images referenced in the database are present
#     images = myDB.action('select BookImg,BookName,BookID from books')
#     cachedir = os.path.join(lazylibrarian.CACHEDIR, 'book')
#     cleaned = 0
#     kept = 0
#     for item in images:
#         keep = True
#         imgfile = ''
#         if item['BookImg'] is None or item['BookImg'] == '':
#             keep = False
#         if keep and not item['BookImg'].startswith('http') and not item['BookImg'] == "images/nocover.png":
#             # html uses '/' as separator, but os might not
#             imgname = item['BookImg'].rsplit('/')[-1]
#             imgfile = os.path.join(cachedir, imgname)
#             if not os.path.isfile(imgfile):
#                 keep = False
#         if keep:
#             kept += 1
#         else:
#             cleaned += 1
#             logger.debug('Cover missing for %s %s' % (item['BookName'], imgfile))
#             myDB.action('update books set BookImg="images/nocover.png" where Bookid=?', (item['BookID'],))
#
#     msg = "Cleaned %i missing cover file%s, kept %i" % (cleaned, plural(cleaned), kept)
#     result.append(msg)
#     logger.debug(msg)
#
#     # verify the author images referenced in the database are present
#     images = myDB.action('select AuthorImg,AuthorName,AuthorID from authors')
#     cachedir = os.path.join(lazylibrarian.CACHEDIR, 'author')
#     cleaned = 0
#     kept = 0
#     for item in images:
#         keep = True
#         imgfile = ''
#         if item['AuthorImg'] is None or item['AuthorImg'] == '':
#             keep = False
#         if keep and not item['AuthorImg'].startswith('http') and not item['AuthorImg'] == "images/nophoto.png":
#             # html uses '/' as separator, but os might not
#             imgname = item['AuthorImg'].rsplit('/')[-1]
#             imgfile = os.path.join(cachedir, imgname)
#             if not os.path.isfile(imgfile):
#                 keep = False
#         if keep:
#             kept += 1
#         else:
#             cleaned += 1
#             logger.debug('Image missing for %s %s' % (item['AuthorName'], imgfile))
#             myDB.action('update authors set AuthorImg="images/nophoto.png" where AuthorID=?', (item['AuthorID'],))
#
#     msg = "Cleaned %i missing author image%s, kept %i" % (cleaned, plural(cleaned), kept)
#     result.append(msg)
#     logger.debug(msg)
#     return result

def checkint(value):
    try:
        res = int(eval(str(value)))
        if type(res) == int:
            return res
    except:
        return

def proxyList():
    proxies = None
    if schoolkeeper.CONFIG['PROXY_HOST']:
        proxies = {}
        for item in getList(schoolkeeper.CONFIG['PROXY_TYPE']):
            if item in ['http', 'https']:
                proxies.update({item: schoolkeeper.CONFIG['PROXY_HOST']})
    return proxies

