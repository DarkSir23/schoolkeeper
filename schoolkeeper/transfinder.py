import schoolkeeper
from schoolkeeper import logger
import pysftp
import os
import csv

class transfinderConnection:
    def __init__(self, tfhost=None, tfuser=None, tfpassword=None):
        self.is_connected = False
        if not tfhost:
            self.tfhost = schoolkeeper.CONFIG['TRANSFINDER_SFTP_ADDR']
        else:
            self.tfhost=tfhost
        if not tfuser:
            self.tfuser = schoolkeeper.CONFIG['TRANSFINDER_SFTP_USER']
        else:
            self.tfuser = tfuser
        if not tfpassword:
            self.tfpassword = schoolkeeper.CONFIG['TRANSFINDER_SFTP_PASS']
        else:
            self.tfpassword = tfpassword
        self.cnopts = pysftp.CnOpts()
        self.cnopts.hostkeys = None
        self.filename = None
        logger.debug('[TRANSFINDER]: %s - %s - %s' % (self.tfhost, self.tfuser, self.tfpassword))

    def upload(self, filename):
        conn = pysftp.Connection(host=self.tfhost, username=self.tfuser, password=self.tfpassword, cnopts=self.cnopts)
        with conn.cd('uploads'):
            conn.put(filename)
        conn.close()

    def download(self, filename=None, location=None):
        conn = pysftp.Connection(host=self.tfhost, username=self.tfuser, password=self.tfpassword, cnopts=self.cnopts)
        if not filename:
            self.filename = schoolkeeper.CONFIG['TRANSFINDER_IMPORT_FILE']
        else:
            self.filename = filename
        if not location:
            self.location = os.path.join(schoolkeeper.CACHEDIR, self.filename)
        else:
            self.location = os.path.join(location, self.filename)
        try:
            logger.debug('[TRANSFINDER]: Getting file: %s' % self.filename)
            conn.get(self.filename, self.location)
            infilename = self.location
        except Exception as e:
            infilename = None
            logger.debug('[TRANSFINDER]: Could not get file: %s -- %s' % (self.filename,e))
        return infilename

def convertTransfinderData():
    myTF = transfinderConnection()
    useExcel = True
    infilename = myTF.download()
    success = False
    if infilename:
        if useExcel:
            import pandas as pd
            import xlrd
            wb = xlrd.open_workbook(infilename, logfile=open(os.devnull, 'w'))
            df = pd.read_excel(wb, engine='xlrd')
            tempfilename = os.path.splitext(infilename)[0] + '.csv'
            logger.debug('File Read, converting to %s' % tempfilename)
            df.to_csv(tempfilename)
            infilename = tempfilename
        try:
            with open(infilename, 'r') as infile:
                csvlist = []
                try:
                    csvreader = csv.DictReader(infile)
                except UnicodeDecodeError as e:
                    logger.debug('Unable to import Transfinder File:  %s' % e)
                except Exception as e:
                    logger.debug('Unable to read Transfinder File:  %s' % e)
                for row in csvreader:
                    if len(row['Local ID']) > 2:
                        newrow = [str(row['Local ID']).split('.')[0], row['AM Bus Number'], row['AM Driver'], row['AM Transfer'], row['PM Bus Number'], row['PM Driver']]
                        csvlist.append(newrow)
        except Exception as e:
            msg = 'Could not import Transfinder data: %s' % e
            logger.debug(msg)
            return msg


        outfilename = schoolkeeper.CONFIG['TRANSFINDER_EXPORT_FILE']
        try:
            with open(outfilename, 'wb') as csvfile:
                writer = csv.writer(csvfile, delimiter='\t', lineterminator='\r\n', quoting=csv.QUOTE_MINIMAL)
                for line in csvlist:
                    writer.writerow(line)
            msg = '[TRANSFINDER] Output file created.'
            logger.debug(msg)
            return msg
        except Exception as e:
            msg = 'Could not write output file: %s' % e
            logger.debug(msg)
            return msg
    else:
        return 'Could not get filename'

