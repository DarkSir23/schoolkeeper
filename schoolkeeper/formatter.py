import datetime
import os
import re
import shlex
import string
import time
import unicodedata


def safe_unicode(obj, *args):
    """ return the unicode representation of obj """
    try:
        return unicode(obj, *args)
    except UnicodeDecodeError:
        # obj is byte string
        ascii_text = str(obj).encode('string_escape')
        return unicode(ascii_text)

def now():
    dtnow = datetime.datetime.now()
    return dtnow.strftime("%Y-%m-%d %H:%M:%S")

def check_int(var, default):
    """
    Return an integer representation of var
    or return default value if var is not integer
    """
    try:
        return int(var)
    except (ValueError, TypeError):
        return default

def plural(var):
    if check_int(var, 0) == 1:
        return ''
    return 's'

def next_run(when_run):
    """
    Give a readable approximation of how long until a job will be run
    """
    timenow = time.time()
    when_run = time.strptime(when_run, '%Y-%m-%d %H:%M:%S')
    when_run = time.mktime(when_run)
    diff = when_run - timenow  # time difference in seconds
    # calculate whole units, plus round up by adding 1(true) if remainder >= half
    days = int(diff / 86400) + (diff % 86400 >= 43200)
    hours = int(diff / 3600) + (diff % 3600 >= 1800)
    minutes = int(diff / 60) + (diff % 60 >= 30)
    seconds = int(diff)

    if days > 1:
        return "%i days" % days
    elif hours > 1:
        return "%i hours" % hours
    elif minutes > 1:
        return "%i minutes" % minutes
    else:
        return "%i seconds" % seconds
