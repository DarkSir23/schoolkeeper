from __future__ import with_statement

import ConfigParser
import threading
import cherrypy
import subprocess
import sys
import os
import platform
import time
import json



from schoolkeeper import logger, database, activedirectory, dbactions, transfinder, powerschool, efunds, images
from lib.apscheduler.scheduler import Scheduler
from schoolkeeper.formatter import check_int
from schoolkeeper.cache import fetchURL
from schoolkeeper.common import checkRunningJobs, restartJobs

FULL_PATH = None
PROG_DIR = None
DAEMON = False
SIGNAL = None
PIDFILE = ''
DATADIR = ''
CACHEDIR = ''
SESSIONDIR = ''
CONFIGFILE = ''
LOGLEVEL = 1
CONFIG = {}
CFG = ''
COMMIT_LIST =None
SYS_ENCODING = 'utf-8'
UPDATE_MSG = ''
HTTP_ROOT = ''
CURRENT_TAB = '1'
BOOTSTRAP_THEMELIST = []
BOOTSTRAP4_THEMELIST = []
SUPERADMIN_LEVELS = ['SuperAdmin', 'Admin', 'Staff', 'Driver']
ETHNICITY_CODES = ['', 'American Indian', 'Black or African', 'Asian', 'Hispanic or Latino', 'White', 'Other', 'Native Hawaiian']
SCHED = Scheduler()
INIT_LOCK = threading.Lock()
__INITIALIZED__ = False
started = False
GITLAB_TOKEN = 'gitlab+deploy-token-27986:BMqvySYAfv51A_7kNFJw@gitlab.com'

LOGLIST = []
LOGTOGGLE = 2 # normal logging

CONFIG_GIT = ['GIT_HOST', 'GIT_REPO', 'GIT_USER', 'GIT_BRANCH', 'LATEST_VERSION', 'GIT_UPDATED', 'CURRENT_VERSION',
    'COMMITS_BEHIND', 'INSTALL_TYPE']
CONFIG_NONWEB = ['LOGFILES', 'LOGSIZE', 'LOGLIMIT']
CONFIG_NONDEFAULT = ['BOOTSTRAP_THEME', 'BOOTSTRAP4_THEME']
DB_TYPES = ['SQLITE3','POSTGRESQL']
DEVICE_IMPORT_DESTINATIONS = [
    {'destination': 'NONE','description': 'None'},
    {'destination': 'STUDENT_ID','description': 'Student ID'},
    {'destination': 'STAFF_ID','description': 'Staff ID'},
    {'destination': 'DEVICE_TAG','description': 'Device Tag'},
    {'destination': 'DEVICE_SERIAL','description': 'Device Serial Number'},
    {'destination': 'DEVICE_NOTES','description': 'Device Notes'},
    {'destination': 'DEVICE_MODEL','description': 'Device Model'},
    {'destination': 'DEVICE_PURCHASED','description': 'Device Year Purchased'},
    {'destination': 'STAFF_FIRST','description': 'Staff First Name'},
    {'destination': 'STAFF_LAST','description': 'Staff Last Name'},
    {'destination': 'DEVICE_ADNAME', 'description': 'Device Active Directory Name'},
    {'destination': 'DEVICE_MAC', 'description': 'Device MAC Address'},
    {'destination': 'DEVICE_LOCATION', 'description': 'Device Location'}
]
DEVICE_IMPORT_STATICDESTINATIONS = [
    {'destination': 'NONE','description': 'None'},
    {'destination': 'STAFF_ID','description': 'Staff ID'},
    {'destination': 'DEVICE_NOTES','description': 'Device Notes'},
    {'destination': 'DEVICE_MODEL','description': 'Device Model'},
    {'destination': 'DEVICE_PURCHASED','description': 'Device Year Purchased'},
    {'destination': 'STAFF_FIRST','description': 'Staff First Name'},
    {'destination': 'STAFF_LAST','description': 'Staff Last Name'},
    {'destination': 'DEVICE_LOCATION', 'description': 'Device Location'}
]

PICTURE_IMPORT_DESTINATIONS = [
    {'destination': 'NONE', 'description': 'None'},
    {'destination': 'STUDENT_ID', 'description': 'Student ID'},
    {'destination': 'STAFF_ID', 'description': 'Staff ID'},
    {'destination': 'INTERNAL_ID', 'description': 'Internal ID'},
    {'destination': 'FILENAME', 'description': 'File Name'},
]

CONFIG_DEFINITIONS = {
    # Name	Type	Section	Default
    'USER_ACCOUNTS': ('bool', 'General', 0),
    'LOGDIR': ('str', 'General', ''),
    'LOGLIMIT': ('int', 'General', 500),
    'LOGFILES': ('int', 'General', 10),
    'LOGSIZE': ('int', 'General', 204800),
    'LOGLEVEL': ('int', 'General', 1),
    'IMAGEDIR': ('str', 'General', ''),
    'DISPLAYLENGTH': ('int', 'General', 10),
    'HTTP_PORT': ('int', 'General', 8339),
    'HTTP_HOST': ('str', 'General', '0.0.0.0'),
    'HTTP_TIMEOUT': ('int', 'General', 30),
    'USER_AGENT': ('str', 'General', ''),
    'HTTP_USER': ('str', 'General', ''),
    'HTTP_PASS': ('str', 'General', ''),
    'HTTP_HASHED_PASSWORD': ('Bool', 'General', 0),
    'HTTP_PROXY': ('bool', 'General', 0),
    'HTTP_ROOT': ('str', 'General', '/'),
    'HTTP_LOOK': ('str', 'General', 'bootstrap'),
    'HTTPS_ENABLED': ('bool', 'General', 0),
    'HTTPS_CERT': ('str', 'General', ''),
    'HTTPS_KEY': ('str', 'General', ''),
    'PROXY_HOST': ('str', 'General', ''),
    'PROXY_TYPE': ('str', 'General', ''),
    'GIT_PROGRAM': ('str', 'General', ''),
    'GIT_HOST': ('str', 'Git', 'gitlab.com'),
    'GIT_USER': ('str', 'Git', 'DarkSir23'),
    'GIT_REPO': ('str', 'Git', 'schoolkeeper'),
    'GIT_BRANCH': ('str', 'Git', 'master'),
    'GIT_UPDATED': ('str', 'Git', ''),
    'INSTALL_TYPE': ('str', 'Git', ''),
    'CURRENT_VERSION': ('str', 'Git', ''),
    'LATEST_VERSION': ('str', 'Git', ''),
    'COMMITS_BEHIND': ('int', 'Git', 0),
    'VERSIONCHECK_INTERVAL': ('int', 'Git', '24'),
    'BOOTSTRAP_THEME': ('str', 'General', 'cerulean'),
    'BOOTSTRAP4_THEME': ('str', 'General', 'cerulean'),
    'API_ENABLED': ('bool', 'General', 0),
    'API_KEY': ('str', 'General', ''),
    'TOGGLES': ('bool', 'General', 1),
    'CORP_NAME': ('str', 'General', ''),
    'CORP_DOMAIN': ('str', 'General', ''),
    'CURRENT_SCHOOL_YEAR': ('int', 'General', 2018),
    'LDAP_SERVER': ('str', 'LDAP', ''),
    'LDAP_DOMAIN': ('str', 'LDAP', ''),
    'LDAP_BASEDN': ('str', 'LDAP', ''),
    'LDAP_BASESTUDENTDN': ('str', 'LDAP', ''),
    'LDAP_BASESTAFFDN': ('str', 'LDAP', ''),
    'LDAP_BASEDEVICEDN': ('str', 'LDAP', ''),
    'LDAP_USER': ('str', 'LDAP', ''),
    'LDAP_PASS': ('str', 'LDAP', ''),
    'ALLOW_LDAP_LOGIN': ('bool', 'LDAP', 1),
    'LDAP_ADMIN1_GROUP': ('str', 'LDAP', ''),
    'LDAP_STAFF_GROUP': ('str', 'LDAP', ''),
    'LDAP_TEACHER_GROUP': ('str', 'LDAP', ''),
    'AD_TIMER': ('int', 'LDAP', 480),
    'GAM_PATH': ('str', 'GAM', ''),
    'GAMADV_PATH': ('str', 'GAM', ''),
    'GAM_CLASS_MAIL_YEARS': ('int', 'GAM', 6),
    'FILE_PS_STUDENTS': ('str', 'Files', ''),
    'FILE_PS_STAFF': ('str', 'Files', ''),
    'FOLDER_PS_CLASSES': ('str', 'Files', ''),
    'FOLDER_PS_SCHEDULES': ('str', 'Files', ''),
    'FOLDER_OTHER_IMPORT': ('str', 'Files', ''),
    'PS_TIMER': ('int', 'Files', 60),
    'FOLDER_ATTACHMENTS': ('str', 'Files', ''),
    'DB_TYPE': ('str', 'Database', 'SQLITE3'),
    'DB_USERNAME': ('str', 'Database', 'schoolkeeper'),
    'DB_PASSWORD': ('str', 'Database', 'p@ssword'),
    'DB_HOST': ('str', 'Database', 'localhost'),
    'DB_DATABASE': ('str', 'Database', 'schoolkeeper_db'),
    'PIVOT_ENABLE': ('bool', 'PIVOT', 0),
    'PIVOT_SFTP_ADDR': ('str', 'PIVOT', ''),
    'PIVOT_SFTP_USER': ('str', 'PIVOT', ''),
    'PIVOT_SFTP_PASS': ('str', 'PIVOT', ''),
    'TRANSFINDER_ENABLE': ('bool', 'Transfinder', 0),
    'TRANSFINDER_SFTP_ADDR': ('str', 'Transfinder', ''),
    'TRANSFINDER_SFTP_USER': ('str', 'Transfinder', ''),
    'TRANSFINDER_SFTP_PASS': ('str', 'Transfinder', ''),
    'TRANSFINDER_IMPORT_FILE': ('str', 'Transfinder', ''),
    'TRANSFINDER_EXPORT_FILE': ('str', 'Transfinder', ''),
    'EFUNDS_ENABLE': ('bool', 'EFunds', 0),
    'EFUNDS_SFTP_ADDR': ('str', 'EFunds', ''),
    'EFUNDS_SFTP_USER': ('str', 'EFunds', ''),
    'EFUNDS_SFTP_PASS': ('str', 'EFunds', ''),
    'EFUNDS_IMPORT_FILE': ('str', 'EFunds', ''),
    'EFUNDS_EXPORT_FILE': ('str', 'EFunds', ''),
    'GROUPS_ENABLE': ('bool', 'Google', 1),
    'GROUPS_MIN_GRADELEVEL': ('int', 'Google', 7),
}

def check_section(sec):
    if CFG.has_section(sec):
        return True
    else:
        CFG.add_section(sec)
        return False

def check_setting(cfg_type, cfg_name, item_name, def_val, log=True):
    """ Check option exists, corece to correct type, or return default """
    my_val = def_val
    if cfg_type == 'int':
        try:
            my_val = CFG.getint(cfg_name, item_name)
        except ConfigParser.Error:
            # no such item, might be a new entry
            my_val = int(def_val)
        except Exception as e:
            logger.warn('Invalid int for %s: %s, using default %s' % (cfg_name, item_name, int(def_val)))
            logger.debug(str(e))
            my_val = int(def_val)

    elif cfg_type == 'bool':
        try:
            my_val = CFG.getboolean(cfg_name, item_name)
        except ConfigParser.Error:
            # no such item, might be a new entry
            my_val = bool(def_val)
        except Exception as e:
            logger.warn('Invalid bool for %s: %s, using default %s' % (cfg_name, item_name, bool(def_val)))
            logger.debug(str(e))
            my_val = bool(def_val)

    elif cfg_type == 'str':
        try:
            my_val = CFG.get(cfg_name, item_name)
            if my_val.startswith('"') and my_val.endswith('"'):
                my_val = my_val[1:-1]
            if not len(my_val):
                my_val = def_val
            my_val = my_val.decode(SYS_ENCODING)
        except ConfigParser.Error:
            # no such item, might be a new entry
            my_val = str(def_val)
        except Exception as e:
            logger.warn('Invalid str for %s: %s, using default %s' % (cfg_name, item_name, str(def_val)))
            logger.debug(str(e))
            my_val = str(def_val)

    check_section(cfg_name)
    CFG.set(cfg_name, item_name, my_val)
    if log:
        logger.debug("%s : %s -> %s" % (cfg_name, item_name, my_val))

    return my_val


def initialize():
    global FULL_PATH, PROG_DIR, ARGS, DAEMON, SIGNAL, PIDFILE, DATADIR, CONFIGFILE, COMMIT_LIST, SYS_ENCODING, LOGLEVEL, \
        CONFIG, CFG, CURRENT_TAB, BOOTSTRAP_THEMELIST, BOOTSTRAP4_THEMELIST, __INITIALIZED__, CACHEDIR, DEVICE_IMPORT_DESTINATIONS, DEVICE_IMPORT_STATICDESTINATIONS, SUPERADMIN_LEVELS

    with INIT_LOCK:

        if __INITIALIZED__:
            return False

        check_section('General')
        # False to silence logging until logger initialzied
        for key in ['LOGLIMIT', 'LOGFILES', 'LOGSIZE']:
            item_type, section, default = CONFIG_DEFINITIONS[key]
            CONFIG[key.upper()] = check_setting(item_type, section, key.lower(), default, log=False)
        CONFIG['LOGDIR'] = os.path.join(DATADIR, 'logs')
        CONFIG['IMAGEDIR'] = os.path.join(DATADIR, 'images')

        # Create logdir
        if not os.path.exists(CONFIG['LOGDIR']):
            try:
                os.makedirs(CONFIG['LOGDIR'])
            except OSError as e:
                if LOGLEVEL:
                    print '%s : unable to create folder for logs: %s' % (
                        CONFIG['LOGDIR'], str(e))

        # Create imagedir
        if not os.path.exists(CONFIG['IMAGEDIR']):
            try:
                os.makedirs(CONFIG['IMAGEDIR'])
            except OSError as e:
                if LOGLEVEL:
                    print '%s : unable to create folder for images: %s' % (
                        CONFIG['IMAGEDIR'], str(e))


        # Start the logger, silence console logging if we need to
        CFGLOGLEVEL = check_setting('int', 'General', 'loglevel', 9, log=False)
        if LOGLEVEL == 1: # default if no debug or quiet on cmdline
            if CFGLOGLEVEL == 9:  # default value if none in config
                LOGLEVEL = 2 # If not set in Config or cmdline, then lets set to DEBUG
            else:
                LOGLEVEL = CFGLOGLEVEL # Config setting picked up

        CONFIG['LOGLEVEL'] = LOGLEVEL
        logger.schoolkeeper_log.initLogger(loglevel=CONFIG['LOGLEVEL'])
        logger.info("Log level set to [%s] - Log Directory is [%s] - Config level is [%s]" % (
            CONFIG['LOGLEVEL'], CONFIG['LOGDIR'], CFGLOGLEVEL))
        if CONFIG['LOGLEVEL'] > 2:
            logger.info("Screen Log set to FULL DEBUG")
        elif CONFIG['LOGLEVEL'] == 2:
            logger.info("Screen Log set to DEBUG")
        else:
            logger.info("Screen Log set to INFO/WARN/ERROR")

        config_read()
        if not os.path.exists(CONFIGFILE):
            config_write()
        logger.info('SYS_ENCODING is %s' % SYS_ENCODING)
        CACHEDIR = os.path.join(DATADIR, 'cache')
        try:
            os.makedirs(CACHEDIR)
        except OSError as e:
            if not os.path.isdir(CACHEDIR):
                logger.error('Could not create cachedir; %s' % e.strerror)
        SESSIONDIR = os.path.join(DATADIR, 'sessions')
        try:
            os.makedirs(SESSIONDIR)
        except OSError as e:
            if not os.path.isdir(SESSIONDIR):
                logger.error('Could not create sessiondir; %s' % e.strerror)


        time_now = int(time.time())

        # Initialize the database
        try:
            myDB = database.DBConnection()
            version = myDB.get_version()
            check = myDB.integrity_check()
            logger.info("Database is version %s, integrity check: %s" % (version, check[0]))

        except Exception as e:
            logger.error("Can't connect to the database: %s" % str(e))

        BOOTSTRAP_THEMELIST = build_bootstrap_themes() # Add when bootstrap is added
        BOOTSTRAP4_THEMELIST = build_bootstrap_themes('bootstrap4')

        __INITIALIZED__ = True
        checkRunningJobs()
        return True

def config_read(reloaded=False):
    global CONFIG, CONFIG_DEFINITIONS, CONFIG_NONWEB, CONFIG_NONDEFAULT

    for key in CONFIG_DEFINITIONS.keys():
        item_type, section, default = CONFIG_DEFINITIONS[key]
        CONFIG[key.upper()] = check_setting(item_type, section, key.lower(), default)
    if not CONFIG['LOGDIR']:
        CONFIG['LOGDIR'] = os.path.join(DATADIR, 'logs')
    if not CONFIG['IMAGEDIR']:
        CONFIG['IMAGEDIR'] = os.path.join(DATADIR, 'images')
    if CONFIG['HTTP_PORT'] < 21 or CONFIG['HTTP_PORT'] > 65535:
        CONFIG['HTTP_PORT'] = 8339
    if reloaded:
        logger.info('Config file reloaded')
    else:
        logger.info('Config file loaded')

def config_write():
    global CONFIG_NONWEB, CONFIG_NONDEFAULT, LOGLEVEL

    interface = CFG.get('General', 'http_look')
    for key in CONFIG_DEFINITIONS.keys():
        item_type, section, default = CONFIG_DEFINITIONS[key]
        if key == 'WALL_COLUMNS': # may be modified by user interface but not on config page
            value = CONFIG[key]
        elif key not in CONFIG_NONWEB and not (interface == 'default' and key in CONFIG_NONDEFAULT):
            check_section(section)
            value = CONFIG[key]
            if key == 'LOGLEVEL':
                LOGLEVEL = check_int(value,2)
            elif key in ['LOGDIR', 'DOWNLOAD_DIR']:
                value = value.encode(SYS_ENCODING)
        else:
            value = CFG.get(section, key.lower())
            if CONFIG['LOGLEVEL'] > 2:
                logger.debug("Leaving %s unchanged (%s)" % (key, value))
            CONFIG[key] = value
        CFG.set(section, key.lower(), value)

    # sanity check for typos
    for key in CONFIG.keys():
        if key not in CONFIG_DEFINITIONS.keys():
            logger.warn('Unsaved config key: %s' % key)

    # myDB = database.DBConnection() # Uncomment if any DB info needs written to the config

    msg = None
    try:
        with open(CONFIGFILE + '.new', 'wb') as configfile:
            CFG.write(configfile)
    except Exception as e:
        msg = '{} {} {}'.format('Unable to create new config file:', CONFIGFILE, e.strerror)
        logger.warn(msg)
        return

    try:
        os.remove(CONFIGFILE + '.bak')
    except OSError as e:
        if e.errno is not 2: # doesn't exist is ok
            msg = '{} {}{} {}'.format('Error deleting backup file:', CONFIGFILE, '.bak', e.strerror)
            logger.warn(msg)

    try:
        os.rename(CONFIGFILE, CONFIGFILE + '.bak')
    except OSError as e:
        if e.errno is not 2: # doesn't exist is ok as wouldn't exist until first save
            msg = '{} {} {}'.format('Unable to backup config file', CONFIGFILE, e.strerror)
            logger.warn(msg)

    try:
        os.rename(CONFIGFILE + '.new', CONFIGFILE)
    except OSError as e:
        msg = '{} {} {}'.format('Unable to rename new config file:', CONFIGFILE, e.strerror)
        logger.warn(msg)

    if not msg:
        msg = 'Config file [%s] has been updated' % CONFIGFILE
        logger.info(msg)

def build_bootstrap_themes(theme='bootstrap3'):
    themelist = []
    if not os.path.isdir(os.path.join(PROG_DIR, 'data', 'interfaces', 'bootstrap')):
        return themelist  # return empty if bookstrap interface not installed
    if theme == 'bootstrap4':
        URL = 'http://bootswatch.com/api/4.json'
    else:
        URL = 'http://bootswatch.com/api/3.json'
    result, success = fetchURL(URL, None, False)  # use default headers, no retry

    if not success:
        logger.debug("Error getting bootstrap themes : %s" % result)
        return themelist

    try:
        results = json.loads(result)
        for theme in results['themes']:
            themelist.append(theme['name'].lower())
    except Exception as e:
        # error reading results
        logger.debug('JSON Error reading bootstrap themes, %s' % str(e))

    logger.debug("Bootstrap found %i themes" % len(themelist))
    return themelist

def daemonize():

    # make a non-session-leader child process
    try:
        pid = os.fork()
        if pid != 0:
            sys.exit(0)
    except OSError as e:
        raise RuntimeError("1st fork failed: %s [%d]" % (e.strerror, e.errno))

        os.setsid()

        prev = os.umask(0)
        os.umask(prev and int('077', 8))

        try:
            pid = os.fork()
            if pid != 0:
                sys.exit()
        except OSError as e:
            raise RuntimeError("2nd fork failed: %s [%d]" % (e.strerror, e.errno))

        dev_null = file('/dev/null', 'r')
        os.dup2(dev_null.fileno(), sys.stdin.fileno())

        if PIDFILE:
            pid = str(os.getpid())
            logger.debug(u"Writing PID " + pid + " to " + str(PIDFILE))
            file(PIDFILE, 'w').write("%s\n" % pid)

def start():
    global __INITIALIZED__, started

    if __INITIALIZED__:
        SCHED.start()
        started = True
        if not UPDATE_MSG:
            restartJobs(start='Start')
            dbactions.updateDeviceCache()

def logmsg(level, msg):
    # log messages to logger if initialized, or print if not.
    if __INITIALIZED__:
        if level == 'error':
            logger.error(msg)
        elif level == 'debug':
            logger.debug(msg)
        elif level == 'warn':
            logger.warn(msg)
        else:
            logger.info(msg)
    else:
        print level.upper(), msg

def shutdown(restart=False, update=False):
    cherrypy. engine.exit()
    SCHED.shutdown(wait=False)

    if not restart and not update:
        logmsg('info', 'SchoolKeeper is shutting down...')

    if update:
        logmsg('info', 'SchoolKeeper is updating...')
        try:
            if versioncheck.update():
                logmsg('info', 'SchoolKeeper version updated')
                config_write()
        except Exception as e:
            logmsg('warn', 'SchoolKeeper failed to update: %s. Restarting.' % str(e))

    if PIDFILE:
        logmsg('info', 'Removing pidfile %s' % PIDFILE)
        os.remove(PIDFILE)

    if restart:
        logmsg('info', 'SchoolKeeper is restarting...')
        # Try to use running python
        executable = sys.executable
        if not executable:
            if platform.system() == "windows":
                params = ["where", "python2"]
                try:
                    executable = subprocess.check_output(params, stderr=subprocess.STDOUT).strip()
                except Exception as e:
                    logger.debug("where python2 failed: %s" % str(e))
            else:
                params = ["which", "python2"]
                try:
                    executable = subprocess.check_output(params, stderr=subprocess.STDOUT).strip()
                except Exception as e:
                    logger.debug("where python2 failed: %s" % str(e))

        if not executable:
            executable = 'python'

        popen_list = [executable, FULL_PATH]
        popen_list += ARGS
        if '--update' in popen_list:
            popen_list.remove('--update')
        if LOGLEVEL:
            if '--quiet' in popen_list:
                popen_list.remove('--quiet')
            if '-q' in popen_list:
                popen_list.remove('-q')

        logmsg('debug', 'Restarting SchoolKeeper with ' + str(popen_list))
        subprocess.Popen(popen_list, cwd=os.getcwd())

    logmsg('info', 'schoolkeeper is exiting')
    config_write()
    os._exit(0)
