from cgi import escape
from datetime import datetime, timedelta
import re

import cherrypy


from lib.hashing_passwords import check_hash

import schoolkeeper
from schoolkeeper import logger, database, dblogger, activedirectory


SESSION_KEY = '_cp_username'

def check_credentials(username, password, admin_login='0'):
    """Verifies credentials for username and password.
    Returns True and the user group on success or False and no user group"""
    myDB = database.DBConnection()
    userlist = myDB.action("select userid, interface, bootstrap4look, interfacelook from users")
    if schoolkeeper.CONFIG['HTTP_HASHED_PASSWORD'] and \
        username == schoolkeeper.CONFIG['HTTP_USER'] and check_hash(password, schoolkeeper.CONFIG['HTTP_PASS']):
        return True, 'SuperAdmin', schoolkeeper.SUPERADMIN_LEVELS, None, None
    elif username == schoolkeeper.CONFIG['HTTP_USER'] and password == schoolkeeper.CONFIG['HTTP_PASS']:
        return True, 'SuperAdmin', schoolkeeper.SUPERADMIN_LEVELS, None, None
    elif schoolkeeper.CONFIG['ALLOW_LDAP_LOGIN']:
        myLdap = activedirectory.ldapConnection(ldapusername=username, ldappassword=password)
        if myLdap.is_connected:
            auth = myLdap.getlogininfo()
            if 'sAMAccountName' in auth.keys():
                access_level = ['Password']
                if myLdap.checkldapgroups(auth['distinguishedName'],schoolkeeper.CONFIG['LDAP_ADMIN1_GROUP']):
                    access_level.remove('Password')
                    access_level.append('Admin')
                if myLdap.checkldapgroups(auth['distinguishedName'],schoolkeeper.CONFIG['LDAP_STAFF_GROUP']):
                    access_level.append('Staff')
                if myLdap.checkldapgroups(auth['distinguishedName'], schoolkeeper.CONFIG['LDAP_TEACHER_GROUP']):
                    access_level.append('Teacher')
                interface = None
                interfacelook = None
                userfound = False
                for user in userlist:
                    if auth['sAMACcountName'].lower() == user['userid'].lower():
                        interface = user['interface']
                        if interface == 'bootstrap4':
                            interfacelook = user['bootstrap4look']
                        else:
                            interfacelook = user['interfacelook']
                        userfound = True
                if not userfound:
                    myDB.upsert('users', {'interface': None, 'bootstrap4look': None, 'interfacelook': None}, {'userid': auth['sAMAccountName'].lower()})
                return True, auth['sAMAccountName'].lower(), access_level, interface, interfacelook
    return False, None, None, None, None


def check_auth(*args, **kwargs):
    """A tool that looks in config for 'auth.require'. If found and it
    is not None, a login is required and the entry is evaluated as a list of
    conditions that the user must fulfill"""
    conditions = cherrypy.request.config.get('auth.require', None)
    if conditions is not None:
        _session = cherrypy.session.get(SESSION_KEY)

        if _session and (_session['user'] and _session['expiry']) and _session['expiry'] > datetime.now():
            logger.debug('Session Expired.  Time: %s - Session: %s' % (datetime.now(), _session))
            cherrypy.request.login = _session['user']
            for condition in conditions:
                # A condition is just a callable that returns true or false
                if not condition():
                    raise cherrypy.HTTPRedirect(schoolkeeper.CONFIG['HTTP_ROOT'])
        else:
            logger.debug('No Session.  Time: %s - Session: %s' %(datetime.now(), _session))
            raise cherrypy.HTTPRedirect(schoolkeeper.CONFIG['HTTP_ROOT'] + "auth/logout")


def requireAuth(*conditions):
    """A decorator that appends conditions to the auth.require config
    variable."""
    def decorate(f):
        if not hasattr(f, '_cp_config'):
            f._cp_config = dict()
        if 'auth.require' not in f._cp_config:
            f._cp_config['auth.require'] = []
        f._cp_config['auth.require'].extend(conditions)
        return f
    return decorate


# Conditions are callables that return True
# if the user fulfills the conditions they define, False otherwise
#
# They can access the current username as cherrypy.request.login
#
# Define those at will however suits the application.

def member_of(groupname):
    def check():
        # replace with actual check if <username> is in <groupname>
        _session = cherrypy.session.get(SESSION_KEY)
        return groupname == _session['access_level']
    return check


def has_access_level(level):
    def check():
        _session = cherrypy.session.get(SESSION_KEY)
        if level in _session['access_level']:
            return True
        else:
            return False
    return check

def name_is(reqd_username):
    return lambda: reqd_username == cherrypy.request.login

# These might be handy


def any_of(*conditions):
    """Returns True if any of the conditions match"""
    def check():
        for c in conditions:
            if c():
                return True
        return False
    return check


# By default all conditions are required, but this might still be
# needed if you want to use it inside of an any_of(...) condition
def all_of(*conditions):
    """Returns True if all of the conditions match"""
    def check():
        for c in conditions:
            if not c():
                return False
        return True
    return check

class AuthController(object):

    def on_login(self, user_id, username, access_level):
        """Called on successful login"""

        # Save login to the database
        ip_address = cherrypy.request.headers.get('X-Forwarded-For', cherrypy.request.headers.get('Remote-Addr'))
        host = cherrypy.request.headers.get('Origin')
        user_agent = cherrypy.request.headers.get('User-Agent')
        remote_host = cherrypy.request.remote.ip
        dblogger.user("%s logged into the web interface.  Source IP: %s" % (username, remote_host), username, username)


        logger.debug(u"schoolkeeper WebAuth :: %s user '%s' logged into schoolkeeper. IP: %s" % (access_level, username, ip_address))

    def on_logout(self, username, access_level):
        """Called on logout"""
        logger.debug(u"schoolkeeper WebAuth :: %s User '%s' logged out of schoolkeeper." % (access_level, username))

    def get_loginform(self, username="", msg=""):
        from schoolkeeper.webServe import serve_template
        return serve_template(templatename="login.html", title="Login", username=escape(username, True), msg=msg)

    @cherrypy.expose
    def index(self):
        raise cherrypy.HTTPRedirect(schoolkeeper.HTTP_ROOT + "login")

    @cherrypy.expose
    def login(self, username=None, password=None, remember_me='0', admin_login='0'):
        remote_host = cherrypy.request.remote.ip
        if not cherrypy.config.get('tools.sessions.on'):
            raise cherrypy.HTTPRedirect(schoolkeeper.HTTP_ROOT + "home")

        if not username and not password:
            return self.get_loginform()

        (vaild_login, user_id, access_level, theme, themelook) = check_credentials(username, password, admin_login)

        if vaild_login:

            expiry = datetime.now() + (timedelta(days=30) if remember_me == '1' else timedelta(minutes=60))
            logger.debug("Date: %s - Expiry: %s" % (datetime.now(), expiry))
            cherrypy.session.regenerate()
            cherrypy.request.login = username
            cherrypy.request.accesslevel = access_level
            cherrypy.session[SESSION_KEY] = {'user_id': user_id,
                                             'user': username,
                                             'access_level': access_level,
                                             'expiry': expiry,
                                             'theme': theme,
                                             'themelook': themelook}

            self.on_login(user_id, username, access_level)
            raise cherrypy.HTTPRedirect(schoolkeeper.CONFIG['HTTP_ROOT'])

        elif admin_login == '1':
            logger.debug(u"schoolkeeper WebAuth :: Invalid admin login attempt from '%s'.  Source IP: %s" % (username, remote_host))
            raise cherrypy.HTTPRedirect(schoolkeeper.CONFIG['HTTP_ROOT'])
        else:
            logger.debug(u"schoolkeeper WebAuth :: Invalid login attempt from '%s'.  Source IP: %s" % (username, remote_host))
            return self.get_loginform(username, u"Incorrect username/email or password.")

    @cherrypy.expose
    def logout(self):
        if not cherrypy.config.get('tools.sessions.on'):
            raise cherrypy.HTTPRedirecte(schoolkeeper.CONFIG['HTTP_ROOT'])

        _session = cherrypy.session.get(SESSION_KEY)
        cherrypy.session[SESSION_KEY] = None

        if _session and _session['user']:
            cherrypy.request.login = None
            self.on_logout(_session['user'], _session['access_level'])
        raise cherrypy.HTTPRedirect(schoolkeeper.CONFIG['HTTP_ROOT'] + "auth/login")
