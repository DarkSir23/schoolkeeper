import schoolkeeper
import pysftp
import csv
from schoolkeeper import logger

class efundsConnection:
    def __init__(self, efundshost=None, efundsuser=None, efundspassword=None):
        self.is_connected = False
        if not efundshost:
            self.efundshost = schoolkeeper.CONFIG['EFUNDS_SFTP_ADDR']
        else:
            self.efundshost=efundshost
        if not efundsuser:
            self.efundsuser = schoolkeeper.CONFIG['EFUNDS_SFTP_USER']
        else:
            self.efundsuser = efundsuser
        if not efundspassword:
            self.efundspassword = schoolkeeper.CONFIG['EFUNDS_SFTP_PASS']
        else:
            self.efundspassword = efundspassword
        self.cnopts = pysftp.CnOpts()
        self.cnopts.hostkeys = None

    def upload(self, filename):
        conn = pysftp.Connection(host=self.efundshost, username=self.efundsuser, password=self.efundspassword, cnopts=self.cnopts)
        with conn.cd('/users/whitkocsc'):
            response = conn.put(filename)
        conn.close()
        logger.debug("Server Response: %s" % response)
        return response

    def balanceConversion(self, infile, outfile):
        csvlist = []
        fieldnames = ['id', 'schoolid', 'lastname', 'firstname', 'studentid', 'balance']
        try:
            with open(infile, 'r') as csvinfile:
                csvreader = csv.DictReader(csvinfile, fieldnames=fieldnames)
                for row in csvreader:
                    if row['schoolid'] != "SchoolNumber":
                        csvlist.append([row['studentid'], row['schoolid'], row['lastname'], row['firstname'], row['balance']])
                logger.debug("Record Count: %s" % len(csvlist))
        except Exception as e:
            logger.debug("Could not read file: %s - %s" % (infile, e))
            return False
        try:
            with open(outfile, 'wb') as csvoutfile:
                outfieldnames = ['Student_Number', 'School Number', 'LastName', 'FirstName', 'Balance']

                csvwriter = csv.writer(csvoutfile, delimiter='|')
                csvwriter.writerow(outfieldnames)
                for row in csvlist:
                    logger.debug(row)
                    csvwriter.writerow(row)
                    # logger.debug('Writing: %s - %s' % (row[4], row[2]))

        except Exception as e:
            logger.debug("Could not write file: %s - %s" % (outfile, e))
            return False
        return outfile
