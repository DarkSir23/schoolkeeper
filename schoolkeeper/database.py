from __future__ import with_statement

import sqlite3
import threading
import time
import psycopg2
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
from psycopg2.extras import DictCursor
import os

import schoolkeeper
from schoolkeeper import logger

db_lock = threading.Lock()

class DBConnection:
    def __init__(self, autocommit=False):
        self.totalchanges = 0
        if schoolkeeper.CONFIG['DB_TYPE'] == 'SQLITE3':
            db_database = os.path.join(schoolkeeper.DATADIR, schoolkeeper.CONFIG['DB_DATABASE'] + '.db')
            self.connection = sqlite3.connect(db_database, 20)
            self.connection.row_factory = sqlite3.Row
            self.connection.text_factory = lambda x: unicode(x, "utf-8", "ignore")
        elif schoolkeeper.CONFIG['DB_TYPE'] == 'POSTGRESQL':
            db_database = schoolkeeper.CONFIG['DB_DATABASE']
            db_username = schoolkeeper.CONFIG['DB_USERNAME']
            db_password = schoolkeeper.CONFIG['DB_PASSWORD']
            db_host = schoolkeeper.CONFIG['DB_HOST']
            try:
                self.connection = psycopg2.connect("dbname='%s' user='%s' host='%s' password='%s'" % (db_database, db_username, db_host, db_password))
                self.connection.set_session(autocommit=autocommit)
            except psycopg2.OperationalError as e:
                self.connection = psycopg2.connect("dbname='postgres' user='%s' host='%s' password='%s'" % (db_username, db_host, db_password))
                self.connection.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
                cur = self.connection.cursor()
                cur.execute('CREATE DATABASE ' + db_database)
                temp1 = cur.execute('SET my.version to 0;')
                temp2 = cur.execute('ALTER DATABASE %s SET my.version from current' % (db_database))
                logger.debug("Stuff: %s - %s" % (temp1, temp2))
                cur.close()
                self.connection.commit()
                self.connection.close()
                self.connection = psycopg2.connect("dbname='%s' user='%s' host='%s' password='%s'" % (db_database, db_username, db_host, db_password))

    def action(self, query, args=None, suppress=None):
        with db_lock:

            if not query:
                return

            sqlResult = None
            attempt = 0

            while attempt < 5:

                try:
                    if schoolkeeper.CONFIG['DB_TYPE'] == 'SQLITE3':
                        if not args:
                            sqlResult = self.connection.execute(query)
                        else:
                            sqlResult = self.connection.execute(query, args)
                        self.totalchanges = self.connection.total_changes
                    elif schoolkeeper.CONFIG['DB_TYPE'] == 'POSTGRESQL':
                        cur = self.connection.cursor(cursor_factory=DictCursor)
                        if not args:
                            cur.execute(self.dbformat(query))
                            sqlResult = cur
                        else:
                            newquery = self.dbformat(query, args)
                            # logger.debug("Query: %s" % newquery)
                            # logger.debug("args: %s" % args)
                            cur.execute(newquery, args)
                            sqlResult = cur
                        self.totalchanges += cur.rowcount
                    self.connection.commit()
                    break

                except (sqlite3.OperationalError, psycopg2.OperationalError) as e:
                    if "unable to open database file" in e.message or "database is locked" in e.message:
                        logger.warn('Database Error: %s' % e)
                        logger.debug("Attempted db query: [%s]" % query)
                        attempt += 1
                        if attempt == 5:
                            logger.error("Failed db query: [%s]" % query)
                        else:
                            time.sleep(1)
                    else:
                        logger.error('Database error: %s' % e)
                        logger.error("Failed query: [%s]" % query)
                        raise

                except (sqlite3.IntegrityError, psycopg2.IntegrityError) as e:
                    # we could ignore unique errors in sqlite by using "insert or ignore into" statements
                    # but this would also ignore null values as we can't specify which errors to ignore :-(
                    # The python interface to sqlite only returns english text messages, not error codes
                    msg = e.message
                    msg = msg.lower()
                    if suppress == 'UNIQUE' and ('not unique' in msg or 'unique constraint failed' in msg):
                        if int(schoolkeeper.LOGLEVEL) > 2:
                            logger.debug('Suppressed [%s] %s' % (query, e.message))
                        self.connection.commit()
                        break
                    else:
                        logger.error('Database Integrity error: %s' % e)
                        logger.error("Failed query: [%s]" % query)
                        raise

                except (sqlite3.DatabaseError, psycopg2.DatabaseError) as e:
                    logger.error('Fatal error executing %s :: %s' % (query, e))
                    raise

            return sqlResult

    def match(self, query, args=None):
        try:
            # if there are no results, action() returns None and .fetchone() fails
            sqlResults = self.action(query, args).fetchone()
        except (sqlite3.Error, psycopg2.Error, AttributeError):
            return []
        if not sqlResults:
            return []

        return sqlResults

    def select(self, query, args=None):
        try:
            # if there are no results, action() returns None and .fetchall() fails
            sqlResults = self.action(query, args).fetchall()
        except (sqlite3.Error, psycopg2.Error, AttributeError):
            return []
        if not sqlResults:
            return []

        return sqlResults

    @staticmethod
    def genParams(myDict):
        return [x + " = ?" for x in myDict.keys()]

    def upsert(self, tableName, valueDict, keyDict):


        changesBefore = self.totalchanges
        # genParams = lambda myDict: [x + " = ?" for x in myDict.keys()]
        query = "UPDATE " + tableName + " SET " + ", ".join(self.genParams(valueDict)) + \
                " WHERE " + " AND ".join(self.genParams(keyDict))

        self.action(self.dbformat(query), valueDict.values() + keyDict.values())

        if self.totalchanges == changesBefore:
            for key in keyDict.keys():
                if not keyDict[key]:
                    del keyDict[key]
            query = "INSERT INTO " + tableName + " (" + ", ".join(valueDict.keys() + keyDict.keys()) + ")" + \
                    " VALUES (" + ", ".join(["?"] * len(valueDict.keys() + keyDict.keys())) + ")"
            return self.action(self.dbformat(query), valueDict.values() + keyDict.values())

    def get_version(self):
        db_version = 0
        if schoolkeeper.CONFIG['DB_TYPE'] == 'SQLITE3':
            result = self.match('PRAGMA user_version')
            if result and result[0]:
                value = str(result[0])
                if value.isdigit():
                    db_version = int(value)
        elif schoolkeeper.CONFIG['DB_TYPE'] == 'POSTGRESQL':
            try:
                db_version = self.action("SELECT current_setting('my.version') as version;").fetchone()[0]
                # logger.debug("DBVER : %s" % temp)
            except psycopg2.ProgrammingError:
                self.action("SET my.version to 0;")
                self.action("ALTER DATABASE %s set my.version from current;" % schoolkeeper.CONFIG['DB_DATABASE'])
                db_version = self.action("SELECT current_setting('my.version') as version;").fetchone()[0]
                # logger.debug("DBVER2 : %s" % temp)

        return db_version

    def set_version(self, version):
        if schoolkeeper.CONFIG['DB_TYPE'] == 'SQLITE3':
            self.action('PRAGMA user_version=%s' % version)
        elif schoolkeeper.CONFIG['DB_TYPE'] == 'POSTGRESQL':
            self.action('SET my.version to %s;' % version)
            self.action("ALTER DATABASE %s set my.version from current;" % schoolkeeper.CONFIG['DB_DATABASE'])
            logger.debug("Setting: %s" % version)

    def integrity_check(self):
        if schoolkeeper.CONFIG['DB_TYPE'] == 'SQLITE3':
            return self.match('PRAGMA integrity_check')
        else:
            return [1]

    def dbformat(self, sqlstring, args=None):
        if schoolkeeper.CONFIG['DB_TYPE'] == 'SQLITE3':
            return sqlstring
        elif schoolkeeper.CONFIG['DB_TYPE'] == 'POSTGRESQL':
            sqllist = sqlstring.split('?')
            outstring = ''
            for i, item in enumerate(sqllist):
                outstring += item
                if i+1 < len(sqllist):
                    outstring += '%s'
                    if args and isinstance(args[i],int):
                        outstring += '::integer'
            # logger.debug("DBFormat: %s" % outstring)
            return outstring.replace(' COLLATE NOCASE', '')


