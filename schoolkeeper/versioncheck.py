import os
import platform
import re
import socket
import subprocess
import tarfile
import threading
import time
try:
    import urllib3
    import requests
except ImportError:
    import lib.requests as requests

import schoolkeeper
import lib.simplejson as simplejson
from schoolkeeper import logger, version
from schoolkeeper.formatter import check_int
from schoolkeeper.common import proxyList, getUserAgent
from schoolkeeper.common import USER_AGENT

def logmsg(level, msg):
    # log messages to logger if initialised, or print if not.
    if schoolkeeper.__INITIALIZED__:
        if level == 'error':
            logger.error(msg)
        elif level == 'info':
            logger.info(msg)
        elif level == 'debug':
            logger.debug(msg)
        elif level == 'warn':
            logger.warn(msg)
        else:
            logger.info(msg)
    else:
        print level.upper(), msg

def runGit(args):
    # Function to execute GIT commands taking care of error logging etc
    if schoolkeeper.CONFIG['GIT_PROGRAM']:
        git_locations = ['"' + schoolkeeper.CONFIG['GIT_PROGRAM'] + '"']
    else:
        git_locations = ['git']

    if platform.system().lower() == 'darwin':
        git_locations.append('/usr/local/git/bin/git')

    output = err = None

    for cur_git in git_locations:

        cmd = cur_git + ' ' + args

        try:
            logmsg('debug', '(RunGit)Trying to execute: "' + cmd + '" with shell in ' + schoolkeeper.PROG_DIR)
            p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT,
                                 shell=True, cwd=schoolkeeper.PROG_DIR)
            output, err = p.communicate()
            logmsg('debug', '(RunGit)Git output: [%s]' % output.strip('\n'))

        except OSError:
            logmsg('debug', '(RunGit)Command ' + cmd + ' didn\'t work, couldn\'t find git')
            continue

        if 'not found' in output or "not recognized as an internal or external command" in output:
            logmsg('debug', '(RunGit)Unable to find git with command ' + cmd)
            logmsg('error', 'git not found - please ensure git executable is in your PATH')
            output = None
        elif 'fatal:' in output or err:
            logmsg('error', '(RunGit)Git returned bad info. Are you sure this is a git installation?')
            output = None
        elif output:
            break

    return output, err

def getInstallType():
    # need a way of detecting if we are running a windows .exe file
    # (which we can't upgrade)  rather than just running git or source on windows
    # We use a string in the version.py file for this
    # FUTURE:   Add a version number string in this file too?
    try:
        install = version.SCHOOLKEEPER_VERSION.lower()
    except:
        install = 'unknown'

    if install in ['windows', 'win32build']:
        schoolkeeper.CONFIG['INSTALL_TYPE'] = 'win'
        schoolkeeper.CURRENT_BRANCH = 'Windows'

    elif install == 'package':  # deb, rpm, other non-upgradeable
        schoolkeeper.CONFIG['INSTALL_TYPE'] = 'package'
        schoolkeeper.CONFIG['GIT_BRANCH'] = 'Package'

    elif os.path.isdir(os.path.join(schoolkeeper.PROG_DIR, '.git')):
        schoolkeeper.CONFIG['INSTALL_TYPE'] = 'git'
        schoolkeeper.CONFIG['GIT_BRANCH'] = getCurrentGitBranch()
    else:
        schoolkeeper.CONFIG['INSTALL_TYPE'] = 'source'
        schoolkeeper.CONFIG['GIT_BRANCH'] = 'master'

    logmsg('debug', '(getInstallType) [%s] install detected. Setting Branch to [%s]' %
           (schoolkeeper.CONFIG['INSTALL_TYPE'], schoolkeeper.CONFIG['GIT_BRANCH']))

def getCurrentVersion():
    if schoolkeeper.CONFIG['INSTALL_TYPE'] == 'win':
        logmsg('debug', '(getCurrentVersion) Windows install - no update available')

        # Don't have a way to update exe yet, but don't want to set VERSION to None
        VERSION = 'Windows Install'

    elif schoolkeeper.CONFIG['INSTALL_TYPE'] == 'git':
        output, err = runGit('rev-parse HEAD')

        if not output:
            logmsg('error', '(getCurrentVersion) Couldn\'t find latest git installed version.')
            cur_commit_hash = 'GIT Cannot establish version'
        else:
            cur_commit_hash = output.strip()

            if not re.match('^[a-z0-9]+$', cur_commit_hash):
                logmsg('error', '(getCurrentVersion) Output doesn\'t look like a hash, not using it')
                cur_commit_hash = 'GIT invalid hash return'

        VERSION = cur_commit_hash

    elif schoolkeeper.CONFIG['INSTALL_TYPE'] in ['source', 'package']:

        version_file = os.path.join(schoolkeeper.PROG_DIR, 'version.txt')

        if not os.path.isfile(version_file):
            VERSION = 'No Version File'
            logmsg('debug', '(getCurrentVersion) [%s] missing.' % version_file)
            return VERSION
        else:
            fp = open(version_file, 'r')
            current_version = fp.read().strip(' \n\r')
            fp.close()

            if current_version:
                VERSION = current_version
            else:
                VERSION = 'No Version set in file'
                return VERSION
    else:
        logmsg('error', '(getCurrentVersion) Install Type not set - cannot get version value')
        VERSION = 'Install type not set'
        return VERSION

    updated = updateVersionFile(VERSION)
    if updated:
        logmsg('debug', '(getCurrentVersion) - Install type [%s] Local Version is set to [%s] ' % (
            schoolkeeper.CONFIG['INSTALL_TYPE'], VERSION))
    else:
        logmsg('debug', '(getCurrentVersion) - Install type [%s] Local Version is unchanged [%s] ' % (
            schoolkeeper.CONFIG['INSTALL_TYPE'], VERSION))

    return VERSION

def getCurrentGitBranch():
    # Can only work for GIT driven installs, so check install type
    if schoolkeeper.CONFIG['INSTALL_TYPE'] != 'git':
        logmsg('debug', 'Non GIT Install doing getCurrentGitBranch')
        return 'NON GIT INSTALL'

    # use git rev-parse --abbrev-ref HEAD which returns the name of the current branch
    current_branch, err = runGit('rev-parse --abbrev-ref HEAD')
    current_branch = str(current_branch)
    current_branch = current_branch.strip(' \n\r')

    if not current_branch:
        logmsg('error', 'failed to return current branch value')
        return 'InvalidBranch'

    logmsg('debug', '(getCurrentGitBranch) Current local branch of repo is [%s] ' % current_branch)

    return current_branch

def checkForUpdates():
    """ Called at startup, from webserver with thread name WEBSERVER, or as a cron job """
    if 'Thread-' in threading.currentThread().name:
        threading.currentThread().name = "CRON-VERSIONCHECK"
    logmsg('debug', 'Set Install Type, Current & Latest Version and Commit status')
    getInstallType()
    schoolkeeper.CONFIG['CURRENT_VERSION'] = getCurrentVersion().encode('ascii')
    schoolkeeper.CONFIG['LATEST_VERSION'] = getLatestVersion().encode('ascii')
    if schoolkeeper.CONFIG['CURRENT_VERSION'] == schoolkeeper.CONFIG['LATEST_VERSION']:
        schoolkeeper.CONFIG['COMMITS_BEHIND'] = 0
        schoolkeeper.COMMIT_LIST = ""
    else:
        schoolkeeper.CONFIG['COMMITS_BEHIND'], schoolkeeper.COMMIT_LIST = getCommitDifferenceFromGit()
    logmsg('debug', 'Update check complete')

def getLatestVersion():
    if schoolkeeper.CONFIG['INSTALL_TYPE'] in ['git', 'source', 'package']:
        latest_version = getLatestVersion_FromGit()
    elif schoolkeeper.CONFIG['INSTALL_TYPE'] in ['win']:
        latest_version = 'WINDOWS INSTALL'
    else:
        latest_version = 'UNKNOWN INSTALL'

    schoolkeeper.CONFIG['LATEST_VERSION'] = latest_version
    return latest_version

def getLatestVersion_FromGit():
    latest_version = 'Unknown'

    # Can only work for non Windows driven installs, so check install type
    if schoolkeeper.CONFIG['INSTALL_TYPE'] == 'win':
        logmsg('debug', '(getLatestVersion_FromGit) Error - should not be called under a windows install')
        latest_version = 'WINDOWS INSTALL'
    else:
        # check current branch value of the local git repo as folks may pull from a branch not master
        branch = schoolkeeper.CONFIG['GIT_BRANCH']

        if branch == 'InvalidBranch':
            logmsg('debug', '(getLatestVersion_FromGit) - Failed to get a valid branch name from local repo')
        else:
            if branch == 'Package':  # check packages against master
                branch = 'master'
            # Get the latest commit available from github
            if 'gitlab' in schoolkeeper.CONFIG['GIT_HOST']:
                url = 'https://%s/api/v4/projects/%s%%2F%s/repository/branches/%s' % (
                    schoolkeeper.GITLAB_TOKEN,
                    schoolkeeper.CONFIG['GIT_USER'],
                    schoolkeeper.CONFIG['GIT_REPO'],
                    branch
                )
            else:
                url = 'https://api.%s/repos/%s/%s/commits/%s' % (
                    schoolkeeper.CONFIG['GIT_HOST'],
                    schoolkeeper.CONFIG['GIT_USER'],
                    schoolkeeper.CONFIG['GIT_REPO'],
                    branch
                )
            logmsg('debug',
                   '(getLatestVersion_FromGit) Retrieving latest version information from github command=[%s]' % url)

            timestamp = check_int(schoolkeeper.CONFIG['GIT_UPDATED'], 0)
            age = ''
            if timestamp:
                DAYNAMES = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
                MONNAMES = ['', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                tm = time.gmtime(timestamp)
                age = "%s, %02d %s %04d %02d:%02d:%02d GMT" % (DAYNAMES[tm.tm_wday], tm.tm_mday,
                                                               MONNAMES[tm.tm_mon], tm.tm_year, tm.tm_hour,
                                                               tm.tm_min, tm.tm_sec)

            try:
                headers = {'User-Agent': getUserAgent()}
                if age:
                    logmsg('debug', 'Checking if modified since %s' % age)
                    headers.update({'If-Modified-Since': age})
                proxies = proxyList()
                timeout = check_int(schoolkeeper.CONFIG['HTTP_TIMEOUT'], 30)
                r = requests.get(url, timeout=timeout, headers=headers, proxies=proxies)

                if str(r.status_code).startswith('2'):
                    git = r.json()
                    if 'gitlab' in schoolkeeper.CONFIG['GIT_HOST']:
                        latest_version = git['commit']['id']
                    else:
                        latest_version = git['sha']
                    logmsg('debug', 'Branch [%s] Latest Version has been set to [%s]' % (
                        branch, latest_version))
                elif str(r.status_code) == '304':
                    latest_version = schoolkeeper.CONFIG['CURRENT_VERSION']
                    logmsg('debug', 'Not modified, currently on Latest Version')
                else:
                    logmsg('warn', 'Could not get the latest commit from git')
                    logmsg('debug', 'git latest version returned %s' % r.status_code)
                    latest_version = 'Not_Available_From_Git'
            except Exception as e:
                logmsg('warn', 'Could not get the latest commit from git')
                logmsg('debug', 'git %s for %s: %s' % (type(e).__name__, url, str(e)))
                latest_version = 'Not_Available_From_Git'

    return latest_version

def getCommitDifferenceFromGit():
    # See how many commits behind we are
    commits = -1
    # Takes current latest version value and trys to diff it with the latest
    # version in the current branch.
    commit_list = ''
    if schoolkeeper.CONFIG['LATEST_VERSION'] in ['Not_Available_From_GitHUB', 'Not_Available_From_Git']:
        commits = 0  # don't report a commit diff as we don't know anything
        commit_list = 'Unable to get latest version from %s' % schoolkeeper.CONFIG['GIT_HOST']
        logmsg('info', commit_list)
    if schoolkeeper.CONFIG['CURRENT_VERSION'] and commits != 0:
        if 'gitlab' in schoolkeeper.CONFIG['GIT_HOST']:
            url = 'https://%s/api/v4/projects/%s%%2F%s/repository/compare?from=%s&to=%s' % (
                schoolkeeper.GITLAB_TOKEN, schoolkeeper.CONFIG['GIT_USER'],
                schoolkeeper.CONFIG['GIT_REPO'], schoolkeeper.CONFIG['CURRENT_VERSION'],
                schoolkeeper.CONFIG['LATEST_VERSION'])
        else:
            url = 'https://api.%s/repos/%s/%s/compare/%s...%s' % (
                schoolkeeper.CONFIG['GIT_HOST'], schoolkeeper.CONFIG['GIT_USER'],
                schoolkeeper.CONFIG['GIT_REPO'], schoolkeeper.CONFIG['CURRENT_VERSION'],
                schoolkeeper.CONFIG['LATEST_VERSION'])
        logmsg('debug', 'Check for differences between local & repo by [%s]' % url)

        try:
            headers = {'User-Agent': getUserAgent()}
            proxies = proxyList()
            timeout = check_int(schoolkeeper.CONFIG['HTTP_TIMEOUT'], 30)
            r = requests.get(url, timeout=timeout, headers=headers, proxies=proxies)
            git = r.json()
            # for gitlab, commits = len(git['commits'])  no status/ahead/behind
            if 'gitlab' in schoolkeeper.CONFIG['GIT_HOST']:
                if 'commits' in git:
                    commits = len(git['commits'])
                    msg = 'Git: Total Commits [%s]' % commits
                    logmsg('debug', msg)
                else:
                    logmsg('warn', 'Could not get difference status from git: %s' % str(git))

                if commits > 0:
                    for item in git['commits']:
                        commit_list = "%s\n%s" % (item['title'], commit_list)
            else:
                if 'total_commits' in git:
                    commits = int(git['total_commits'])
                    msg = 'Git: Status [%s] - Ahead [%s] - Behind [%s] - Total Commits [%s]' % (
                        git['status'], git['ahead_by'], git['behind_by'], git['total_commits'])
                    logmsg('debug', msg)
                else:
                    logmsg('warn', 'Could not get difference status from git: %s' % str(git))

                if commits > 0:
                    for item in git['commits']:
                        commit_list = "%s\n%s" % (item['commit']['message'], commit_list)
        except Exception as e:
            logmsg('warn', 'Could not get difference status from git: %s' % type(e).__name__)


    if commits > 1:
        logmsg('info', '[VersionCheck] -  New version is available. You are %s commits behind' % commits)
    elif commits == 1:
        logmsg('info', '[VersionCheck] -  New version is available. You are one commit behind')
    elif commits == 0:
        logmsg('info', '[VersionCheck] -  AllianceKeeper is up to date ')
        # schoolkeeper.CONFIG['GIT_UPDATED'] = time.strftime("%a, %d %b %Y %H:%M:%S GMT", time.gmtime())
    elif commits < 0:
        msg = '[VersionCheck] -  You are running an unknown version of AllianceKeeper. '
        msg += 'Run the updater to identify your version'
        logmsg('info', msg)

    logmsg('debug', '(getCommitDifferenceFromGit) - exiting with commit value of [%s]' % commits)
    # schoolkeeper.CONFIG['COMMITS_BEHIND'] = commits
    return commits, commit_list


def updateVersionFile(new_version_id):
    # Update version.txt located in AK home dir.
    version_path = os.path.join(schoolkeeper.PROG_DIR, 'version.txt')

    try:
        logmsg('debug', "(updateVersionFile) Updating [%s] with value [%s]" % (
            version_path, new_version_id))
        if os.path.exists(version_path):
            ver_file = open(version_path, 'r')
            current_version = ver_file.read().strip(' \n\r')
            ver_file.close()
            if current_version == new_version_id:
                return False

        ver_file = open(version_path, 'w')
        ver_file.write(new_version_id)
        ver_file.close()
        schoolkeeper.CONFIG['CURRENT_VERSION'] = new_version_id
        return True
    except IOError as e:
        logmsg('error',
               u"(updateVersionFile) Unable to write current version to version.txt, update not complete: %s" % str(e))
        return False



def update():
    if schoolkeeper.CONFIG['INSTALL_TYPE'] == 'win':
        logmsg('debug', '(update) Windows install - no update available')
        logmsg('info', '(update) Windows .exe updating not supported yet.')
        return False
    elif schoolkeeper.CONFIG['INSTALL_TYPE'] == 'package':
        logmsg('debug', '(update) Package install - no update available')
        logmsg('info', '(update) Please use your package manager to update')
        return False

    elif schoolkeeper.CONFIG['INSTALL_TYPE'] == 'git':
        branch = getCurrentGitBranch()

        _, _ = runGit('stash clear')
        output, err = runGit('pull origin ' + branch)

        success = True
        if not output:
            logmsg('error', '(update) Couldn\'t download latest version')
            success = False
        for line in output.split('\n'):
            if 'Already up-to-date.' in line:
                logmsg('info', '(update) No update available, not updating')
                logmsg('info', '(update) Output: ' + str(output))
                success = False
                # schoolkeeper.CONFIG['GIT_UPDATED'] = time.strftime("%a, %d %b %Y %H:%M:%S GMT", time.gmtime())
            elif 'Aborting' in line or 'local changes' in line:
                logmsg('error', '(update) Unable to update from git: ' + line)
                logmsg('info', '(update) Output: ' + str(output))
                success = False
        if success:
            schoolkeeper.CONFIG['GIT_UPDATED'] = time.strftime("%a, %d %b %Y %H:%M:%S GMT", time.gmtime())
            return True

    elif schoolkeeper.CONFIG['INSTALL_TYPE'] == 'source':
        if 'gitlab' in schoolkeeper.CONFIG['GIT_HOST']:
            tar_download_url = 'https://%s/%s/%s/-/archive/%s/%s-%s.tar.gz' % (
                schoolkeeper.GITLAB_TOKEN, schoolkeeper.CONFIG['GIT_USER'],
                schoolkeeper.CONFIG['GIT_REPO'], schoolkeeper.CONFIG['GIT_BRANCH'],
                schoolkeeper.CONFIG['GIT_REPO'], schoolkeeper.CONFIG['GIT_BRANCH'])
        else:
            tar_download_url = 'https://%s/%s/%s/tarball/%s' % (
                schoolkeeper.CONFIG['GIT_HOST'], schoolkeeper.CONFIG['GIT_USER'],
                schoolkeeper.CONFIG['GIT_REPO'], schoolkeeper.CONFIG['GIT_BRANCH'])
        update_dir = os.path.join(schoolkeeper.PROG_DIR, 'update')

        try:
            logmsg('info', 'Downloading update from: ' + tar_download_url)
            headers = {'User-Agent': getUserAgent()}
            proxies = proxyList()
            timeout = check_int(schoolkeeper.CONFIG['HTTP_TIMEOUT'], 30)
            r = requests.get(tar_download_url, timeout=timeout, headers=headers, proxies=proxies)
        except requests.exceptions.Timeout:
            logmsg('error', "Timeout retrieving new version from " + tar_download_url)
            return False
        except Exception as e:
            if hasattr(e, 'reason'):
                errmsg = e.reason
            else:
                errmsg = str(e)
            logmsg('error',
                   "Unable to retrieve new version from " + tar_download_url + ", can't update: %s" % errmsg)
            return False

        download_name = r.url.split('/')[-1]

        tar_download_path = os.path.join(schoolkeeper.PROG_DIR, download_name)

        # Save tar to disk
        with open(tar_download_path, 'wb') as f:
            f.write(r.content)

        # Extract the tar to update folder
        logmsg('info', 'Extracting file ' + tar_download_path)
        try:
            with tarfile.open(tar_download_path) as tar:
                tar.extractall(update_dir)
        except Exception as e:
            logger.error('Failed to unpack tarfile %s (%s): %s' % (type(e).__name__, tar_download_path, str(e)))
            return False

        # Delete the tar.gz
        logmsg('info', 'Deleting file ' + tar_download_path)
        os.remove(tar_download_path)

        # Find update dir name
        update_dir_contents = [x for x in os.listdir(update_dir) if os.path.isdir(os.path.join(update_dir, x))]
        if len(update_dir_contents) != 1:
            logmsg('error', "Invalid update data, update failed: " + str(update_dir_contents))
            return False
        content_dir = os.path.join(update_dir, update_dir_contents[0])

        # walk temp folder and move files to main folder
        for rootdir, dirnames, filenames in os.walk(content_dir):
            rootdir = rootdir[len(content_dir) + 1:]
            for curfile in filenames:
                old_path = os.path.join(content_dir, rootdir, curfile)
                new_path = os.path.join(schoolkeeper.PROG_DIR, rootdir, curfile)

                if os.path.isfile(new_path):
                    os.remove(new_path)
                os.renames(old_path, new_path)

        # Update version.txt and timestamp
        updateVersionFile(schoolkeeper.CONFIG['LATEST_VERSION'])
        schoolkeeper.CONFIG['GIT_UPDATED'] = str(int(time.time()))
        schoolkeeper.CONFIG['CURRENT_VERSION'] = schoolkeeper.CONFIG['LATEST_VERSION']
        return True

    else:
        logmsg('error', "Cannot perform update - Install Type not set")
        return False



