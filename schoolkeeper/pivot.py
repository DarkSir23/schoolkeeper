import schoolkeeper
import pysftp

class pivotConnection:
    def __init__(self, pivothost=None, pivotuser=None, pivotpassword=None):
        self.is_connected = False
        if not pivothost:
            self.pivothost = schoolkeeper.CONFIG['PIVOT_SFTP_ADDR']
        else:
            self.pivothost=pivothost
        if not pivotuser:
            self.pivotuser = schoolkeeper.CONFIG['PIVOT_SFTP_USER']
        else:
            self.pivotuser = pivotuser
        if not pivotpassword:
            self.pivotpassword = schoolkeeper.CONFIG['PIVOT_SFTP_PASS']
        else:
            self.pivotpassword = pivotpassword
        self.cnopts = pysftp.CnOpts()
        self.cnopts.hostkeys = None

    def upload(self, filename):
        conn = pysftp.Connection(host=self.pivothost, username=self.pivotuser, password=self.pivotpassword, cnopts=self.cnopts)
        with conn.cd('uploads'):
            conn.put(filename)
        conn.close()